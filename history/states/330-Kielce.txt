
state={
	id=330
	name="STATE_330"
	resources={
		steel=8.000
	}

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 3
			industrial_complex = 2
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 3
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 3
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 3
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 3
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 3
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 3
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 3
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = POL
		add_core_of = POL
		1939.10.6 = {
			owner = GPO
			controller = GPO
			add_core_of = GPO

		}
		1945.3.20 = {
			owner = SOV
			controller = SOV

		}
		victory_points = {
			5008 3 
		}
		victory_points = {
			9973 3 
		}
		victory_points = {
			9974 5 
		}
		victory_points = {
			9977 15 
		}

	}

	provinces={
		328 552 674 2229 5008 5461 5557 9569 9570 9573 9574 9744 9887 9973 9974 9977 11286 
	}
	manpower=1835700
	buildings_max_level_factor=1.000
	state_category=town
	local_supplies=0.000
}
