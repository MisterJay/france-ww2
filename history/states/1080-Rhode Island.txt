state={
	id=1080
	name="STATE_1080"
	history={
		owner = USA
		add_core_of = USA
		buildings = {
			infrastructure = 3
			arms_factory = 1
			industrial_complex = 4
			air_base = 4
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 6
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 3
				industrial_complex = 3
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 4
				industrial_complex = 3
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 4
				industrial_complex = 2
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 5
				industrial_complex = 2
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}


		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 5
				industrial_complex = 1
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 6
				industrial_complex = 1
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
	}
	provinces={
		8647
	}
	state_category = city
	manpower=1400786
	buildings_max_level_factor=1.000
	local_supplies=0.000
}
