
state={
	id=717
	name="STATE_717"

	history={
		buildings = {
			infrastructure = 1
			arms_factory = 0
			industrial_complex = 0
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0
		}

		1940.5.10 = {
			buildings = {
				infrastructure = 1
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1941.6.21 = {
			buildings = {
				infrastructure = 1
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1942.11.19 = {
			buildings = {
				infrastructure = 1
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1943.7.11 = {
			buildings = {
				infrastructure = 1
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1944.6.6 = {
			buildings = {
				infrastructure = 1
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}


		1944.12.14 = {
			buildings = {
				infrastructure = 1
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1945.3.20 = {
			buildings = {
				infrastructure = 1
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		owner = SAU
		add_core_of = SAU

	}

	provinces={
		2 13 32 59 60 70 79 88 108 128 145 150 176 181 216 236 349 358 463 497 527 528 531 544 579 581 588 629 701 714 736 741 742 820 834 857 892 935 968 978 979 997 1005 1053 1151 1169 1257 1359 1383 1425 1492 1562 1628 1774 1809 1815 1816 1845 1889 1944 1978 2156 2185 2245 2263 2276 2355 2501 2604 2640 2733 2763 2852 2912 3052 3202 3247 3416 3438 3456 3462 3490 3699 3825 3894 4034 4060 4063 4168 4233 4397 4683 4770 4816 4845 4981 7134 7145 7146 7150 7153 7155 7159 7163 7169 7176 7195 7272 7317 7337 7430 7463 7734 7743 7744 7745 7746
	}
	manpower=3987651
    state_category = rural
	buildings_max_level_factor=1.000
}
