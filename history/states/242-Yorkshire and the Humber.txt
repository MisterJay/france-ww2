state={
	id=242
	name="STATE_242"
	provinces={
		160 462 3405
	}
	resources={
		steel=121 # was: 120
	}
	history={
		buildings = {
			infrastructure = 3
			arms_factory = 1
			industrial_complex = 2
			air_base = 10
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0
		}

		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 3
				air_base = 10
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 5
				air_base = 10
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 5
				air_base = 10
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 5
				air_base = 10
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 5
				air_base = 10
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}


		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 5
				air_base = 10
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 5
				air_base = 10
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		owner = ENG
		add_core_of = ENG
        victory_points = { 3405 5 }
	}
	manpower=4762495
    state_category = city
	buildings_max_level_factor=1.000
}
