
state={
	id=512
	name="STATE_512"
	resources={
		aluminium=96.000
	}

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 1
			industrial_complex = 1
			air_base = 10
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 1
				air_base = 10
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 3
				air_base = 10
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 3
				air_base = 10
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 3
				air_base = 10
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 3
				air_base = 10
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 3
				air_base = 10
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 3
				air_base = 10
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = FRA
		add_core_of = FRA
		1941.1.1 = {
			owner = EFR
			controller = EFR
			add_core_of = EFR
			add_core_of = GFR

		}
		1942.11.19 = {
			owner = ITA
			controller = ITA
			remove_core_of = EFR

		}
		1944.6.6 = {
			owner = GFR
			controller = GFR

		}
		1944.12.16 = {
			owner = FRA
			controller = FRA

		}
		victory_points = {
			2989 5 
		}
		victory_points = {
			3279 5 
		}
		victory_points = {
			6486 5 
		}
		victory_points = {
			8604 20 
		}

	}

	provinces={
		806 959 1567 1643 1649 2391 2533 2619 2654 2819 2989 3279 3359 3800 4126 4142 4556 4852 6374 6386 6430 6465 6466 6484 6486 6510 6523 6533 6557 6559 6582 8099 8126 11342 
	}
	manpower=4659213
	buildings_max_level_factor=1.000
	state_category=town
	local_supplies=0.000
}
