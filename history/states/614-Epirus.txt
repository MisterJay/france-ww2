
state={
	id=614
	name="STATE_614"

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 0
			industrial_complex = 1
			air_base = 3
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0
		}

		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}


		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 1
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		owner = GRE
		add_core_of = GRE
		1941.4.1 = {
			owner = ITA
			controller = ITA

		}
		1944.6.6 = {
			owner = GER
			controller = GER

		}

		1944.12.16 = {
			owner = GRE
			controller = GRE
		}
		victory_points = {
			6836 2
		}
		victory_points = {
			1692 2
		}
		victory_points = {
			1126 5
		}
		victory_points = {
			6869 2
		}

	}

	provinces={
		1126 1593 1692 2209 3188 3920 3984 4198 4895 6836 6854 6869 6872 6878 9720 9723
	}
	manpower=570000
	buildings_max_level_factor=1.000
	state_category=large_town
}
