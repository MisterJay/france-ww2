
state={
	id=749
	name="STATE_749"

	history={
		buildings = {
			infrastructure = 1
			arms_factory = 0
			industrial_complex = 0
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0
		}

		1940.5.10 = {
			buildings = {
				infrastructure = 1
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1941.6.21 = {
			buildings = {
				infrastructure = 1
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1942.11.19 = {
			buildings = {
				infrastructure = 1
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1943.7.11 = {
			buildings = {
				infrastructure = 1
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1944.6.6 = {
			buildings = {
				infrastructure = 1
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}


		1944.12.14 = {
			buildings = {
				infrastructure = 1
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1945.3.20 = {
			buildings = {
				infrastructure = 1
				arms_factory = 0
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		owner = ITA
		add_core_of = LBA
		1942.11.19 = {
			ENG = {
				set_province_controller = 7391
				set_province_controller = 9037
				set_province_controller = 7432
				set_province_controller = 4947
				set_province_controller = 2262
				set_province_controller = 4114
				set_province_controller = 1566
				set_province_controller = 3092
				set_province_controller = 9467
				set_province_controller = 3542
				set_province_controller = 3353
				set_province_controller = 7373
			}
		}

		1943.7.11 = {
			controller = ENG
		}

        victory_points = { 803 1 }
	}

	provinces={
		268 422 803 1027 1350 1566 1648 1733 2127 2262 2503 2516 2768 3068 3092 3353 3542 3557 3802 4114 4465 4505 4947 7373 7391 7411 7432 7433 7458 7461 9037 9463 9467
	}
	manpower=8808
	buildings_max_level_factor=1.000
	state_category=wasteland
}
