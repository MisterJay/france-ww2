
state={
	id=38
	name="STATE_38"
	provinces={
		8260
	}

	history={
		buildings = {
			infrastructure = 3
		}
		owner = FRA

		1940.6.22 = {
			owner = GFR
		}
		1944.12.16 = {
			owner = FRA
		}
	}
	manpower=1
	state_category = river_crossing
	buildings_max_level_factor=1.000
}
