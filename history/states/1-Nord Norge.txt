
state={
	id=1
	name="STATE_1"
resources={
		chromium=40.000
	tungsten=54
	}
	history={
		buildings = {
			infrastructure = 2
			arms_factory = 0
			industrial_complex = 1
			air_base = 1
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0
		}

		1940.5.10 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 1
				air_base = 1
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1941.6.21 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 1
				air_base = 1
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1942.11.19 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 1
				air_base = 1
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1943.7.11 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 1
				air_base = 1
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1944.6.6 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 1
				air_base = 1
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}


		1944.12.14 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 1
				air_base = 1
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1945.3.20 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 1
				air_base = 1
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		owner = NOR
		add_core_of = NOR


		buildings = {
			526 ={
				naval_base = 5
			}
		}
		victory_points = { 5031 1 }
        victory_points = { 526 10 }
        victory_points = { 943 2 }
        victory_points = { 618 1 }
        victory_points = { 4455 1 }
        victory_points = { 686 5 }

		1940.5.10 = {
			owner = GNO
			add_core_of = GNO

			NOR = {
				set_province_controller = 526
				set_province_controller = 2087
				set_province_controller = 1902
				set_province_controller = 1412
				set_province_controller = 7494
				set_province_controller = 4455
				set_province_controller = 5015
				set_province_controller = 686
				set_province_controller = 326
				set_province_controller = 5010
				set_province_controller = 921
				set_province_controller = 2553
				set_province_controller = 697
				set_province_controller = 7491
			}
		}
		1941.6.22 = {
			controller = GNO
		}
		1944.12.16 = {
			SOV = {
				set_province_controller = 697
				set_province_controller = 2553
			}
		}

		1945.3.20 = {
			SOV = {
				set_province_controller = 921
				set_province_controller = 5010
			}
		}
	}

	provinces={
		222 260 326 526 618 686 692 697 734 921 943 1195 1245 1412 1902 2087 2427 2553 4455 4885 5010 5015 5024 5025 5031 5035 7491 7494 7519
	}
	manpower=355251
    state_category = rural
	buildings_max_level_factor=1.000
}
