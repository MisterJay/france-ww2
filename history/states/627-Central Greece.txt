
state={
	id=627
	name="STATE_627"
	resources={
		chromium=20.000
		steel=20.000
	}

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 0
			industrial_complex = 1
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 3
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 3
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = GRE
		add_core_of = GRE
		1941.4.1 = {
			owner = ITA
			controller = ITA

		}
		1944.6.6 = {
			owner = GER
			controller = GER

		}
		1944.12.16 = {
			owner = GRE
			controller = GRE

		}
		victory_points = {
			6935 10 
		}
		victory_points = {
			3681 1 
		}
		victory_points = {
			6879 5 
		}
		victory_points = {
			4633 5 
		}
		victory_points = {
			1983 10 
		}

	}

	provinces={
		1075 1179 1370 1983 2201 2273 2574 2699 2888 2922 2968 3158 3333 3552 3610 3619 3681 3859 4189 4396 4563 4633 4869 4932 6850 6851 6856 6860 6863 6866 6874 6879 6884 6885 6891 6894 6897 6904 6905 6917 6935 6948 6950 6954 7672 7673 7675 7676 7684 7691 9718 9719 11318 
	}
	manpower=300000
	buildings_max_level_factor=1.000
	state_category=rural
	local_supplies=0.000
}
