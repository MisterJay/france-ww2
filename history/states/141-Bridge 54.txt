
state={
	id=141
	name="STATE_141"
	provinces={
		8374
	}

	history={
		buildings = {
			infrastructure = 3
		}
		owner = BEL
		1941.6.22 = {
			owner = GER
			controller = GER
		}
		1941.6.22 = {
			owner = GBF
		}

		1944.12.16 = {
			owner = BEL
			controller = BEL
		}
	}
	manpower=1
	state_category = river_crossing
	buildings_max_level_factor=1.000
}
