
state={
	id=680
	name="STATE_680"

	history={
		buildings = {
			infrastructure = 2
			arms_factory = 1
			industrial_complex = 1
			air_base = 1
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 1
				air_base = 1
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 1
				air_base = 1
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 1
				air_base = 1
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 1
				air_base = 1
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 1
				air_base = 1
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 1
				air_base = 1
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 1
				air_base = 1
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = FRA
		add_core_of = SYR
		1940.6.22 = {
			owner = EFR

		}
		1941.7.12 = {
			owner = FRA

		}
		victory_points = {
			3804 1
		}
		victory_points = {
			4475 5
		}
		victory_points = {
			9455 20
		}

	}

	provinces={
		449 744 874 1521 1680 1739 2272 2414 2438 2740 2765 2769 2993 3018 3176 3211 3804 3904 4067 4135 4475 4663 4684 7014 7064 7066 7088 7102 7123 7132 7143 9455 11402
	}
	manpower=547797
	buildings_max_level_factor=1.000
	state_category=large_town
	local_supplies=0.000
}
