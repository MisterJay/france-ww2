
state={
	id=365
	name="STATE_365"
	resources={
		steel=100.000
		aluminium=24.000
	}

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 2
			industrial_complex = 1
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

			906 = { bunker = 3 }
			4840 = { bunker = 3 }
			5841 = { bunker = 3 }
		}
		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 2
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 3
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 3
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 3
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 3
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 3
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 3
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = GER
		add_core_of = GER
		set_demilitarized_zone = yes
		1937.1.1 = {
			set_demilitarized_zone = no

		}
		1945.3.20 = {
			controller = FRA

		}
		victory_points = {
			4961 5 
		}
		victory_points = {
			5739 5 
		}
		victory_points = {
			4323 15 
		}
		victory_points = {
			5786 5 
		}

	}

	provinces={
		906 954 1553 3982 4273 4323 4840 4961 5705 5715 5728 5739 5756 5786 5841 8331 11363 
	}
	manpower=1665256
	buildings_max_level_factor=1.000
	state_category=city
	local_supplies=0.000
}
