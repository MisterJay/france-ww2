
state={
	id=221
	name="STATE_221"

	history={
		buildings = {
			infrastructure = 2
			arms_factory = 0
			industrial_complex = 2
			air_base = 6
			anti_air_building = 0
				dockyard = 2
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 3
				air_base = 6
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 3
				air_base = 6
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 3
				air_base = 6
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 3
				air_base = 6
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 3
				air_base = 6
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 4
				air_base = 6
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 4
				air_base = 6
				anti_air_building = 0
				dockyard = 2
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = ENG
		add_core_of = ENG
		victory_points = {
			11013 10
		}

	}

	provinces={
		904 11013
	}
	manpower=2701200
	buildings_max_level_factor=1.000
	state_category=large_city
	local_supplies=0.000
}
