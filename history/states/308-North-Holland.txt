
state={
	id=308
	name="STATE_308"
	
	history={
		buildings = {
			infrastructure = 4
			arms_factory = 2
			industrial_complex = 4
			dockyard = 2
			air_base = 0
			anti_air_building = 6
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0
			5412 = {
				bunker = 3

			}
			2797 = {
				bunker = 3

			}
			703 = {
				bunker = 8

			}

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 4
				arms_factory = 3
				industrial_complex = 5
				dockyard = 2
				air_base = 0
				anti_air_building = 6
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 4
				arms_factory = 4
				industrial_complex = 6
				dockyard = 2
				air_base = 0
				anti_air_building = 6
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 4
				arms_factory = 4
				industrial_complex = 6
				dockyard = 2
				air_base = 0
				anti_air_building = 6
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 4
				arms_factory = 4
				industrial_complex = 7
				dockyard = 2
				air_base = 0
				anti_air_building = 6
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 4
				arms_factory = 4
				industrial_complex = 7
				dockyard = 2
				air_base = 0
				anti_air_building = 6
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 4
				arms_factory = 4
				industrial_complex = 8
				dockyard = 2
				air_base = 0
				anti_air_building = 6
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 4
				arms_factory = 5
				industrial_complex = 9
				dockyard = 2
				air_base = 0
				anti_air_building = 6
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = HOL
		add_core_of = HOL
		1941.6.22 = {
			owner = GNL
			controller = GNL
			add_core_of = GNL

		}
		buildings = {
			2797 = {
				naval_base = 8

			}

		}
		victory_points = {
			5412 30
		}
		victory_points = {
			5375 5
		}
		victory_points = {
			2797 15
		}
		victory_points = {
			4588 5
		}
		victory_points = {
			11226 5
		}

	}

	provinces={
		703 2797 4588 4894 5371 5375 5384 5392 5412 8458 8463 11226
	}
	manpower=1537865
	buildings_max_level_factor=1.000
	state_category=metropolis
	local_supplies=0.000
}
