
state={
	id=231
	name="STATE_231"

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 4
			industrial_complex = 1
			air_base = 6
			anti_air_building = 0
				dockyard = 1
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 5
				industrial_complex = 4
				air_base = 6
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 5
				industrial_complex = 4
				air_base = 6
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 5
				industrial_complex = 4
				air_base = 6
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 5
				industrial_complex = 4
				air_base = 6
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 5
				industrial_complex = 4
				air_base = 6
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 5
				industrial_complex = 4
				air_base = 6
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 5
				industrial_complex = 4
				air_base = 6
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = GER
		add_core_of = GER
		buildings = {
			825 = {
				naval_base = 4

			}

		}
		1945.3.20 = {
			SOV = {
				set_province_controller = 3945
				set_province_controller = 2599
				set_province_controller = 3277
				set_province_controller = 3822
				set_province_controller = 5242
				set_province_controller = 5232
				set_province_controller = 3769
				set_province_controller = 5224
				set_province_controller = 5225
				set_province_controller = 3504
				set_province_controller = 2833
				set_province_controller = 728
				set_province_controller = 2255
				set_province_controller = 5238
				set_province_controller = 5241
				set_province_controller = 3586
				set_province_controller = 1753
				set_province_controller = 9906
				set_province_controller = 11383

			}

		}
		victory_points = {
			728 5
		}
		victory_points = {
			825 5
		}
		victory_points = {
			3945 2
		}
		victory_points = {
			5235 30
		}
		victory_points = {
			5242 2
		}

	}

	provinces={
		728 825 1753 2255 2599 2833 3277 3504 3586 3769 3822 3945 5224 5225 5229 5232 5235 5238 5241 5242 9906 11383
	}
	manpower=2140234
	buildings_max_level_factor=1.000
	state_category=city
	local_supplies=0.000
}
