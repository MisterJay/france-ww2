
state={
	id=264
	name="STATE_264"

	history={
		buildings = {
			infrastructure = 2
			arms_factory = 0
			industrial_complex = 1
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0
		}

		1940.5.10 = {
			buildings = {
				infrastructure = 2
				arms_factory = 0
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1941.6.21 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 3
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1942.11.19 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 3
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1943.7.11 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 3
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1944.6.6 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}


		1944.12.14 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1945.3.20 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		owner = IRE
		add_core_of = IRE

        victory_points = { 5340 5 }
		victory_points = { 1080 1 }
	}

	provinces={
		1080 1324 1715 3100 5340
	}
	manpower=941400
    state_category = town
	buildings_max_level_factor=1.000
}
