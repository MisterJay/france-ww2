
state={
	id=473
	name="STATE_473"
	resources={
		aluminium=6.000
	}

	history={
		buildings = {
			infrastructure = 4
			arms_factory = 3
			industrial_complex = 4
			air_base = 3
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 4
				arms_factory = 5
				industrial_complex = 6
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 4
				arms_factory = 6
				industrial_complex = 8
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 4
				arms_factory = 6
				industrial_complex = 8
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 4
				arms_factory = 7
				industrial_complex = 9
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 4
				arms_factory = 7
				industrial_complex = 9
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 4
				arms_factory = 8
				industrial_complex = 10
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 4
				arms_factory = 9
				industrial_complex = 11
				air_base = 3
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = ROM
		add_core_of = ROM

	}

	provinces={
		343 856 1050 1119 1168 1201 1273 1942 2095 3194 3205 3665 3728 3766 4057 4098 4769 6222 6272 6344 6384 9492 9774 9780 9781 9787 9792 9795 9797 9798 9803 9804 9969 11344 
	}
	manpower=3122980
	buildings_max_level_factor=1.000
	state_category=metropolis
	local_supplies=0.000
}
