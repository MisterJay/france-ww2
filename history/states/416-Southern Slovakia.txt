
state={
	id=416
	name="STATE_416"
	resources={
		steel=21.000
		chromium=5.000
	}

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 0
			industrial_complex = 1
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0
		}

		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 3
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}


		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 3
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 3
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		owner = CZE
		add_core_of = CZE
		add_core_of = SLO
		add_core_of = HUN
		1939.1.1 = {
			owner = HUN
			controller = HUN

		}

		1944.12.16 = {
			SOV = {
				set_province_controller = 9538
				set_province_controller = 937
				set_province_controller = 9763
			}
		}
		1945.3.20 = {
			SOV = {
				set_province_controller = 9536
				set_province_controller = 2416
				set_province_controller = 9867
			}
		}
        victory_points = { 655 5 }
        victory_points = { 9536 5 }
        victory_points = { 9527 1 }
	}

	provinces={
		655 738 937 2416 4524 9520 9521 9527 9536 9538 9763 9853 9861 9867 9941
	}
	manpower=854300
	buildings_max_level_factor=1.000
	state_category=town
}
