
state={
	id=664
	name="STATE_664"

	history={
		buildings = {
			infrastructure = 2
			arms_factory = 0
			industrial_complex = 0
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0
		}

		1940.5.10 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1941.6.21 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1942.11.19 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1943.7.11 = {
			buildings = {
				infrastructure = 2
				arms_factory = 1
				industrial_complex = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1944.6.6 = {
			buildings = {
				infrastructure = 2
				arms_factory = 2
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}


		1944.12.14 = {
			buildings = {
				infrastructure = 2
				arms_factory = 2
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1945.3.20 = {
			buildings = {
				infrastructure = 2
				arms_factory = 2
				industrial_complex = 2
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		owner = GRE
		add_core_of = GRE
		1941.4.1 = {
			owner = ITA
			controller = ITA

		}
		1944.6.6 = {
			owner = GER
			controller = GER

		}

		1944.12.16 = {
			owner = GRE
			controller = GRE
		}

		buildings = {
			1673 = { naval_base = 2 }
			2794 = { naval_base = 2 }
		}
		victory_points = { 1673 5 }
        victory_points = { 4074 2 }
        victory_points = { 3164 5 }
        victory_points = { 4421 3 }
		victory_points = { 2425 2 }
        victory_points = { 3614 2 }
	}

	provinces={
		1353 1673 1682 1771 2101 2425 2429 2611 2637 2794 3050 3319 3368 3428 3530 3592 3614 4041 4074 4213 4382 4421 4558 4717 4855 4865 6969 6977 6984 6987 6997 7001 7004 7034 7074 7081 7131 7572 7669
	}
	manpower=945000
    state_category = rural
	buildings_max_level_factor=1.000
}
