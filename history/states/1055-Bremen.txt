state={
	id=1055
	name="STATE_1055"
	provinces={
		10002 10006 10012
	}

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 0
			industrial_complex = 2
			air_base = 0
			anti_air_building = 0
				dockyard = 1
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0
		}

		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 1
				industrial_complex = 4
				air_base = 0
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 4
				industrial_complex = 6
				air_base = 0
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 4
				industrial_complex = 6
				air_base = 0
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 4
				industrial_complex = 6
				air_base = 0
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 4
				industrial_complex = 6
				air_base = 0
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}


		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 4
				industrial_complex = 6
				air_base = 0
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 4
				industrial_complex = 6
				air_base = 0
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		owner = GER
		add_core_of = GER

        victory_points = { 10012 30 }
        victory_points = { 10006 10 }
		victory_points = { 10002 3 }
	}
	manpower=465321
    state_category = large_town
	buildings_max_level_factor=1.000
}
