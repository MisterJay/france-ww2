
state={
	id=91
	name="STATE_91"
	resources={
		steel=25.000
	}

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 2
			industrial_complex = 4
			air_base = 4
			anti_air_building = 0
				dockyard = 1
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 3
				industrial_complex = 6
				air_base = 4
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 3
				industrial_complex = 8
				air_base = 4
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 3
				industrial_complex = 9
				air_base = 4
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 3
				industrial_complex = 9
				air_base = 4
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 4
				industrial_complex = 10
				air_base = 4
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 4
				industrial_complex = 11
				air_base = 4
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 4
				industrial_complex = 11
				air_base = 4
				anti_air_building = 0
				dockyard = 1
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = NOR
		add_core_of = NOR
		buildings = {
			4167 = {
				naval_base = 7

			}

		}
		victory_points = {
			4167 25
		}
		victory_points = {
			4268 5
		}
		victory_points = {
			5130 1
		}
		victory_points = {
			2271 3
		}
		victory_points = {
			4299 5
		}
		victory_points = {
			391 1
		}
		victory_points = {
			4482 5
		}
		victory_points = {
			5143 3
		}
		victory_points = {
			5089 2
		}
		victory_points = {
			5104 1
		}
		victory_points = {
			5116 1
		}
		victory_points = {
			10891 5
		}
		victory_points = {
			3063 1
		}
		1940.5.10 = {
			owner = GNO
			add_core_of = GNO

		}

	}

	provinces={
		8 217 391 412 661 684 782 816 1103 1157 1243 1371 1462 1494 1576 1613 1654 1980 1990 2171 2224 2271 2415 2517 2601 2647 2683 2755 2892 2905 2965 3063 3113 3142 3196 3364 3379 3406 3468 3484 3525 3603 3623 3672 3716 3746 3965 4007 4036 4056 4096 4137 4167 4181 4185 4268 4299 4389 4409 4447 4476 4482 4542 4574 4575 4576 4640 4652 4723 4897 4986 5068 5069 5071 5074 5075 5079 5080 5083 5085 5087 5088 5089 5093 5094 5097 5098 5099 5103 5104 5105 5106 5108 5109 5113 5114 5116 5117 5118 5123 5124 5126 5127 5128 5130 5136 5141 5143 5144 5148 5149 5150 5154 5157 5161 5162 10891
	}
	manpower=1410219
	buildings_max_level_factor=1.000
	state_category=city
	local_supplies=0.000
}
