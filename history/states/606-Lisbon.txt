
state={
	id=606
	name="STATE_606"
	resources={
		rubber=290.000
		steel=10.000
	}

	history={
		victory_points = {
			8930 30
		}
		victory_points = {
			8906 5
		}
		victory_points = {
			8914 1
		}
		buildings = {
			infrastructure = 4
			arms_factory = 3
			industrial_complex = 2
			dockyard = 2
			air_base = 5
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 4
				arms_factory = 6
				industrial_complex = 4
				dockyard = 7
				air_base = 5
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 4
				arms_factory = 7
				industrial_complex = 6
				dockyard = 7
				air_base = 5
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 4
				arms_factory = 7
				industrial_complex = 6
				dockyard = 7
				air_base = 5
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 4
				arms_factory = 7
				industrial_complex = 7
				dockyard = 7
				air_base = 5
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 4
				arms_factory = 7
				industrial_complex = 7
				dockyard = 7
				air_base = 5
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 4
				arms_factory = 7
				industrial_complex = 7
				dockyard = 7
				air_base = 5
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 4
				arms_factory = 7
				industrial_complex = 8
				dockyard = 7
				air_base = 5
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = POR
		add_core_of = POR
		buildings = {
			8930 = {
				naval_base = 7

			}

		}

	}

	provinces={
		8906 8914 8930 11189
	}
	manpower=1995800
	buildings_max_level_factor=1.000
	state_category=metropolis
	local_supplies=0.000
}
