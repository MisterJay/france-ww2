
state={
	id=218
	name="STATE_218"

	history={
		buildings = {
			infrastructure = 3
			arms_factory = 0
			industrial_complex = 0
			air_base = 4
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 0
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 0
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 0
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 0
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 0
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 0
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 3
				arms_factory = 0
				industrial_complex = 0
				air_base = 4
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = SOV
		add_core_of = SOV

		1942.11.19 = {
			owner = GMO
			controller = GMO
			SOV = {
				set_province_controller = 10606
				set_province_controller = 10624
				set_province_controller = 1906
				set_province_controller = 1073
				set_province_controller = 10585
			}
		}
		1943.7.11 = {
			SOV = {
				set_province_controller = 10544
				set_province_controller = 10573
				set_province_controller = 1987
				set_province_controller = 10544
			}
		}
		1944.6.6 = {
			controller = SOV
		}
		victory_points = { 10576 10 } #218
		victory_points = { 1073 5 } #218
		victory_points = { 5236 5 } #218
		victory_points = { 123 3 } #218
		victory_points = { 1906 3 } #218
		victory_points = { 5220 1 } #218
		victory_points = { 10551 1 } #218
		victory_points = { 10606 1 } #218
		victory_points = { 1987 1 } #218
		victory_points = { 10573 5 } #218

	}

	provinces={
		123 1073 1906 1987 2028 5220 5236 10544 10551 10573 10576 10585 10593 10606 10617 10624
	}
	manpower=348956
	buildings_max_level_factor=1.000
	state_category=large_town
	local_supplies=0.000
}
