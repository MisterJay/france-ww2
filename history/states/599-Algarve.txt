
state={
	id=599
	name="STATE_599"

	history={
		victory_points = { 8766 1 }
		victory_points = { 8760 1 }
		victory_points = { 9223 5 }
		victory_points = { 8756 1 }
		buildings = {
			infrastructure = 4
			arms_factory = 0
			industrial_complex = 3
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0
		}

		1940.5.10 = {
			buildings = {
				infrastructure = 4
				arms_factory = 0
				industrial_complex = 4
				dockyard = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1941.6.21 = {
			buildings = {
				infrastructure = 4
				arms_factory = 0
				industrial_complex = 4
				dockyard = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1942.11.19 = {
			buildings = {
				infrastructure = 4
				arms_factory = 0
				industrial_complex = 4
				dockyard = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1943.7.11 = {
			buildings = {
				infrastructure = 4
				arms_factory = 0
				industrial_complex = 4
				dockyard = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1944.6.6 = {
			buildings = {
				infrastructure = 4
				arms_factory = 1
				industrial_complex = 4
				dockyard = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}

		1944.12.14 = {
			buildings = {
				infrastructure = 4
				arms_factory = 1
				industrial_complex = 4
				dockyard = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		1945.3.20 = {
			buildings = {
				infrastructure = 4
				arms_factory = 1
				industrial_complex = 4
				dockyard = 1
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0
			}
		}
		owner = POR
		add_core_of = POR
		buildings = {
			9223 = { naval_base = 3 }
		}
	}

	provinces={
		8756 8760 8763 8766 9223
	}
	manpower=346982
	buildings_max_level_factor=1.000
	state_category = town
}
