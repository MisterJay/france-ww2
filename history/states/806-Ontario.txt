
state={
	id=806
	name="STATE_806"

	resources={
		steel=8 # was: 12.000
		aluminium=9 # was: 14.000
	}

	history={
		buildings = {
			infrastructure = 4
			arms_factory = 7
			industrial_complex = 14
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 4
				arms_factory = 11
				industrial_complex = 15
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 4
				arms_factory = 13
				industrial_complex = 16
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 4
				arms_factory = 13
				industrial_complex = 17
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 4
				arms_factory = 13
				industrial_complex = 17
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 4
				arms_factory = 14
				industrial_complex = 18
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 4
				arms_factory = 14
				industrial_complex = 18
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 4
				arms_factory = 14
				industrial_complex = 18
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = CAN
		add_core_of = CAN
		victory_points = {
			8641 20
		}
		victory_points = {
			8657 10
		}

	}

	provinces={
		8641 8654 8665
	}
	manpower=3267017
	buildings_max_level_factor=1.000
	state_category = large_city
	local_supplies=0.000
}
