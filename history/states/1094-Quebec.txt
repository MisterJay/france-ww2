state={
	id=1094
	name="STATE_1094"
	resources={
		aluminium=7.000
	}

	history={
		buildings = {
			infrastructure = 1
			arms_factory = 1
			industrial_complex = 2
			air_base = 4
			dockyard = 1
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

			8656 = {
				naval_base = 2
			}
			8655 = {
				naval_base = 6
			}
		}
		1940.5.10 = {
			buildings = {
				infrastructure = 1
				arms_factory = 2
				industrial_complex = 2
				air_base = 4
			dockyard = 1
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 1
				arms_factory = 2
				industrial_complex = 2
				air_base = 4
			dockyard = 1
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 1
				arms_factory = 2
				industrial_complex = 3
				air_base = 4
			dockyard = 1
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 1
				arms_factory = 2
				industrial_complex = 3
				air_base = 4
			dockyard = 1
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 1
				arms_factory = 2
				industrial_complex = 3
				air_base = 4
			dockyard = 1
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}


		1944.12.14 = {
			buildings = {
				infrastructure = 1
				arms_factory = 3
				industrial_complex = 3
				air_base = 4
			dockyard = 1
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 1
				arms_factory = 3
				industrial_complex = 3
				air_base = 4
			dockyard = 1
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = CAN
		add_core_of = CAN
	}
	provinces={
		8655 8656
	}
	manpower=318860
	buildings_max_level_factor=1.000
	state_category=rural
}
