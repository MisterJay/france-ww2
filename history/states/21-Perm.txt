
state={
	id=21
	name="STATE_21"

	history={
		buildings = {
			infrastructure = 1
			arms_factory = 1
			industrial_complex = 0
			air_base = 0
			anti_air_building = 0
			fuel_silo = 0
			radar_station = 0
			rocket_site = 0

		}
		1940.5.10 = {
			buildings = {
				infrastructure = 1
				arms_factory = 1
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1941.6.21 = {
			buildings = {
				infrastructure = 1
				arms_factory = 1
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1942.11.19 = {
			buildings = {
				infrastructure = 1
				arms_factory = 1
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1943.7.11 = {
			buildings = {
				infrastructure = 1
				arms_factory = 1
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.6.6 = {
			buildings = {
				infrastructure = 1
				arms_factory = 1
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1944.12.14 = {
			buildings = {
				infrastructure = 1
				arms_factory = 1
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		1945.3.20 = {
			buildings = {
				infrastructure = 1
				arms_factory = 1
				industrial_complex = 0
				air_base = 0
				anti_air_building = 0
				fuel_silo = 0
				radar_station = 0
				rocket_site = 0

			}

		}
		owner = SOV
		add_core_of = SOV

		victory_points = { 11132 1 } #21
		victory_points = { 11108 1 } #21
		victory_points = { 209 1 } #21
		victory_points = { 28 1 } #21
		victory_points = { 11123 1 } #21
		victory_points = { 11102 5 } #21
		victory_points = { 3080 1 } #21
		victory_points = { 11098 1 } #21


	}

	provinces={
		28 120 139 209 1128 1602 3080 3495 4681 11068 11085 11098 11102 11108 11123 11124 11132 11153
	}
	manpower=2085000
	buildings_max_level_factor=1.000
	state_category=rural
	local_supplies=0.000
}
