division_template = {
	name = "American Infantry Regiment"			#
	division_names_group = USA_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

		artillery_brigade = { x = 1 y = 0 }
	}
	support = {
       	engineer = { x = 0 y = 0 }
		signal_company = { x = 0 y = 1 }
		maintenance_company = { x = 0 y = 2 } #82nd had maintenance
	}
}

division_template = {
	name = "American Airborne Regiment"  	#

	division_names_group = USA_INF_01

	#These were both paratroopers, gliders, and seaborne troops
	regiments = {
		#Paratroopers
		paratrooper = { x = 0 y = 0 }
		paratrooper = { x = 0 y = 1 }
		paratrooper = { x = 0 y = 2 }
	}
	support = {
		artillery = { x = 0 y = 0 }  # US Hvy Field Arty consisted of 2 Bns, 155mm howitzers
		engineer = { x = 0 y = 1 }   # US Eng Rgt consisted of 2 Bns
		recon = { x = 0 y = 2 } #82nd had a recon company
		maintenance_company = { x = 0 y = 3 } #82nd had maintenance
		anti_tank = { x = 0 y = 3 } #82nd had SPTD's, but as we dont have those as support companies they're getting anti tank
	}
}

division_template = {
	name = "American Armor Regiment"					# American Armour
	division_names_group = USA_ARM_01

	regiments = {
		medium_armor = { x = 0 y = 0 }
		medium_armor = { x = 0 y = 1 }

		medium_sp_artillery_brigade = { x = 1 y = 0 }
		medium_tank_destroyer_brigade = { x = 1 y = 1 }

		mechanized = { x = 2 y = 0 }
	    mechanized = { x = 2 y = 1 }
	}
	support = {
        field_hospital = { x = 0 y = 0 }
		maintenance_company = { x = 0 y = 1 }
		logistics_company = { x = 0 y = 2 }
		engineer = { x = 0 y = 3 }
		signal_company = { x = 0 y = 4 }
	}
	priority = 2
}

units = {
	# USA #
	# Utah
		# 82nd airborne
			division= {
				name = "Force A, 82. Airborne Division"
				location = 8599
				division_template = "American Airborne Regiment"
				start_experience_factor = 0.3
			}
			division= {
				name = "Force B and C, 82. Airborne Division"
				location = 8599
				division_template = "American Airborne Regiment"
				start_experience_factor = 0.3
			}
		#
		# 101st airborne
			division= {
				name = "Force A, 101. Airborne Division"
				location = 8599
				division_template = "American Airborne Regiment"
				start_experience_factor = 0.3
			}
			division= {
				name = "Force B and C, 101. Airborne Division"
				location = 8599
				division_template = "American Airborne Regiment"
				start_experience_factor = 0.3
			}
		#
		# 4th infantry
			division= {
				name = "4. Infantry Division"
				location = 8599
				division_template = "American Infantry Regiment"
				start_experience_factor = 0.3
			}
		#
		# 90th infantry
			division= {
				name = "357. Infantry Regiment, 90. Infantry Division"
				location = 8599
				division_template = "American Infantry Regiment"
				start_experience_factor = 0.3
			}
			division= {
				name = "358. Infantry Regiment, 90. Infantry Division"
				location = 8599
				division_template = "American Infantry Regiment"
				start_experience_factor = 0.3
			}
			division= {
				name = "359. Infantry Regiment, 90. Infantry Division"
				location = 8599
				division_template = "American Infantry Regiment"
				start_experience_factor = 0.3
			}
		#
	#

	# Omaha
		# 1st infantry
			division= {
				name = "16. Infantry Regiment, 1. Infantry Division"
				location = 8598
				division_template = "American Infantry Regiment"
				start_experience_factor = 0.3
			}
			division= {
				name = "18. Infantry Regiment, 1. Infantry Division"
				location = 8598
				division_template = "American Infantry Regiment"
				start_experience_factor = 0.3
			}
			division= {
				name = "116. Infantry Regiment, 1. Infantry Division"
				location = 8598
				division_template = "American Infantry Regiment"
				start_experience_factor = 0.3
			}
		#
		# 29th infantry
			division= {
				name = "115. Infantry Regiment, 29. Infantry Division"
				location = 8598
				division_template = "American Infantry Regiment"
				start_experience_factor = 0.3
			}
			division= {
				name = "175. Infantry Regiment, 29. Infantry Division"
				location = 8598
				division_template = "American Infantry Regiment"
				start_experience_factor = 0.3
			}
			division= {
				name = "26. Infantry Regiment, 29. Infantry Division"
				location = 8598
				division_template = "American Infantry Regiment"
				start_experience_factor = 0.3
			}
		#
	#
}
air_wings = {
	37  = { fighter_equipment_2 = { owner = "FRA"  amount = 1000 } }
	48  = { fighter_equipment_2 = { owner = "FRA"  amount = 1000 } }
	53  = { CAS_equipment_2 = { owner = "FRA"  amount = 1000 } }
	31  = { CAS_equipment_2 = { owner = "FRA" amount = 1000 } }
	33 = { tac_bomber_equipment_2 = { owner = "FRA" amount = 1000 } }
	10 = { CAS_equipment_2 = { owner = "FRA" amount = 200 } }
	10 = { strat_bomber_equipment_2 = { owner = "FRA" amount = 200 } }
}

instant_effect = {
	add_equipment_production = {
		equipment = {
			type = infantry_equipment_3
		}
		requested_factories = 15
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = support_equipment_1
		}
		requested_factories = 5
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = motorized_equipment_1
		}
		requested_factories = 5
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = artillery_equipment_3
		}
		requested_factories = 5
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = anti_tank_equipment_3
		}
		requested_factories = 5
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = fighter_equipment_3
		}
		requested_factories = 5
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = CAS_equipment_3
		}
		requested_factories = 5
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = strat_bomber_equipment_3
		}
		requested_factories = 5
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = transport_plane_equipment_1
		}
		requested_factories = 5
		efficiency = 100
	}
}
