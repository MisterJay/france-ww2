﻿division_template = {
	name = "Frontier District"
	division_names_group = ENG_INF_01
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
	}
}

units = {
	##### Egyptian Frontier Force  #####
	division = {
		name = "1st Egyptian Frontier Force"
		location = 9435
		division_template = "Frontier District"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3

	}
	division = {
		name = "2nd Egyptian Frontier Force"
		location = 7307
		division_template = "Frontier District"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3

	}
	division = {
		name = "3rd Egyptian Frontier Force"
		location = 7475
		division_template = "Frontier District"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3

	}
	division = {
		name = "5th Egyptian Frontier Force"
		location = 2158
		division_template = "Frontier District"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3

	}
	division = {
		name = "6th Egyptian Frontier Force"
		location = 9443
		division_template = "Frontier District"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3

	}
}


instant_effect = {
	add_equipment_production = {
		equipment = {
			type = infantry_equipment_0
			creator = "EGY"
		}
		requested_factories = 1
		progress = 0.12
		efficiency = 100
	}
}
