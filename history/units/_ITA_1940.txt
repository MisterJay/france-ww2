﻿##### Division Templates #####
division_template = {
	name = "Divisione di Fanteria"
	division_names_group = ITA_INF_01
	# Represents regular all infantry divisions
	# In later years, CCNN Brigade added to Inf Division (1x2 militia/inf regiment)
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }

		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }

		artillery_brigade = { x = 3 y = 0 }
		artillery_brigade = { x = 3 y = 1 }
		artillery_brigade = { x = 3 y = 2 }
	}
	support = {
		engineer = { x = 0 y = 0 }
		anti_tank = { x = 0 y = 1 }
		artillery = { x = 0 y = 2 }
	}
}
division_template = {
	name = "Divisione Alpina"				# Divisione Alpina (high experience, best equipment)
	division_names_group = ITA_MNT_01

	regiments = {
		mountaineers = { x = 0 y = 0 }		# Elite Alpina Regiments
		mountaineers = { x = 0 y = 1 }
		mountaineers = { x = 0 y = 2 }

		mountaineers = { x = 1 y = 0 }
		mountaineers = { x = 1 y = 1 }
		mountaineers = { x = 1 y = 2 }

		artillery_brigade = { x = 2 y = 0 }
		artillery_brigade = { x = 2 y = 1 }
	}
	support = {
		engineer = { x = 0 y = 0 }
       	field_hospital = { x = 0 y = 1 }
	}
	priority = 2
}
division_template = {
	name = "Gruppa d'Alpina"				# Divisione Alpina (high experience, best equipment)
	division_names_group = ITA_MNT_01

	regiments = {
		mountaineers = { x = 0 y = 0 }		# Elite Alpina Regiments
		mountaineers = { x = 0 y = 1 }

		mountaineers = { x = 1 y = 0 }
		mountaineers = { x = 1 y = 1 }

		artillery_brigade = { x = 2 y = 0 }
		artillery_brigade = { x = 2 y = 1 }
	}
	support = {
		engineer = { x = 0 y = 0 }
       	field_hospital = { x = 0 y = 1 }
		artillery = { x = 0 y = 2 }
	}
	priority = 2
}
division_template = {
	name = "Divisione Africana di Autotrasportabile"		# Divisione Autotrasportabile (North Africa)
	# Note: semi-motorized regular infantry in North Africa
	division_names_group = ITA_MOT_01
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		mot_artillery_brigade = { x = 2 y = 0 }
		mot_artillery_brigade = { x = 2 y = 1 }
		mot_artillery_brigade = { x = 2 y = 2 }
		light_armor = { x = 3 y = 0 }		# L3/35 tankettes
	}
	support = {
		engineer = { x = 0 y = 0 }
		anti_tank = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Divisione Autotrasportabile"		# Divisione Autotrasportabile (North Africa)
	# Note: semi-motorized regular infantry in North Africa
	division_names_group = ITA_MOT_01
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }

		mot_artillery_brigade = { x = 2 y = 0 }
		mot_artillery_brigade = { x = 2 y = 1 }
		mot_artillery_brigade = { x = 2 y = 2 }
	}
	support = {
		engineer = { x = 0 y = 0 }
		anti_tank = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Divisione Motorizzata"			# Divisione Motorizzata, first raised in 1939
	division_names_group = ITA_MOT_01

	regiments = {
		motorized = { x = 0 y = 0 }
		motorized = { x = 0 y = 1 }
		motorized = { x = 1 y = 0 }
		motorized = { x = 1 y = 1 }
		motorized = { x = 2 y = 0 }
		motorized = { x = 2 y = 1 }
		motorized = { x = 2 y = 2 }

		mot_artillery_brigade = { x = 3 y = 0 }
		mot_artillery_brigade = { x = 3 y = 1 }
		mot_artillery_brigade = { x = 3 y = 2 }
	}
	support = {
		engineer = { x = 0 y = 0 }
		anti_air = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Reggimento Bersaglieri" 			# Divisione Celere (Fast Division)
	division_names_group = ITA_CAV_02

	regiments = {
		bicycle_battalion = { x = 0 y = 0 }
		bicycle_battalion = { x = 0 y = 1 }
		bicycle_battalion = { x = 0 y = 2 }
	}
	priority = 1
}
division_template = {
	name = "Divisione di Cavalleria" 			# Cavalry Division
	division_names_group = ITA_CAV_02

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 0 y = 2 }

		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
		cavalry = { x = 1 y = 2 }

		cavalry = { x = 2 y = 0 }
		cavalry = { x = 2 y = 1 }
		cavalry = { x = 2 y = 2 }

	}
	support = {
		engineer = { x = 0 y = 0 }
	}
	priority = 1
}
division_template = {
	name = "Divisione Celere" 			# Divisione Celere (Fast Division)
	division_names_group = ITA_CAV_02

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		bicycle_battalion = { x = 0 y = 2 }

		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
		bicycle_battalion = { x = 1 y = 2 }

		light_armor = { x = 2 y = 0 }

		mot_artillery_brigade = { x = 3 y = 0 }
		mot_artillery_brigade = { x = 3 y = 1 }
		mot_artillery_brigade = { x = 3 y = 2 }
	}
	support = {
		engineer = { x = 0 y = 0 }
		anti_air = { x = 0 y = 1 }
	}
	priority = 2
}
division_template = {
	name = "Divisione Corazzata" 			# Divisione Corazzata
	division_names_group = ITA_ARM_01

	regiments = {
		light_armor = { x = 0 y = 0 }
		light_armor = { x = 0 y = 1 }
		light_armor = { x = 1 y = 0 }
		light_armor = { x = 1 y = 1 }

		motorized = { x = 2 y = 0 }
		motorized = { x = 2 y = 1 }
		motorized = { x = 2 y = 2 }

		mot_artillery_brigade = { x = 3 y = 0 }
		mot_artillery_brigade = { x = 3 y = 1 }
		mot_artillery_brigade = { x = 3 y = 2 }
	}
	support = {
		engineer = { x = 0 y = 0 }
		anti_tank = { x = 0 y = 1 }
	}
	priority = 2
}
division_template = {
	name = "Reggimento Corazzato" 			# Divisione Corazzata
	division_names_group = ITA_ARM_01

	regiments = {
		bicycle_battalion = { x = 0 y = 0 }
		bicycle_battalion = { x = 0 y = 1 }
		bicycle_battalion = { x = 0 y = 2 }
		light_armor = { x = 1 y = 0 }
		light_armor = { x = 1 y = 1 }
		light_armor = { x = 1 y = 2 }
	}
	support = {
		engineer = { x = 0 y = 0 }
	}
	priority = 2
}
division_template = {
	name = "Raggruppamento di Cavalleria" 				# Raggruppamento Celere (less equipped than frontline Celere)
	division_names_group = ITA_CAV_02
	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 0 y = 2 }
		bicycle_battalion = { x = 1 y = 0 }
		bicycle_battalion = { x = 1 y = 1 }
		bicycle_battalion = { x = 1 y = 2 }
	}
	support = {
		engineer = { x = 0 y = 0 }
		anti_air = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Raggruppamento Celere" 				# Raggruppamento Celere (less equipped than frontline Celere)
	division_names_group = ITA_CAV_02
	regiments = {
		light_armor = { x = 0 y = 0 }
		light_armor = { x = 0 y = 1 }
		light_armor = { x = 0 y = 2 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
		cavalry = { x = 1 y = 2 }
		bicycle_battalion = { x = 2 y = 0 }
		bicycle_battalion = { x = 2 y = 1 }
		bicycle_battalion = { x = 2 y = 2 }
	}
	support = {
		engineer = { x = 0 y = 0 }
		anti_air = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Divisione Coloniale"
	# Represents lesser-equipped colonial units
	division_names_group = ITA_COL_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }

		artillery_brigade = { x = 2 y = 0 }
		artillery_brigade = { x = 2 y = 1 }
	}
	support = {
		engineer = { x = 0 y = 0 }
		anti_air = { x = 0 y = 1 }
		anti_tank = { x = 0 y = 2 }
	}
	priority = 0
}

units = {
	# Army Group West #
		# Army 1 #
			# Corps 2 #
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 5
					} # "4a Divisione 'Livorno'"
					location = 1410
					division_template = "Divisione Alpina"		# "Semi-Mountain" (less trained & equipped than Alpina)
					start_experience_factor = 0.2
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 33
					} # "33a Divisione 'Acqui'"
					location = 6471
					division_template = "Divisione Alpina"		# "Semi-Mountain" (less trained & equipped than Alpina)
					start_experience_factor = 0.2
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 36
					} # "36a Divisione 'Forli'"
					location = 3908
					division_template = "Divisione Alpina"		# "Semi-Mountain" (less trained & equipped than Alpina)
					start_experience_factor = 0.2
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 4
					} # "4a Divisione Alpina 'Cuneense'"
					location = 6433
					division_template = "Divisione Alpina"
					start_experience_factor = 0.3
				}
				division = {
					name = "2a Gruppa d'Alpina"
					location = 2148
					division_template = "Gruppa d'Alpina"
					start_experience_factor = 0.2
					start_equipment_factor = 0.6
				}
			#
			# Corps 3 #
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 3
					} # "3a Divisione 'Ravenna'"
					location = 6524
					division_template = "Divisione Alpina"		# "Semi-Mountain" (less trained & equipped than Alpina)
					start_experience_factor = 0.2
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 6
					} # "6a Divisione 'Cuneo'"
					location = 3047
					division_template = "Divisione di Fanteria"		# "Semi-Mountain" (less trained & equipped than Alpina)
					start_experience_factor = 0.3
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 4
					}
					location = 2148
					division_template = "Divisione di Fanteria"		# "Semi-Mountain" (less trained & equipped than Alpina)
					start_experience_factor = 0.3
				}
				division = {
					name = "1a Gruppa d'Alpina"
					location = 2956
					division_template = "Gruppa d'Alpina"
					start_experience_factor = 0.2
					start_equipment_factor = 0.6
				}
			#
			# Corps 15 #
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 5
					} # "5a Divisione 'Cosseria'"
					location = 6574
					division_template = "Divisione di Fanteria"
					start_experience_factor = 0.3
					start_equipment_factor = 0.7
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 44
					} # "44a Divisione 'Cremona'"
					location = 6601
					division_template = "Divisione di Fanteria"
					start_experience_factor = 0.2
					start_equipment_factor = 0.7
				}

				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 37
					} # "37a Divisione 'Modena'"
					location = 3307
					division_template = "Divisione Alpina"		# "Semi-Mountain" (less trained & equipped than Alpina)
					start_experience_factor = 0.2
				}
			#

			division = {
				division_name = {
					is_name_ordered = yes
					name_order = 7
				} # "7a Divisione 'Lupi di Toscana'"
				location = 6543
				division_template = "Divisione di Fanteria"
				start_experience_factor = 0.2
			}
			division = {
				division_name = {
					is_name_ordered = yes
					name_order = 16
				} # "16a Div. 'Cacciatori delle Alpi'"
				location = 4569
				division_template = "Divisione di Fanteria"
				start_experience_factor = 0.2
			}
			division = {
				division_name = {
					is_name_ordered = yes
					name_order = 22
				} # "22a Div. 'Cacciatori delle Alpi'"
				location = 4416
				division_template = "Divisione di Fanteria"
				start_experience_factor = 0.2
			}
			division = {
				division_name = {
					is_name_ordered = yes
					name_order = 5
				} # "5a Divisione Alpina 'Pusteria'"
				location = 6450
				division_template = "Divisione Alpina"
				start_experience_factor = 0.3
			}
			division = {
				name = "1a Raggruppamento Celere"
				location = 6450
				division_template = "Raggruppamento di Cavalleria"
				start_experience_factor = 0.2
				start_equipment_factor = 0.7
			}
		#
		# Army 4 #
			# Corps 1 #
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 1
					} # "1a Divisione 'Superga'"
					location = 4794
					division_template = "Divisione Alpina"		# "Semi-Mountain" (less trained & equipped than Alpina)
					start_experience_factor = 0.2
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 24
					} # "24a Divisione 'Pinerolo'"
					location = 6377
					division_template = "Divisione di Fanteria"
					start_experience_factor = 0.3
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 59
					} # "59a Divisione 'Superga'"
					location = 6351
					division_template = "Divisione Alpina"		# "Semi-Mountain" (less trained & equipped than Alpina)
					start_experience_factor = 0.2
				}
			#
			# Corps 4 #
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 2
					} # "2a Divisione 'Sforzesca'"
					location = 3908
					division_template = "Divisione Alpina"		# "Semi-Mountain" (less trained & equipped than Alpina)
					start_experience_factor = 0.2
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 26
					} # "26a Divisione 'Assietta'"
					location = 6433
					division_template = "Divisione Alpina"		# "Semi-Mountain" (less trained & equipped than Alpina)
					start_experience_factor = 0.2
				}
				division = {
					name = "3a Gruppa d'Alpina"
					location = 6433
					division_template = "Gruppa d'Alpina"
					start_experience_factor = 0.2
					start_equipment_factor = 0.6
				}
			#
			# Corps Alpine (Alpini) #
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 1
					} # "1a Divisione Alpina 'Taurinense'"
					location = 4340
					division_template = "Divisione Alpina"
					start_experience_factor = 0.4
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 2
					} # "2a Divisione Alpina 'Tridentina'"
					location = 1660
					division_template = "Divisione Alpina"
					start_experience_factor = 0.4
				}
				division = {
					name = "Gruppa d'Alpina Levanna"
					location = 6351
					division_template = "Gruppa d'Alpina"
					start_experience_factor = 0.2
					start_equipment_factor = 0.6
				}
			#
			division = {
				division_name = {
					is_name_ordered = yes
					name_order = 11
				} # "11a Divisione 'Brennero'"
				location = 6450
				division_template = "Divisione di Fanteria"
				start_experience_factor = 0.2
				start_equipment_factor = 0.7
			}
			division = {
				division_name = {
					is_name_ordered = yes
					name_order = 58
				} # "58a Divisione 'Legnano'"
				location = 6450
				division_template = "Divisione di Fanteria"
				start_experience_factor = 0.2
				start_equipment_factor = 0.7
			}
			division = {
				division_name = {
					is_name_ordered = yes
					name_order = 4
				} # "4a Raggruppamento Celere"
				location = 6342
				division_template = "Raggruppamento Celere"
				start_experience_factor = 0.2
				start_equipment_factor = 0.7
			}
		#
		# Army 7 #
			# Corps 7 #
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 20
					} # "20a Divisione 'Friuli'"
					location = 4416
					division_template = "Divisione di Fanteria"
					start_experience_factor = 0.2
					start_equipment_factor = 0.7
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 41
					} # "41a Divisione 'Firenze'"
					location = 6496
					division_template = "Divisione di Fanteria"
					start_experience_factor = 0.2
					start_equipment_factor = 0.7
				}
			#
			# Corps 8 #
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 21
					} # "21a Div. 'Granatieri di Sardegna'"
					location = 4569
					division_template = "Divisione di Fanteria"
					start_experience_factor = 0.2
					start_equipment_factor = 0.7
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 51
					} # "51a Divisione 'Siena'"
					location = 4569
					division_template = "Divisione di Fanteria"
					start_experience_factor = 0.2
					start_equipment_factor = 0.7
				}
			#
		#
	#
	# Army Group East #
		# Army 2 #
			# Corps 5 #
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 12
					} # "12a Divisione 'Sassari'"
					location = 6390
					division_template = "Divisione di Fanteria"
					start_experience_factor = 0.2
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 15
					} # "15a Divisione 'Bergamo'"
					location = 6390
					division_template = "Divisione di Fanteria"
					start_experience_factor = 0.2
				}

				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 57
					} # "57a Divisione 'Lombardia'"
					location = 3001
					division_template = "Divisione di Fanteria"
					start_experience_factor = 0.2
					start_equipment_factor = 0.7
				}
			#
			# Corps 11 #
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 13
					} # "13a Divisione 'Re'"
					location = 2720
					division_template = "Divisione Alpina"		# "Semi-Mountain" (less trained & equipped than Alpina)
					start_experience_factor = 0.2
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 14
					} # "14a Divisione 'Isonzo'"
					location = 4763
					division_template = "Divisione Alpina"		# "Semi-Mountain" (less trained & equipped than Alpina)
					start_experience_factor = 0.2
				}
				division = {
					name = "Gruppa d'Alpina Alto Isonzo"
					location = 2354
					division_template = "Gruppa d'Alpina"
					start_experience_factor = 0.2
					start_equipment_factor = 0.6
				}
				division = {
					name = "2a Reggimento Bersaglieri"
					location = 6322
					division_template = "Reggimento Bersaglieri"
					start_experience_factor = 0.2
					start_equipment_factor = 0.8
				}
				division = {
					name = "3a Divisione di Cavalleria"
					location = 4316
					division_template = "Divisione di Cavalleria"
					start_experience_factor = 0.2
					start_equipment_factor = 0.8
				}
			#
		#
		# Army 6 #
			# Corps Celere #
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 1
					} # "1a Divisione Celere 'Eugenio di Savoia'"
					location = 6266
					division_template = "Divisione Celere"
					start_experience_factor = 0.3
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 2
					} # "2a Divisione Celere 'Emanuele Filiberto'"
					location = 6302
					division_template = "Divisione Celere"
					start_experience_factor = 0.3
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 3
					} # "3a Divisione Celere 'Principe Amedeo'"
					location = 4718
					division_template = "Divisione Celere"
					start_experience_factor = 0.3
				}
			#
			# Corps Corazzato #
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 101
					} # "101a Divisione 'Trieste'"
					location = 6431
					division_template = "Divisione Motorizzata"
					start_experience_factor = 0.3
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 102
					} # "102a Divisione 'Trento'"
					location = 4985
					division_template = "Divisione Motorizzata"
					start_experience_factor = 0.3
				}
				# Corpo d'Armata Corazzatta (CO: Dall'Ora) #
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 132
					} # "132a Divisione 'Ariete'"
					location = 4018
					division_template = "Divisione Corazzata"
					start_experience_factor = 0.3
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 133
					} # "133a Divisione 'Littorio'"
					location = 3709
					division_template = "Divisione Corazzata"
					start_experience_factor = 0.3
				}
			#
			# Corps Autotrasportabile #
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 9
					} # "9a Divisione 'Pasubio'"
					location = 3916
					division_template = "Divisione Autotrasportabile"
					start_experience_factor = 0.3
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 10
					} # "9a Divisione 'Pasubio'"
					location = 3916
					division_template = "Divisione Autotrasportabile"
					start_experience_factor = 0.3
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 52
					} # "9a Divisione 'Pasubio'"
					location = 6373
					division_template = "Divisione Autotrasportabile"
					start_experience_factor = 0.3
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 3
					} # "9a Divisione 'Pasubio'"
					location = 6404
					division_template = "Reggimento Corazzato"
					start_experience_factor = 0.3
				}
			#
		#
		# Army 8 #
			# Corps 6 #
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 18
					} # "18a Divisione 'Messina'"
					location = 2753
					division_template = "Divisione di Fanteria"
					start_experience_factor = 0.2
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 49
					} # "49a Divisione 'Parma'"
					location = 6354
					division_template = "Divisione Alpina"		# "Semi-Mountain" (less trained & equipped than Alpina)
					start_experience_factor = 0.2
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 56
					} # "56a Divisione 'Casale'"
					location = 2143
					division_template = "Divisione di Fanteria"
					start_experience_factor = 0.2
					start_equipment_factor = 0.7
				}
			#
			# Corps 14 #
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 32
					} # "32a Divisione 'Marche'"
					location = 2351
					division_template = "Divisione Alpina"		# "Semi-Mountain" (less trained & equipped than Alpina)
					start_experience_factor = 0.2
				}
				# XI Corpo d'Armata (CO: Roux) #
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 38
					} # "38a Divisione 'Puglie'"
					location = 2093
					division_template = "Divisione Alpina"		# "Semi-Mountain" (less trained & equipped than Alpina)
					start_experience_factor = 0.2
				}
			#
		#

	#
	# Army Group South #
		# Army 3 #
			# Corps 9 #
				# IV Corpo d'Armata (CO: Mercalli) #
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 29
					} # "29a Divisione 'Piemonte'"
					location = 3331
					division_template = "Divisione di Fanteria"
					start_experience_factor = 0.3
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 47
					} # "47a Divisione 'Bari'"
					location = 6829
					division_template = "Divisione di Fanteria"
					start_experience_factor = 0.2
					start_equipment_factor = 0.7
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 48
					} # "48a Divisione 'Taro'"
					location = 6841
					division_template = "Divisione di Fanteria"
					start_experience_factor = 0.2
					start_equipment_factor = 0.7
				}
			#
			division = {
				division_name = {
					is_name_ordered = yes
					name_order = 4
				} # "9a Divisione 'Pasubio'"
				location = 6773
				division_template = "Reggimento Corazzato"
				start_experience_factor = 0.3
			}
		#
		# Corps Sicily #
			division = {
				division_name = {
					is_name_ordered = yes
					name_order = 28
				} # "47a Divisione 'Bari'"
				location = 7022
				division_template = "Divisione di Fanteria"
				start_experience_factor = 0.2
				start_equipment_factor = 0.7
			}
			division = {
				division_name = {
					is_name_ordered = yes
					name_order = 54
				} # "48a Divisione 'Taro'"
				location = 4913
				division_template = "Divisione di Fanteria"
				start_experience_factor = 0.2
				start_equipment_factor = 0.7
			}
			division = {
				name = "10a Reggimento Bersaglieri"
				location = 2588
				division_template = "Reggimento Bersaglieri"
				start_experience_factor = 0.2
				start_equipment_factor = 0.8
			}
		#
		# Corps Sardinia #
			division = {
				division_name = {
					is_name_ordered = yes
					name_order = 30
				} # "30a Divisione 'Sabauda'"
				location = 6805
				division_template = "Divisione di Fanteria"
				start_experience_factor = 0.3
				start_equipment_factor = 0.7
			}
			division = {
				division_name = {
					is_name_ordered = yes
					name_order = 31
				} # "31a Divisione 'Calabria'"
				location = 6947
				division_template = "Divisione di Fanteria"
				start_experience_factor = 0.2
				start_equipment_factor = 0.7
			}
		#

	#
	# Army Group North Africa #
		# Army 5 #
			# Corps 10 #
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 25
					} # "25a Divisione 'Bologna'"
					location = 3512
					division_template = "Divisione Africana di Autotrasportabile"
					start_experience_factor = 0.3
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 55
					} # "55a Divisione 'Savona'"
					location = 1855
					division_template = "Divisione Africana di Autotrasportabile"
					start_experience_factor = 0.3
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 60
					} # "60a Divisione 'Sabratha'"
					location = 1057
					division_template = "Divisione Africana di Autotrasportabile"
					start_experience_factor = 0.3
					start_equipment_factor = 0.85

				}
			#
			# Corps 20 #
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 17
					} # "17a Divisione 'Pavia'"
					location = 9430
					division_template = "Divisione Africana di Autotrasportabile"
					start_experience_factor = 0.3
					start_equipment_factor = 0.85

				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 27
					} # "27a Divisione 'Brescia'"
					location = 9447
					division_template = "Divisione Africana di Autotrasportabile"
					start_experience_factor = 0.3
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 61
					} # "61a Divisione 'Sirte'"
					location = 9446
					division_template = "Divisione Africana di Autotrasportabile"
					start_experience_factor = 0.2
					start_equipment_factor = 0.7
				}
			#
			# Corps 23 #
				division = {
					name = "1a Divisione CC.NN. '23 Marzo'"
					location = 9445
					division_template = "Divisione di Fanteria"
					start_experience_factor = 0.1
					start_equipment_factor = 0.7
				}
				division = {
					name = "2a Divisione CC.NN. '28 Ottobre'"
					location = 9440
					division_template = "Divisione di Fanteria"
					start_experience_factor = 0.1
					start_equipment_factor = 0.7
				}
			#
			division = {
				division_name = {
					is_name_ordered = yes
					name_order = 2
				} # "2a Divisione Coloniale 'Libia'"
				location = 9437
				division_template = "Divisione Coloniale"				# Colonial militia, lower training and equipment
				force_equipment_variants = { infantry_equipment_0 = { owner = "ITA" } }
				start_equipment_factor = 0.7
			}

			division = {
				division_name = {
					is_name_ordered = yes
					name_order = 3
				} # "Piazzafotre di Tripoli"
				location = 9445
				division_template = "Divisione Coloniale"				# Colonial militia, lower training and equipment
				force_equipment_variants = { infantry_equipment_0 = { owner = "ITA" } }
				start_equipment_factor = 0.5
			}
		#
		# Army 10 #
			# Corps 21 #
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 62
					} # "62a Divisione 'Mamarica'"
					location = 2882
					division_template = "Divisione Africana di Autotrasportabile"
					start_experience_factor = 0.2
					start_equipment_factor = 0.7
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 63
					} # "63a Divisione 'Cirene'"
					location = 534
					division_template = "Divisione Africana di Autotrasportabile"
					start_experience_factor = 0.2
					start_equipment_factor = 0.7
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 7
					} # "63a Divisione 'Cirene'"
					location = 7377
					division_template = "Reggimento Corazzato"
					start_experience_factor = 0.2
					start_equipment_factor = 0.7
				}
			#
			# Corps 22 #
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 64
					} # "64a Divisione 'Catanzaro'"
					location = 7392
					division_template = "Divisione Autotrasportabile"
					start_experience_factor = 0.2
					start_equipment_factor = 0.7
				}
				division = {
					division_name = {
						is_name_ordered = yes
						name_order = 1
					} # "1a Divisione Libia"
					location = 2377
					division_template = "Divisione Coloniale"				# Colonial militia, lower training and equipment
					force_equipment_variants = { infantry_equipment_0 = { owner = "ITA" } }
					start_equipment_factor = 0.7
				}
				division = {
					name = "4a Divisione CC.NN. '3 Gennaio'"
					location = 2347
					division_template = "Divisione di Fanteria"
					start_experience_factor = 0.1
					start_equipment_factor = 0.7
				}
			#

			division = {
				name = "Piazzafotre di Tobruch"
				location = 9451
				division_template = "Divisione Coloniale"				# Colonial militia, lower training and equipment
				force_equipment_variants = { infantry_equipment_0 = { owner = "ITA" } }
				start_equipment_factor = 0.4
			}
		#
		# Corps Sahara #
			division = {
				name = "I Battaglione Sahariano"
				location = 7359
				division_template = "Divisione Coloniale"				# Colonial militia, lower training and equipment
				force_equipment_variants = { infantry_equipment_0 = { owner = "ITA" } }
				start_equipment_factor = 0.7
			}
		#

	#
	# Army Corps Albania #
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 19
			} # "19a Divisione 'Venezia'"
			location = 6862
			division_template = "Divisione Alpina"		# "Semi-Mountain" (less trained & equipped than Alpina)
			start_experience_factor = 0.2
		}
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 23
			} # "23a Divisione 'Ferrara'"
			location = 2488
			division_template = "Divisione di Fanteria"
			start_experience_factor = 0.2
		}
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 2597
			} # "53a Divisione 'Arezzo'"
			location = 29
			division_template = "Divisione di Fanteria"
			start_experience_factor = 0.2
		}
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 3
			} # "3a Divisione Alpina 'Julia'"
			location = 1141
			division_template = "Divisione Alpina"
			start_experience_factor = 0.3
		}
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 131
			} # "131a Divisione 'Centauro'"
			location = 6818
			division_template = "Divisione Corazzata"
			start_experience_factor = 0.3
		}
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 7
			}
			location = 869
			division_template = "Divisione di Cavalleria"
			start_experience_factor = 0.2
		}
	#
	# Army Corps Aegean #
		division = {
			division_name = {
				is_name_ordered = yes
				name_order = 50
			} # "50a Divisione 'Regina'"
			location = 4471
			division_template = "Divisione di Fanteria"
			start_experience_factor = 0.2
			start_equipment_factor = 0.6

		}
	#
}

### Air Wings ###
air_wings = {
	593 = { 																# Milan
		### 1a Squadra Aerea (CO: Fougier) ###
		fighter_equipment_0 = { owner = "ITA" amount = 84 }					# 2a Divisione 'Borea' -- CR.42 (REVISIT -- VARIANT)
		tac_bomber_equipment_1 =  { owner = "ITA" amount = 123 }				# 4a Divisione Aerea 'Drago' -- BR.20
						# 6a Divisione Aerea 'Falco' -- BR.20
	}
	593 = {																	# Sicily
		### 2a Squadra Aerea -(CO: Lalli) ###
		fighter_equipment_0 = { owner = "ITA" amount = 84 }					# 1a Divisione Aerea 'Aquila' -- CR.42 (REVISIT -- VARIANT)
		tac_bomber_equipment_1 =  { owner = "ITA" amount = 136 }				# 3a Divisione Aerea 'Centauro' -- SM.79
						# 11a Brigada Aerea 'Nibbio' -- SM.79
	}
	593 = {																	# Rome
		### 3a Squadra Aerea (CO: Pelligrini) ###
		fighter_equipment_1 = { owner = "ITA" amount = 96 }					# 8a Brigada Aerea 'Ibis' -- G.50
		tac_bomber_equipment_1 =  { owner = "ITA" amount = 88 }				# 5a Divisione Aerea 'Eolo' -- SM.79
	}

	593 = {																	# Bari
		fighter_equipment_0 = { owner = "ITA" amount = 54 }					# 9a/I. Divisione Aerea 'Leone' -- CR.32
		tac_bomber_equipment_0 =  { owner = "ITA" amount = 24 }				# 9a/II. Divisione Aerea 'Leone' -- SM.79
		nav_bomber_equipment_1 = { owner = "ITA" amount = 24 }				# 9a/III. Divisione Aerea 'Leone' -- CANT.Z.506B
	}
	593 = {																	# Sardinia
		tac_bomber_equipment_1 =  { owner = "ITA" amount = 64 }				# 10a/I. Divisione Aerea 'Marte' -- SM.79
		nav_bomber_equipment_1 = { owner = "ITA" amount = 24 }				# 10a/II. Divisione Aerea 'Marte' -- CANT.Z.506B
	}
	593 = {																	# Albania
		fighter_equipment_0 =  { owner = "ITA" amount = 24 }				# CR.42 (REVISIT -- VARIANT)
		tac_bomber_equipment_0 = { owner = "ITA" amount = 32 }				# 10a/II. Divisione Aerea 'Marte' -- CANT.Z.506B
	}
	593 = {																	# Dodecanese
		tac_bomber_equipment_0 = { owner = "ITA" amount = 24 }				# SM.81
	}

	### Commando Aeronautica Libia (CO: Porro) ###
	593 = {																	# Tripoli
		### Settore Ovest (CO: Barbarino) ###
		fighter_equipment_0 = { owner = "ITA" amount = 54 }					# CR.32
		tac_bomber_equipment_1 =  { owner = "ITA" amount = 72 }				# SM.79
		CAS_equipment_1 =  { owner = "ITA" amount = 36 }					# Ba.65
	}
	593 = {																	# Benghazi
		### Settore Est (CO: Silvestri) ###
		tac_bomber_equipment_1 =  { owner = "ITA" amount = 60 }				# 13a Brigada Aerea 'Pegaso' -- SM.79
	}

	### Commando Aeronautica Africa Orientale (CO: Pinna) ###
	593 = {																	# Eritrea
		### Settore Nord (CO: Piacentini) ###
		fighter_equipment_0 = { owner = "ITA" amount = 45 }					# CR.42 (REVISIT -- VARANT)
		tac_bomber_equipment_0 =  { owner = "ITA" amount = 54 }				# Ca.133
	}
	593 = {																	# Ethiopia
		### Settore Centro (CO: Collati) ###
		tac_bomber_equipment_0 =  { owner = "ITA" amount = 60 }				# Ca.133
	}
}


	#########################
	## STARTING PRODUCTION ##
	#########################
instant_effect = {

	add_equipment_production = {
		equipment = {
			type = infantry_equipment_1
			creator = "ITA"
		}
		requested_factories = 10
		progress = 0.1
		efficiency = 100
	}

	add_equipment_production = {
		equipment = {
			type = support_equipment_1
			creator = "ITA"
		}
		requested_factories = 4
		progress = 0.3
		efficiency = 100
	}

	add_equipment_production = {
		equipment = {
			type = light_tank_equipment_2
			creator = "ITA"
		}
		requested_factories = 1
		progress = 0.4
		efficiency = 100
	}

	add_equipment_production = {
		equipment = {
			type = motorized_equipment_1
			creator = "ITA"
		}
		requested_factories = 2
		progress = 0.4
		efficiency = 100
	}

	add_equipment_production = {
		equipment = {
			type = fighter_equipment_2
			creator = "ITA"
		}
		requested_factories = 2
		progress = 0.2
		efficiency = 100
	}

	add_equipment_production = {
		equipment = {
			type = CAS_equipment_1
			creator = "ITA"
		}
		requested_factories = 2
		progress = 0.2
		efficiency = 100
	}
}
	#####################
