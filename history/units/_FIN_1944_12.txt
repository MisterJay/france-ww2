﻿##### Division Templates #####
division_template = {
	name = "Jalkaväkidivisioona"		# Infantry Division

	division_names_group = FIN_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }

		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }

		artillery_brigade = { x = 3 y = 0 }
		artillery_brigade = { x = 3 y = 1 }
		artillery_brigade = { x = 3 y = 2 }
	}
	support = {
		engineer = { x = 0 y = 0 }
		signal_company = { x = 0 y = 1 }
       	field_hospital = { x = 0 y = 2 }
		anti_tank = { x = 0 y = 3 }
	}
}
division_template = {
	name = "Jalkaväkiprikaatti"		# Infantry Brigade

	division_names_group = FIN_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }

		artillery_brigade = { x = 3 y = 0 }
		artillery_brigade = { x = 3 y = 1 }
	}
	support = {
		engineer = { x = 0 y = 0 }
		signal_company = { x = 0 y = 1 }
       	field_hospital = { x = 0 y = 2 }
		anti_tank = { x = 0 y = 3 }
	}
}
division_template = {
	name = "Erillinen Prikaati" 	# Separate Brigade - Represents groups of separate battalions (Erillinen Pataljoona) and various garrison units

	division_names_group = FIN_GAR_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
	}
	support = {
		engineer = { x = 0 y = 0 }
	}
	priority = 0
}
division_template = {
	name = "Ratsuväkiprikaati"  		# Cavalry Brigade

	division_names_group = FIN_CAV_01

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 1 y = 0 }
	}
	support = {
		recon = { x = 0 y = 0 }
	}
}

division_template = {
	name = "Jääkäriprikaati"					# Jaeger brigade, armoured
	division_names_group = FIN_CAV_01

	regiments = {
		bicycle_battalion = { x = 0 y = 0 }
		bicycle_battalion = { x = 0 y = 1 }
		bicycle_battalion = { x = 0 y = 2 }

		light_armor = { x = 1 y = 0 }
	}
	support = {
		signal_company = { x = 0 y = 0 }
		anti_tank = { x = 0 y = 1 }
	}
}


##### OOB #####
units = {
	# Demobilized
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		location = 2006
		division_template = "Jalkaväkidivisioona"
		start_experience_factor = 0.3
		start_equipment_factor = 0.8
	}
}


### Starting Production ###
instant_effect = {
	add_equipment_production = {
		equipment = {
			type = infantry_equipment_1
			creator = "FIN"
		}
		requested_factories = 1
		progress = 0.66
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = support_equipment_1
			creator = "FIN"
		}
		requested_factories = 1
		progress = 0.35
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = fighter_equipment_0
			creator = "FIN"
		}
		requested_factories = 1
		progress = 0.89
		efficiency = 100
	}
}
