﻿### OOB ###
division_template = {
	name = "Division d'Infanterie"
	division_names_group = FRA_INF_01
	# Represents: Division d'Infanterie (Series A and B), Div. d'Inf. de Forteresse
	# Difference is their equipment
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }

		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }

		artillery_brigade = { x = 3 y = 0 }
		artillery_brigade = { x = 3 y = 1 }
		artillery_brigade = { x = 3 y = 2 }
	}
	support = {
		artillery = { x = 0 y = 0 } 	# Heavy Arty Regiment had 2x 155mm battalions
		engineer = { x = 0 y = 1 }
		anti_tank = { x = 0 y = 2 }
		signal_company = { x = 0 y = 3 }
       	field_hospital = { x = 1 y = 0 }
	}
}
division_template = {
	name = "Division d'Infanterie Alpine"	# Division d'Infanterie Alpine
	division_names_group = FRA_MNT_01
	# Note: trimmed to 3x Rgts w/ arty bn added, 1939

	regiments = {
		mountaineers = { x = 0 y = 0 }	# Regular mountain infantry (Infanterie Alpine)
		mountaineers = { x = 0 y = 1 }
		mountaineers = { x = 0 y = 2 }

		mountaineers = { x = 1 y = 0 }
		mountaineers = { x = 1 y = 1 }
		mountaineers = { x = 1 y = 2 }

		mountaineers = { x = 2 y = 0 }
		mountaineers = { x = 2 y = 1 }
		mountaineers = { x = 2 y = 2 }

		artillery_brigade = { x = 3 y = 0 }
		artillery_brigade = { x = 3 y = 1 }
		artillery_brigade = { x = 3 y = 2 }
	}
	support = {
		artillery = { x = 0 y = 0 } 	# Heavy Arty Regiment had 2x 155mm battalions
		engineer = { x = 0 y = 1 }
		anti_tank = { x = 0 y = 2 }
		signal_company = { x = 0 y = 3 }
       	field_hospital = { x = 1 y = 0 }
	}
}
division_template = {
	name = "Division Coloniale"		# Infantry Division
	division_names_group = FRA_COL_01

	regiments = {
		desertinfantry = { x = 0 y = 0 }
		desertinfantry = { x = 0 y = 1 }
		desertinfantry = { x = 0 y = 2 }

		desertinfantry = { x = 1 y = 0 }
		desertinfantry = { x = 1 y = 1 }
		desertinfantry = { x = 1 y = 2 }

		desertinfantry = { x = 2 y = 0 }
		desertinfantry = { x = 2 y = 1 }
		desertinfantry = { x = 2 y = 2 }

		artillery_brigade = { x = 3 y = 0 }
		artillery_brigade = { x = 3 y = 1 }
		artillery_brigade = { x = 3 y = 2 }
	}
	support = {
		artillery = { x = 0 y = 0 } 	# Heavy Arty Regiment had 2x 155mm battalions
		engineer = { x = 0 y = 1 }
		anti_tank = { x = 0 y = 2 }
		signal_company = { x = 0 y = 3 }
       	field_hospital = { x = 1 y = 0 }
	}
}
division_template = {
	name = "Division Française Libre"		# Infantry Division with Légion Etrangère Régiments

	regiments = {
		desertinfantry = { x = 0 y = 0 }
		desertinfantry = { x = 0 y = 1 }
		desertinfantry = { x = 0 y = 2 }

		desertinfantry = { x = 1 y = 0 }
		desertinfantry = { x = 1 y = 1 }
		desertinfantry = { x = 1 y = 2 }

		artillery_brigade = { x = 2 y = 0 }
		artillery_brigade = { x = 2 y = 1 }
	}
	support = {
		engineer = { x = 0 y = 0 }      # includes 1x AC comp, 1x L tank comp
		recon = { x = 0 y = 1 }
		artillery = { x = 0 y = 2 }
	}
}
division_template = {
	name = "Division d'Infanterie Motorisée"	# Division d'Infanterie Motorisée
	division_names_group = FRA_MOT_01

	regiments = {
		motorized = { x = 0 y = 0 }
		motorized = { x = 0 y = 1 }
		motorized = { x = 0 y = 2 }

		motorized = { x = 1 y = 0 }
		motorized = { x = 1 y = 1 }
		motorized = { x = 1 y = 2 }

		mot_artillery_brigade = { x = 2 y = 0 }
		mot_artillery_brigade = { x = 2 y = 1 }
		mot_artillery_brigade = { x = 2 y = 2 }
	}
	support = {
		mot_recon = { x = 0 y = 0 }      # Recon Group consisted of 26 ACs + motorcycles
		artillery = { x = 0 y = 1 }  # Heavy Arty Regiment had 1x 155mm, 1x 105mm battalions
		engineer = { x = 0 y = 2 }
		signal_company = { x = 0 y = 3 }
		anti_tank = { x = 1 y = 0 }
	}
}

division_template = {
	name = "Division Légère de Cavalerie" 	# Division de Cavalerie
	division_names_group = FRA_CAV_01
	# Note: Transformed to DLC, DLM divisions, 1936-39
	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }

		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }

		motorized = { x = 2 y = 0 }
		motorized = { x = 2 y = 1 }
		mot_artillery_brigade = { x = 2 y = 2 }

		motorized = { x = 3 y = 0 }
		motorized = { x = 3 y = 1 }
		mot_artillery_brigade = { x = 3 y = 2 }
	}
	support = {
       	mot_recon = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
		anti_tank = { x = 0 y = 2 }
		signal_company = { x = 0 y = 3 }
       	field_hospital = { x = 1 y = 0 }
	}
}
division_template = {
	name = "Brigade de Cavalerie" 	# Brigade de Cavalerie
	division_names_group = FRA_CAV_01
	# Note: Transformed to DLC, DLM divisions, 1936-39
	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }

		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
	}
}
division_template = {
	name = "Division Légère Mécanique" 		# Division Légère Mécanique
	division_names_group = FRA_MEC_01

	regiments = {
		light_armor = { x = 0 y = 0 }		# Bn. of Hotchkiss, then Souma (Med.) tanks
		light_armor = { x = 0 y = 1 }		# Bn. of Hotchkiss tanks
		medium_armor = { x = 1 y = 0 }			# Brigade of 2x Rgts., 2 Bns. each (later 1 Rgt. of 3x Bns.)
		medium_armor = { x = 1 y = 1 }
		motorized = { x = 2 y = 0 }
		motorized = { x = 2 y = 1 }

		mot_artillery_brigade = { x = 3 y = 0 }
		mot_artillery_brigade = { x = 3 y = 1 }
		mot_artillery_brigade = { x = 3 y = 2 }
	}
	support = {
		anti_air = { x = 0 y = 0 } 	# Heavy Arty Regiment had 2x 155mm battalions
		engineer = { x = 0 y = 1 }
		anti_tank = { x = 0 y = 2 }
		signal_company = { x = 0 y = 3 }
       	field_hospital = { x = 1 y = 0 }
       	mot_recon = { x = 1 y = 1 }
	}
}
division_template = {
	name = "Division Cuirassée" 		# Division Cuirassée
	division_names_group = FRA_ARM_01

	regiments = {
		medium_armor = { x = 0 y = 0 }
		medium_armor = { x = 0 y = 1 }
		medium_armor = { x = 0 y = 2 }

		mechanized = { x = 1 y = 0 }
		mechanized = { x = 1 y = 1 }
		mechanized = { x = 1 y = 2 }

		medium_sp_artillery_brigade = { x = 2 y = 0 }
		medium_sp_artillery_brigade = { x = 2 y = 1 }
		medium_sp_artillery_brigade = { x = 2 y = 2 }
	}
	support = {
		armored_car_recon = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
		anti_air = { x = 0 y = 2 }
		signal_company = { x = 0 y = 3 }
		anti_tank = { x = 1 y = 0 }
		field_hospital = { x = 1 y = 1 }
	}
}
division_template = {
	name = "Groupe de Bataillons de Chars" 	# Bataillons de chars
	division_names_group = FRA_ARM_01
	regiments = {
		light_armor = { x = 0 y = 0 }
		motorized = { x = 1 y = 0 }
	}
}
division_template = {
	name = "Brigade de Chars" 	# Brigade de Chars de Combat, independent armor brigades of 2x Rgts., 2 Bns. each
	division_names_group = FRA_ARM_01
	regiments = {
		light_armor = { x = 0 y = 0 }
		light_armor = { x = 0 y = 1 }
		light_armor = { x = 0 y = 2 }
		motorized = { x = 1 y = 0 }
	}
}
division_template = {
	name = "Brigade Coloniale"			# Represents: 2xRgt colonial forces and , usually with old equipment
	division_names_group = FRA_COL_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
	}
	priority = 0
}

units = {
	division= {
		name = "1. Division Française Libre"
		location = 9445
		division_template = "Division Française Libre"
		force_equipment_variants = { infantry_equipment_1 = { owner = "FRA" } }
		start_experience_factor = 0.3
		start_equipment_factor = 0.8
	}

	division= {
		name = "1. Division Cuirassée"
		location = 7192
		division_template = "Division Cuirassée"
		start_experience_factor = 0.2
		start_equipment_factor = 1
	}

	division= {
		name = "2. Division Cuirassée Légère"
		location = 9445
		division_template = "Division Cuirassée"
		start_experience_factor = 0.1
		start_equipment_factor = 1
	}

	division= {
		name = "5. Division Cuirassée"
		location = 9442
		division_template = "Division Cuirassée"
		start_experience_factor = 0.1
		start_equipment_factor = 1
	}

	division= {
		name = "2. Division d'Infanterie Marocaine"
		location = 1117
		division_template = "Division Coloniale"
		force_equipment_variants = { infantry_equipment_1 = { owner = "FRA" } }
		start_experience_factor = 0.4
		start_equipment_factor = 0.7
	}

	division= {
		name = "3. Division d'Infanterie Algérienne"
		location = 2123
		division_template = "Division Coloniale"
		force_equipment_variants = { infantry_equipment_1 = { owner = "FRA" } }
		start_experience_factor = 0.4
		start_equipment_factor = 0.7
	}

	division= {
		name = "4. Division d'Infanterie Alpine Marocaine"
		location = 7137
		division_template = "Division d'Infanterie Alpine"
		force_equipment_variants = { infantry_equipment_1 = { owner = "FRA" } }
		start_experience_factor = 0.4
		start_equipment_factor = 0.7
	}

	division= {
		name = "7. Division d'Infanterie Algérienne"
		location = 7175
		division_template = "Division Coloniale"
		force_equipment_variants = { infantry_equipment_1 = { owner = "FRA" } }
		start_experience_factor = 0.4
		start_equipment_factor = 0.7
	}

	division= {
		name = "8. Division d'Infanterie Algérienne"
		location = 7192
		division_template = "Division Coloniale"
		force_equipment_variants = { infantry_equipment_1 = { owner = "FRA" } }
		start_experience_factor = 0.4
		start_equipment_factor = 0.7
	}

	division= {
		name = "9. Division d'Infanterie Coloniale"
		location = 4203
		division_template = "Division Coloniale"
		force_equipment_variants = { infantry_equipment_1 = { owner = "FRA" } }
		start_experience_factor = 0.4
		start_equipment_factor = 0.7
	}
}
