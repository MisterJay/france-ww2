units = {
	# Guards armoured brigade
		division= {
			name = "5. Guards Regiment, Guards Armoured Division"
			location = 10150
			division_template = "British Armour Brigade 2"
			start_experience_factor = 0.3
		}
		division= {
			name = "32. Guards Regiment, Guards Armoured Division"
			location = 10150
			division_template = "British Armour Brigade 2"
			start_experience_factor = 0.3
		}
	#
}
