units = {
	# 4 Canadian armour
		division= {
			name = "41. Armour Regiment, 4. Canadian Armour Division"
			location = 10150
			division_template = "Canadian Armour Regiment"
			start_experience_factor = 0.3
		}
		division= {
			name = "22. Armour Regiment, 4. Canadian Armour Division"
			location = 10150
			division_template = "Canadian Armour Regiment"
			start_experience_factor = 0.3
		}
		division= {
			name = "28. Armour Regiment, 4. Canadian Armour Division"
			location = 10150
			division_template = "Canadian Armour Regiment"
			start_experience_factor = 0.3
		}
	#
}
