﻿##### Division Templates #####
division_template = {
	name = "Infantry Division" #Represents French infantry, but is actually polish
	division_names_group = POL_INF_01
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }

		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }

		artillery_brigade = { x = 3 y = 0 }
		artillery_brigade = { x = 3 y = 1 }
		artillery_brigade = { x = 3 y = 2 }
	}
	support = {
		artillery = { x = 0 y = 0 } 	# Heavy Arty Regiment had 2x 155mm battalions
		engineer = { x = 0 y = 1 }
		anti_tank = { x = 0 y = 2 }
		signal_company = { x = 0 y = 3 }
       	field_hospital = { x = 1 y = 0 }
	}
}
division_template = {
	name = "Brygada Kawalerii"
	division_names_group = POL_CAV_01

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 0 y = 2 }
		light_armor = { x = 1 y = 0 }
		mot_artillery_brigade = { x = 2 y = 0 }
	}
	support = {
		recon = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Brygada Piechoty Górskiej"
	division_names_group = POL_MNT_01

	regiments = {
		mountaineers = { x = 0 y = 0 }
		mountaineers = { x = 0 y = 1 }
		mountaineers = { x = 1 y = 0 }
		mountaineers = { x = 1 y = 1 }
	}
	support = {
		engineer = { x = 0 y = 0 }
	}
}

units = {
	division= {
		name = "4. Infantry Division"
		location = 11190
		division_template = "Infantry Division"
		start_equipment_factor = 0.7
		start_experience_factor = 0.3
	}
	division= {
		name = "10. Armoured Cavalry Brigade"
		location = 591
		division_template = "Brygada Kawalerii"
		start_equipment_factor = 0.9
		start_experience_factor = 0.3
	}
	division= {
		name = "Independent Carpathian Rifle Brigade"
		location = 9435
		division_template = "Brygada Piechoty Górskiej"
		start_equipment_factor = 0.9
		start_experience_factor = 0.2
	}
}
