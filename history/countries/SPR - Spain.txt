﻿#############################################################################################
##################################### SPR History File ######################################
#############################################################################################

#########################################################################################
####################################### Politics ########################################
#########################################################################################
	set_politics = {
		ruling_party = democratic
		last_election = "1933.2.16"
		election_frequency = 36
		elections_allowed = yes
	}
	set_popularities = {
		democratic = 41
		fascism = 37
		communism = 7
		neutrality = 15
	}

	1939.1.1 = {
		set_politics = {
			ruling_party = neutrality
			last_election = "1934.3.26"
			election_frequency = 60
			elections_allowed = no
		}
		set_popularities = {
			democratic = 17
			neutrality = 38
			fascism = 45
		}
	}

	every_owned_state = {
		limit = {
			NOT = {
				state = 640
				state = 720
			}
		}
		set_variable = { var = SPA_garrison_control value = 1 }
		SPA_garrison_control_cost_calculation = yes
	}
	create_dynamic_country = {
	    original_tag = SPR
	    set_country_flag = SPR_nationalist_spain_flag
		set_politics = {
			ruling_party = neutrality
		}
		set_cosmetic_tag = SPR_nationalist_spain
		set_research_slots = 3
	    reserve_dynamic_country = yes
	}
	create_dynamic_country = {
	    original_tag = SPR
	    set_country_flag = SPR_carlist_spain_flag
	    set_politics = {
			ruling_party = neutrality
		}
		set_cosmetic_tag = SPR_carlist_spain
		set_research_slots = 3
	    reserve_dynamic_country = yes
	}
	create_dynamic_country = {
	    original_tag = SPR
	    set_country_flag = SPR_anarchist_spain_flag
	    set_politics = {
			ruling_party = neutrality
		}
		set_cosmetic_tag = SPR_anarchist_spain
		set_research_slots = 3
	    reserve_dynamic_country = yes
	}
	SPR = {
		set_variable = { var = spa_civil_war_divisions_counter_var value = 17 }
		set_variable = { var = spr_civil_war_divisions_counter_var value = 66 }
	}
	for_each_scope_loop = {
	    array = core_states

	    set_state_flag = SPR_original_core
	}
	578 = {
		add_dynamic_modifier = { modifier = autonomous_state }
	}
	524 = {
		add_dynamic_modifier = { modifier = autonomous_state }
	}
	549 = {
		add_dynamic_modifier = { modifier = autonomous_state }
	}

	1939.1.1 = {
		add_manpower = -250000
		set_country_flag = SPR_nationalist_spain_flag
		set_cosmetic_tag = SPR_nationalist_spain
		every_unit_leader = {
			limit = {
				NOT = { has_trait = trait_SPA_nationalist_sympathies }
			}
			retire = yes
		}
		every_unit_leader = {
			limit = {
				has_trait = trait_SPA_nationalist_sympathies
			}
			remove_unit_leader_trait = trait_SPA_nationalist_sympathies
		}
		set_global_flag = spanish_civil_war
		set_global_flag = scw_over
		add_timed_idea = { idea = SPA_recovering_from_civil_war days = 1095 }
	}
#########################################################################################

#########################################################################################
####################################### Characters ######################################
#########################################################################################
	recruit_character = SPR_niceto_alcala_zamora
	recruit_character = SPR_jose_diaz
	recruit_character = SPA_jose_antonio_primo_de_rivera
	recruit_character = SPR_manuel_fal_conde
	recruit_character = SPR_vicente_rojo_lluch
	recruit_character = SPR_jose_miaja
	recruit_character = SPR_jose_asensio_torrado
	recruit_character = SPR_valentin_gonzalez
	recruit_character = SPR_enrique_lister
	recruit_character = SPR_juan_modesto
	recruit_character = SPR_antonio_cordon_garcia
	recruit_character = SPR_janos_galicz
	recruit_character = SPA_francisco_franco
	recruit_character = SPR_mohamed_meziane
	recruit_character = SPR_gonzalo_queipo_de_llano
	recruit_character = SPA_emilio_mola
	recruit_character = SPA_juan_yague
	recruit_character = SPA_agustin_munoz_grandes
	recruit_character = SPA_jose_enrique_varela
	recruit_character = SPA_miguel_cabanellas
	recruit_character = SPR_jose_millan_astray
	recruit_character = SPR_miguel_buiza_fernandez_palacios
	recruit_character = SPA_luis_carrero_blanco
	recruit_character = SPA_rafael_garcia_valino
	recruit_character = SPA_heli_rolando_de_tella
	recruit_character = SPA_miguel_ponte
	recruit_character = SPA_joaquin_garcia_morato
	recruit_character = SPA_juan_vigon_suero_diaz
	recruit_character = SPA_wilhelm_ritter_von_thoma
	recruit_character = SPA_enrique_canovas_lacruz
	recruit_character = SPA_ricardo_rada
	recruit_character = SPR_nicolas_molero
	recruit_character = SPR_felipe_diaz_sandino
	recruit_character = SPR_andres_nin
	recruit_character = SPR_council_of_theorists
	recruit_character = SPR_domenec_batet
	recruit_character = SPR_etelvino_vega
	recruit_character = SPR_mate_zalka
	recruit_character = SPR_defensive_military_council
	recruit_character = SPR_offensive_military_council
	recruit_character = SPR_organizational_military_council
	recruit_character = SPR_ignacio_hidalgo_de_cisneros
	recruit_character = SPR_andres_garcia_la_calle
	recruit_character = SPR_alberto_bayo
	recruit_character = SPR_air_safety_aviation_council
	recruit_character = SPR_all_weather_aviation_council
	recruit_character = SPR_luis_gonzalez_de_ubieta
	recruit_character = SPR_joakin_egia_unzueta
	recruit_character = SPR_commerce_raiding_naval_council
	recruit_character = SPR_reform_naval_council
	recruit_character = SPR_toribio_martinez_cabrera
	recruit_character = SPR_francisco_ciutat_de_miguel
	recruit_character = SPR_antonio_ortega_gutierrez
	recruit_character = SPR_antonio_azarola_y_gresillon
	recruit_character = SPR_jose_rovira
	recruit_character = SPR_carmel_rosa_baserba
	recruit_character = SPR_wilhelm_zaisser
	recruit_character = SPR_antonio_escobar_huerta
	recruit_character = SPR_infantry_military_council
	recruit_character = SPR_army_regrouping_military_council
	recruit_character = SPR_concealment_military_council
	recruit_character = SPR_naval_strike_aviation_council
	recruit_character = SPR_close_air_support_aviation_council
	recruit_character = SPR_logistics_naval_council
	recruit_character = SPR_anti_submarine_naval_council
	recruit_character = SPR_juan_negrin
	recruit_character = SPR_juan_lopez_sanchez
	recruit_character = SPR_juan_garcia_oliver
	recruit_character = SPR_federica_montseny
	recruit_character = SPR_juan_andrade
	recruit_character = SPR_antonia_adroher_i_pascual
	recruit_character = SPR_jesus_hernandez_tomas
	recruit_character = SPR_alexander_orlov
	recruit_character = SPR_indalecio_prieto
	recruit_character = SPR_dolores_ibarruri
	recruit_character = SPR_diego_martinez_barrio
	recruit_character = SPR_augusto_barcia_trelles
	recruit_character = SPR_francisco_largo_caballero
	recruit_character = SPA_luis_valdes_cavanillas
	recruit_character = SPA_tomas_garciano_goni
	recruit_character = SPA_fidel_davila_arrondo
	recruit_character = SPA_jose_antonio_giron
	recruit_character = SPA_manuel_hedilla
	recruit_character = SPA_raimundo_fernandez_cuesta
	recruit_character = SPA_tomas_dominguez_arevalo
	recruit_character = SPA_diego_hidalgo_y_duran
	recruit_character = SPA_ramon_serrano_suner
	recruit_character = SPA_luis_hernando_de_larramendi
	recruit_character = SPA_martin_de_riquer
	recruit_character = SPA_mauricio_de_sivatte

	SPR_enrique_lister = {
		set_character_flag = SPR_do_not_align_me_flag
	}
	SPR_valentin_gonzalez = {
		set_character_flag = SPR_do_not_align_me_flag
	}
#########################################################################################

#########################################################################################
################################ Equipment and Research #################################
#########################################################################################
	set_convoys = 150
	set_research_slots = 3
	capital = 600
	set_stability = 0.5
	set_war_support = 0.5

	1936.1.1 = {
		set_technology = {
			infantry_weapons = 1
			infantry_weapons1 = 1
			armored_car1 = 1
			motorised_infantry = 1
			tech_mountaineers = 1
			tech_support = 1
			tech_engineers = 1
			tech_recon = 1
			basic_train = 1
			gwtank_chassis = 1
			basic_light_tank_chassis = 1
			gw_artillery = 1
			interwar_antitank = 1
			early_ship_hull_light = 1
			basic_torpedo = 1
			early_ship_hull_submarine = 1
			basic_battery = 1
			early_fighter = 1
			fighter1 = 1
			early_bomber = 1
			electronic_mechanical_engineering = 1
			fuel_silos = 1
			synth_oil_experiments = 1
			trench_warfare = 1
			force_rotation = 1
			fleet_in_being = 1
			battlefleet_concentration = 1
			#Manually added
			naval_bomber1 = 1
		}
		if = {
			limit = {
				has_dlc = "Battle for the Bosporus"
			}
			set_technology = { camelry = 1 }
		}

		set_technology = {
			early_ship_hull_light = 1
			basic_ship_hull_light = 1
			early_ship_hull_submarine = 1
			basic_ship_hull_submarine = 1
			early_ship_hull_cruiser = 1
			basic_ship_hull_cruiser = 1
			early_ship_hull_heavy = 1
			basic_battery = 1
			basic_medium_battery = 1
			basic_secondary_battery = 1
			basic_torpedo = 1
			basic_depth_charges = 1
			mtg_transport = 1
		}
	}
	1940.5.10 = {
		set_technology = {
			support_weapons = 1
			infantry_weapons2 = 1
			paratroopers = 1
			marines = 1
			tech_mountaineers2 = 1
			tech_special_forces = 1
			tech_engineers2 = 1
			tech_flamethrower = 1
			tech_recon2 = 1
			desertinfantry_at = 1
			tech_military_police = 1
			tech_maintenance_company = 1
			tech_field_hospital = 1
			tech_logistics_company = 1
			tech_signal_company = 1
			railway_gun = 1
			improved_light_tank_chassis = 1
			advanced_light_tank_chassis = 1
			basic_medium_tank_chassis = 1
			basic_heavy_tank_chassis = 1
			armor_tech_1 = 1
			engine_tech_1 = 1
			interwar_artillery = 1
			artillery1 = 1
			interwar_antiair = 1
			antiair1 = 1
			antitank1 = 1
			antitank2 = 1
			basic_ship_hull_light = 1
			smoke_generator = 1
			basic_depth_charges = 1
			sonar = 1
			early_ship_hull_cruiser = 1
			improved_airplane_launcher = 1
			early_ship_hull_heavy = 1
			basic_secondary_battery = 1
			mtg_transport = 1
			fighter2 = 1
			CAS1 = 1
			tactical_bomber1 = 1
			scout_plane1 = 1
			radio = 1
			radio_detection = 1
			basic_machine_tools = 1
			improved_machine_tools = 1
			basic_machine_tools = 1
			improved_machine_tools = 1
			dispersed_industry = 1
			dispersed_industry2 = 1
			construction1 = 1
			construction2 = 1
			excavation1 = 1
			oil_processing = 1
			grand_battle_plan = 1
			fighter_baiting = 1
			subsidiary_carrier_role = 1
		}
	}
	1941.6.21 = {
		set_technology = {
			support_weapons2 = 1
			improved_infantry_weapons = 1
			mechanised_infantry = 1
			improved_special_forces = 1
			wartime_train = 1
			armored_train = 1
			armor_tech_2 = 1
			engine_tech_2 = 1
			desertinfantry_at2 = 1
			artillery2 = 1
			antiair2 = 1
			antitank3 = 1
			improved_sonar = 1
			basic_ship_hull_cruiser = 1
			basic_cruiser_armor_scheme = 1
			basic_ship_hull_heavy = 1
			early_ship_hull_carrier = 1
			magnetic_detonator = 1
			basic_light_battery = 1
			basic_medium_battery = 1
			basic_heavy_battery = 1
			fighter3 = 1
			CAS2 = 1
			naval_bomber2 = 1
			strategic_bomber1 = 1
			mechanical_computing = 1
			decimetric_radar = 1
			basic_fire_control_system = 1
			advanced_machine_tools = 1
			advanced_machine_tools = 1
			dispersed_industry3 = 1
			construction3 = 1
			fuel_refining = 1
			improved_oil_processing = 1
			prepared_defense = 1
			low_echelon_support = 1
			hunter_killer_groups = 1
		}
	}
	1942.11.19 = {
		set_technology = {
			support_weapons3 = 1
			improved_infantry_weapons_2 = 1
			infantry_at = 1
			paratroopers2 = 1
			marines2 = 1
			tech_mountaineers3 = 1
			survival_training = 1
			tech_engineers3_1 = 1
			tech_flamethrower2 = 1
			tech_recon3 = 1
			tech_military_police2 = 1
			tech_maintenance_company2 = 1
			tech_field_hospital2 = 1
			tech_logistics_company2 = 1
			tech_signal_company2 = 1
			improved_medium_tank_chassis = 1
			improved_heavy_tank_chassis = 1
			armor_tech_3 = 1
			engine_tech_3 = 1
			rocket_artillery = 1
			antitank4 = 1
			improved_depth_charges = 1
			basic_heavy_armor_scheme = 1
			basic_ship_hull_submarine = 1
			basic_light_shell = 1
			basic_medium_shell = 1
			basic_heavy_shell = 1
			improved_secondary_battery = 1
			damage_control_1 = 1
			fire_control_methods_1 = 1
			tactical_bomber2 = 1
			fighter4 = 1
			heavy_fighter1 = 1
			computing_machine = 1
			dispersed_industry4 = 1
			construction4 = 1
			excavation2 = 1
			fuel_refining2 = 1
			advanced_oil_processing = 1
			grand_assault = 1
			dispersed_fighting = 1
			floating_fortress = 1
		}
	}
	1943.7.11 = {
		set_technology = {
			support_weapons4 = 1
			advanced_infantry_weapons = 1
			armored_car2 = 1
			jaegers = 1
			shocktroops = 1
			semi_modern_light_tank_chassis = 1
			artillery3 = 1
			antiair3 = 1
			antitank5 = 1
			improved_ship_hull_light = 1
			improved_cruiser_armor_scheme = 1
			improved_heavy_armor_scheme = 1
			improved_ship_torpedo_launcher = 1
			improved_ship_hull_submarine = 1
			basic_submarine_snorkel = 1
			improved_light_battery = 1
			improved_medium_battery = 1
			improved_heavy_battery = 1
			dp_secondary_battery = 1
			mtg_landing_craft = 1
			CAS3 = 1
			naval_bomber3 = 1
			tactical_bomber3 = 1
			strategic_bomber2 = 1
			scout_plane2 = 1
			improved_computing_machine = 1
			improved_decimetric_radar = 1
			improved_fire_control_system = 1
			assembly_line_production = 1
			assembly_line_production = 1
			rubber_processing = 1
			infantry_offensive = 1
			operational_destruction = 1
			convoy_sailing = 1
		}
	}
	1944.6.6 = {
		set_technology = {
			night_vision = 1
			tech_military_police3 = 1
			tech_maintenance_company3 = 1
			tech_field_hospital3 = 1
			tech_logistics_company3 = 1
			tech_signal_company3 = 1
			armor_tech_4 = 1
			infantry_at2 = 1
			engine_tech_4 = 1
			artillery4 = 1
			rocket_artillery2 = 1
			antiair4 = 1
			antitank_a = 1
			desertinfantry_at3 = 1
			improved_ship_hull_cruiser = 1
			improved_ship_hull_heavy = 1
			basic_ship_hull_carrier = 1
			electric_torpedo = 1
			advanced_ship_torpedo_launcher = 1
			improved_light_shell = 1
			improved_medium_shell = 1
			improved_heavy_shell = 1
			damage_control_2 = 1
			fire_control_methods_2 = 1
			fighter5 = 1
			CAS4 = 1
			naval_bomber4 = 1
			heavy_fighter2 = 1
			strategic_bomber3 = 1
			advanced_computing_machine = 1
			dispersed_industry5 = 1
			construction5 = 1
			excavation3 = 1
			fuel_refining3 = 1
			modern_oil_processing = 1
			armored_operations = 1
			fighter_veteran_initiative = 1
			convoy_escorts = 1
		}
	}
	1944.12.6 = {
		set_technology = {
			advanced_infantry_weapons2 = 1
			armored_car3 = 1
			paratroopers3 = 1
			marines3 = 1
			tech_mountaineers4 = 1
			jaegers2 = 1
			shocktroops2 = 1
			elite_forces = 1
			tech_engineers4 = 1
			tech_flamethrower3 = 1
			tech_recon4 = 1
			advanced_medium_tank_chassis = 1
			advanced_heavy_tank_chassis = 1
			artillery5 = 1
			homing_torpedo = 1
			heavy_fighter3 = 1
			centimetric_radar = 1
			advanced_fire_control_system = 1
			flexible_line = 1
			streamlined_line = 1
			fuel_refining4 = 1
			improved_rubber_processing = 1
			infiltration_assault = 1
			naval_strike_torpedo_tactics_oi = 1
			escort_carriers = 1
		}
	}
	1945.1.1 = {
		set_technology = {
			artillery_a = 1
			rocket_artillery3 = 1
			antiair5 = 1
			infantry_at_a = 1
			antitank_b = 1
			antitank_c = 1
			damage_control_3 = 1
			fire_control_methods_3 = 1
			tactical_bomber4 = 1
			strategic_bomber4 = 1
			improved_centimetric_radar = 1
			excavation4 = 1
			oil_processing_6 = 1
			night_assault_tactics = 1
			cas_veteran_initiative = 1
			integrated_convoy_defence = 1
		}
	}
#########################################################################################

#########################################################################################
######################################## Ideas ##########################################
#########################################################################################
	1936.1.1 = {
		add_ideas = {
			SPA_carlism_1
			SPR_military_disloyalty
			SPR_political_violence
			SPR_national_strikes_3
			age_18
		}
	}
	1940.5.10 = {
		add_ideas = {
			partial_economic_mobilisation
			state_press
			limited_conscription
			age_17
		}

		remove_ideas = {
			SPR_military_disloyalty
			SPR_political_violence
			SPR_national_strikes_3
		}
	}
	1941.6.21 = {
		add_ideas = {
			war_economy
		}
	}
	1942.11.19 = {
		add_ideas = {
			extensive_conscription
			age_15
		}
	}
	1943.7.11 = {
		add_ideas = {

		}
	}
	1944.6.6 = {
		add_ideas = {

		}
	}

	1944.12.16 = {
		add_ideas = {

		}
	}
	1945.1.1 = {
		add_ideas = {

		}
	}
#########################################################################################

#########################################################################################
######################################## Focuses ########################################
#########################################################################################
	1940.5.10 = {
		unlock_national_focus = SPA_a_great_spain									#35
		unlock_national_focus = SPA_hand_over_the_ceda_campaign_chest				#45
		unlock_national_focus = SPA_negotiate_carlist_support						#45
		unlock_national_focus = SPA_the_army_of_africa								#45
		unlock_national_focus = SPA_secure_the_northern_garrisons					#35
		complete_national_focus = SPA_con_paquito									#35
		unlock_national_focus = SPA_unify_the_nationalist_front						#35
		unlock_national_focus = SPA_save_the_alcazar								#35
		unlock_national_focus = SPA_martyrdom_for_primo_de_rivera					#70
		complete_national_focus = SPA_caudillo_of_spain								#70
		complete_national_focus = SPA_consolidate_the_north							#70
		complete_national_focus = SPA_foment_a_carlist_split						#70
		unlock_national_focus = SPA_a_methodical_approach							#35
		unlock_national_focus = SPA_tackle_the_vulnerable_fronts					#35
		complete_national_focus = SPA_extol_the_martyrs_of_the_war					#45
		complete_national_focus = SPA_banish_the_party_leaders						#70
		complete_national_focus = SPA_safeguard_the_freedom_of_worship				#60
		complete_national_focus = SPA_fuse_the_parties								#70
		complete_national_focus = SPA_the_condor_legion								#45
		complete_national_focus = SPA_the_corpo_truppe_volontarie					#45
		unlock_national_focus = SPA_equipment_shipments								#35
		complete_national_focus = SPA_doctrinal_advancements						#45
		complete_national_focus = SPA_obtain_training_staff							#35
		complete_national_focus = SPA_integrate_the_requetes						#70
		complete_national_focus = SPA_utilize_the_intellectuals						#70
		complete_national_focus = SPA_expand_conscription							#45
		complete_national_focus = SPA_national_recovery 							#100
		complete_national_focus = SPA_adopt_the_26_points 							#70
		complete_national_focus = SPA_autarky 										#70
	}

	1941.6.22 = {
		complete_national_focus = SPA_direct_the_universities 						#70
		complete_national_focus = SPA_the_national_and_popular_army_of_spain 		#45
		complete_national_focus = SPA_dictator_for_life 							#60
		complete_national_focus = SPA_stamp_out_the_maquis 							#60
		complete_national_focus = SPA_la_division_azul 								#45
		complete_national_focus = SPA_the_army_of_africa_model 						#70
		complete_national_focus = SPA_prepare_the_pyrenees_defenses 				#35
	}

	1942.11.19 = {
		complete_national_focus = SPA_spanish_austerity								#70
		unlock_national_focus = SPA_equipment_modernization 						#35
		unlock_national_focus = SPA_motorization 									#35
		complete_national_focus = SPA_reduce_reliance_on_foreign_resources 			#70
		unlock_national_focus = SPA_artillery_modernization 						#45
		complete_national_focus = SPA_defenses_against_strategic_bombing 			#70
		complete_national_focus = SPA_defenses_against_invasion 					#70
		complete_national_focus = SPA_improve_cross_country_railways 				#70
		complete_national_focus = SPA_expand_the_axis_gold_trade 					#70
	}

	1943.7.11 = {
		complete_national_focus = SPA_ensure_agricultural_self_sufficiency 			#100
		complete_national_focus = SPA_expand_the_war_industry 						#70
		complete_national_focus = SPA_expand_the_special_forces 					#70
		complete_national_focus = SPA_expand_the_air_branch		 					#70
	}
#########################################################################################

#########################################################################################
####################################### Settings ########################################
#########################################################################################
	### Settings
		set_country_flag = wants_major_news_events
		set_country_flag = wants_medium_news_events
		set_country_flag = wants_minor_news_events

		set_country_flag = wants_fallen_city_newsevents

		set_global_flag = player_wants_axis_resistance
		set_global_flag = player_wants_allies_resistance
	###

	### Bridges ###
		add_dynamic_modifier = { modifier = blowing_bridge_dynamic_modifier }
		add_dynamic_modifier = { modifier = repairing_bridge_dynamic_modifier }
	###
#########################################################################################

#########################################################################################
################################### Equipment Variants ##################################
#########################################################################################
	if = {
		limit = { not = { has_dlc = "Man the Guns" } }
		### Ship Variants ###
		create_equipment_variant = {
			name = "C Class"
			type = submarine_1
			upgrades = {
				ship_reliability_upgrade = 1
				sub_engine_upgrade = 1
				sub_stealth_upgrade = 1
				sub_torpedo_upgrade = 1
			}
		}
		create_equipment_variant = {
			name = "Príncipe Alfonso Class"
			type = light_cruiser_1
			upgrades = {
				ship_reliability_upgrade = 1
				ship_engine_upgrade = 1
				ship_gun_upgrade = 1
				ship_anti_air_upgrade = 1
			}
		}
	}
	if = {
		limit = { has_dlc = "Man the Guns" }
		# Submarines #
		create_equipment_variant = {
			name = "B Class"
			type = ship_hull_submarine_1
			parent_version = 0
			name_group = SPR_SS_HISTORICAL
			modules = {
				fixed_ship_torpedo_slot = ship_torpedo_sub_1
				fixed_ship_engine_slot = sub_ship_engine_1
				rear_1_custom_slot = empty
			}
			obsolete = yes
		}
		create_equipment_variant = {
			name = "C Class"
			type = ship_hull_submarine_2
			name_group = SPR_SS_HISTORICAL
			parent_version = 0
			modules = {
				fixed_ship_torpedo_slot = ship_torpedo_sub_1
				fixed_ship_engine_slot = sub_ship_engine_2
				rear_1_custom_slot = empty
			}
		}
		# Destroyers #
		create_equipment_variant = {
			name = "Alsedo Class"
			type = ship_hull_light_1
			name_group = SPR_DD_HISTORICAL
			parent_version = 0
			modules = {
				fixed_ship_battery_slot = ship_light_battery_1
				fixed_ship_anti_air_slot = ship_anti_air_1
				fixed_ship_fire_control_system_slot = ship_fire_control_system_0
				fixed_ship_radar_slot = empty
				fixed_ship_engine_slot = light_ship_engine_1
				fixed_ship_torpedo_slot = ship_torpedo_1
				mid_1_custom_slot = ship_mine_layer_1
				rear_1_custom_slot = ship_depth_charge_1
			}
			obsolete = yes
		}
		create_equipment_variant = {
			name = "Churucca Class"
			type = ship_hull_light_2
			name_group = SPR_DD_HISTORICAL
			parent_version = 0
			modules = {
				fixed_ship_battery_slot = ship_light_battery_1
				fixed_ship_anti_air_slot = ship_anti_air_1
				fixed_ship_fire_control_system_slot = ship_fire_control_system_0
				fixed_ship_radar_slot = empty
				fixed_ship_engine_slot = light_ship_engine_2
				fixed_ship_torpedo_slot = ship_torpedo_1
				mid_1_custom_slot = empty
				rear_1_custom_slot = ship_depth_charge_1
			}
		}
		create_equipment_variant = {
			name = "Júpiter Class"					# minelayers
			type = ship_hull_light_2
			name_group = SPR_DD_HISTORICAL
			parent_version = 0
			modules = {
				fixed_ship_battery_slot = ship_light_battery_1
				fixed_ship_anti_air_slot = ship_anti_air_1
				fixed_ship_fire_control_system_slot = ship_fire_control_system_0
				fixed_ship_radar_slot = empty
				fixed_ship_engine_slot = light_ship_engine_1
				fixed_ship_torpedo_slot = empty
				mid_1_custom_slot = ship_mine_layer_1
				rear_1_custom_slot = ship_depth_charge_1
			}
		}
		# Light Cruisers #
		create_equipment_variant = {
			name = "República Class"
			type = ship_hull_cruiser_1
			name_group = SPR_CL_HISTORICAL
			parent_version = 0
			modules = {
				fixed_ship_battery_slot = ship_light_medium_battery_1
				fixed_ship_anti_air_slot = ship_anti_air_1
				fixed_ship_fire_control_system_slot = ship_fire_control_system_0
				fixed_ship_radar_slot = empty
				fixed_ship_engine_slot = cruiser_ship_engine_1
				mid_1_custom_slot = ship_torpedo_1
				mid_2_custom_slot = ship_light_medium_battery_1
				rear_1_custom_slot = empty
			}
			obsolete = yes
		}
		create_equipment_variant = {
			name = "Méndez Núñes Class"
			type = ship_hull_cruiser_1
			name_group = SPR_CL_HISTORICAL
			parent_version = 0
			modules = {
				fixed_ship_battery_slot = ship_light_medium_battery_1
				fixed_ship_anti_air_slot = ship_anti_air_1
				fixed_ship_fire_control_system_slot = ship_fire_control_system_0
				fixed_ship_radar_slot = empty
				fixed_ship_engine_slot = cruiser_ship_engine_1
				fixed_ship_armor_slot = ship_armor_cruiser_1
				mid_1_custom_slot = ship_torpedo_1
				mid_2_custom_slot = ship_torpedo_1
				rear_1_custom_slot = empty
			}
			obsolete = yes
		}
		create_equipment_variant = {
			name = "Príncipe Alfonso Class"
			type = ship_hull_cruiser_2
			name_group = SPR_CL_HISTORICAL
			parent_version = 0
			modules = {
				fixed_ship_battery_slot = ship_light_medium_battery_1
				fixed_ship_anti_air_slot = ship_anti_air_1
				fixed_ship_fire_control_system_slot = ship_fire_control_system_0
				fixed_ship_radar_slot = empty
				fixed_ship_engine_slot = cruiser_ship_engine_2
				fixed_ship_armor_slot = ship_armor_cruiser_1
				front_1_custom_slot = empty
				mid_1_custom_slot = ship_torpedo_1
				mid_2_custom_slot = ship_torpedo_1
				rear_1_custom_slot = ship_light_medium_battery_1
			}
		}
		# Heavy Cruisers #
		create_equipment_variant = {
			name = "Canarias Class"
			type = ship_hull_cruiser_2
			name_group = SPR_CA_HISTORICAL
			parent_version = 0
			modules = {
				fixed_ship_battery_slot = ship_medium_battery_2
				fixed_ship_anti_air_slot = ship_anti_air_1
				fixed_ship_fire_control_system_slot = ship_fire_control_system_0
				fixed_ship_radar_slot = empty
				fixed_ship_engine_slot = cruiser_ship_engine_2
				fixed_ship_armor_slot = empty
				mid_1_custom_slot = ship_medium_battery_2
				mid_2_custom_slot = empty
				rear_1_custom_slot = ship_depth_charge_1
			}
		}
		# Battleships #
		create_equipment_variant = {
			name = "España Class"
			type = ship_hull_heavy_1
			name_group = SPR_BB_HISTORICAL
			parent_version = 0
			modules = {
				fixed_ship_battery_slot = ship_heavy_battery_1
				fixed_ship_anti_air_slot = ship_anti_air_1
				fixed_ship_fire_control_system_slot = ship_fire_control_system_0
				fixed_ship_radar_slot = empty
				fixed_ship_engine_slot = heavy_ship_engine_1
				fixed_ship_secondaries_slot = ship_secondaries_1
				fixed_ship_armor_slot = ship_armor_bb_1
				front_1_custom_slot = ship_heavy_battery_1
				mid_1_custom_slot = ship_secondaries_1
				mid_2_custom_slot = ship_secondaries_1
				rear_1_custom_slot = empty
			}
		}
	}

	# 1939 Start #
	1939.1.1 = {
		# Submarines #
		create_equipment_variant = {
			name = "General Mola Class"							# purchased from Italy
			type = ship_hull_submarine_2
			name_group = SPR_SS_HISTORICAL
			parent_version = 0
			modules = {
				fixed_ship_torpedo_slot = ship_torpedo_sub_1
				fixed_ship_engine_slot = sub_ship_engine_2
				rear_1_custom_slot = ship_torpedo_sub_1
			}
			obsolete = yes
		}
		# Destroyers #
		create_equipment_variant = {
			name = "Melilla Class"								# purchased from Italy
			type = ship_hull_light_1
			name_group = SPR_DD_HISTORICAL
			parent_version = 0
			modules = {
				fixed_ship_battery_slot = ship_light_battery_1
				fixed_ship_anti_air_slot = ship_anti_air_1
				fixed_ship_fire_control_system_slot = ship_fire_control_system_0
				fixed_ship_radar_slot = empty
				fixed_ship_engine_slot = light_ship_engine_1
				fixed_ship_torpedo_slot = ship_torpedo_1
				mid_1_custom_slot = ship_mine_layer_1
				rear_1_custom_slot = empty
			}
			obsolete = yes
		}
		create_equipment_variant = {
			name = "Eolo Class"									# minelayer
			type = ship_hull_light_2
			name_group = SPR_DD_HISTORICAL
			parent_version = 0
			modules = {
				fixed_ship_battery_slot = ship_light_battery_2
				fixed_ship_anti_air_slot = ship_anti_air_1
				fixed_ship_fire_control_system_slot = ship_fire_control_system_0
				fixed_ship_radar_slot = empty
				fixed_ship_engine_slot = light_ship_engine_1
				fixed_ship_torpedo_slot = empty
				mid_1_custom_slot = ship_mine_layer_1
				rear_1_custom_slot = ship_depth_charge_1
			}
		}
	}
#########################################################################################

#########################################################################################
######################################## OOBs ###########################################
#########################################################################################
	oob = "_SPR_1936"
		set_naval_oob = "naval_mtg_SPR_1936"

	1940.1.1 = {
		oob = "_SPR_1940"
		set_naval_oob = "naval_mtg_SPR_1940"
	}

	1941.6.21 = {
		oob = "_SPR_1941"
		set_naval_oob = "naval_mtg_SPR_1941"
	}

	1942.11.19 = {
		oob = "_SPR_1942_11"
		set_naval_oob = "naval_mtg_SPR_1942_11"
	}

	1943.1.1 = {
		oob = "_SPR_1943"
		set_naval_oob = "naval_mtg_SPR_1943"
	}

	1944.6.1 = {
		oob = "_SPR_1944_6"
		set_naval_oob = "naval_mtg_SPR_1944_6"
	}

	1944.12.1 = {
		oob = "_SPR_1944_12"
		set_naval_oob = "naval_mtg_SPR_1944_12"
	}

	1945.1.1 = {
		oob = "_SPR_1945"
		set_naval_oob = "naval_mtg_SPR_1945"
	}
#########################################################################################
