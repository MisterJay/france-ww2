﻿#############################################################################################
##################################### ROM History File ######################################
#############################################################################################

#########################################################################################
####################################### Politics ########################################
#########################################################################################
	set_politics = {
		ruling_party = democratic
		last_election = "1933.12.20"
		election_frequency = 48
		elections_allowed = yes
	}
	set_popularities = {
		democratic = 60
		fascism = 18
		communism = 2
		neutrality = 20
	}

	1938.2.10 = {
		set_politics = {
			ruling_party = neutrality
			last_election = "1939.7.15"
			election_frequency = 48
			elections_allowed = no
		}
		set_popularities = {
			democratic = 20
			fascism = 35
			communism = 2
			neutrality = 43
		}
	}

	1944.8.23 = {
		set_politics = {
			ruling_party = democratic
			last_election = "1939.7.15"
			election_frequency = 48
			elections_allowed = yes
		}
		set_popularities = {
			democratic = 60
			fascism = 18
			communism = 2
			neutrality = 20
		}
	}
	1945.3.6 = {
		set_politics = {
			ruling_party = communism
			last_election = "1939.7.15"
			election_frequency = 48
			elections_allowed = yes
		}
		set_popularities = {
			democratic = 18
			fascism = 2
			communism = 60
			neutrality = 20
		}
	}

	recruit_character = ROM_gheorghe_tatarescu

	recruit_character = ROM_corneliu_codreanu
	recruit_character = ROM_octavian_goga
	recruit_character = ROM_armand_calinescu
	recruit_character = ROM_petru_groza
	recruit_character = ROM_king_michael
	recruit_character = ROM_ion_antonescu

	1938.2.10 = {
		# retire_country_leader = yes
		retire_character = ROM_corneliu_codreanu
		create_country_leader = {
			name = "Carol II"
			desc = "POLITICS_CAROL_II_DESC"
			picture = GFX_Portrait_romania_Carol_II
			expire = "1965.1.1"
			ideology = despotism
			traits = {
				hedonist
				camarilla_leader
			}
			id = 550
		}
	}
	1940.9.4 = {
		retire_country_leader = yes

		add_country_leader_role = {
			character = "ROM_ion_antonescu"
			country_leader = {
				ideology=despotism
				traits = {
					#
				}
				expire = "1965.1.1"
			}
			promote_leader = yes
		}
	}

	1944.8.23 = {
		retire_character = ROM_gheorghe_tatarescu
		ROM_king_michael = {
			promote_character = { ideology = liberalism }
		}
	}
	1945.3.6 = {
		# promote_character = ROM_petru_groza
	}

	#1936.1.1 Tatarescu democratic
	#1938.2.10 Carol II neutrality
	#1940.9.4 Ion Antonescu Neutrality
	#1944.8.23 King Michael Democratic
	#1945.3.6 Petru Groza Communism
	if = {
		limit = { has_dlc = "Death or Dishonor" }
		ROM = {
			#Pick from list of the sane events first
			random_list = {
				100 = { country_event = { id = DOD_romania.81 days = 2 random = 20 } }
			}
		}
	}
#########################################################################################

#########################################################################################
####################################### Characters ######################################
#########################################################################################
	recruit_character = ROM_petre_dumitrescu
	recruit_character = ROM_ioan_mihail_racovita
	recruit_character = ROM_gheorghe_avramescu
	recruit_character = ROM_constantin_sanatescu
	recruit_character = ROM_horia_macellariu
	recruit_character = ROM_gheorghe_potopeanu
	recruit_character = ROM_emanoil_ionescu
	recruit_character = ROM_gheorghe_mihail
	recruit_character = ROM_ermil_gheorghiu
	recruit_character = ROM_nicolae_sova
	recruit_character = ROM_gheorghe_jienescu
	recruit_character = ROM_paul_teodorescu
	recruit_character = ROM_gheorghe_vasiliu
	recruit_character = ROM_iuliu_maniu
	recruit_character = ROM_gheorghe_gheorghiu_dej
	recruit_character = ROM_nicolae_malaxa
	recruit_character = ROM_mihail_sturdza
	recruit_character = ROM_gheorghe_argeseanu
	recruit_character = ROM_king_michael
#########################################################################################

#########################################################################################
################################ Equipment and Research #################################
#########################################################################################
	set_convoys = 20
	set_research_slots = 3
	capital = 473
	set_stability = 0.6
	set_war_support = 0.61

	1936.1.1 = {
		set_technology = {
			infantry_weapons = 1
			infantry_weapons1 = 1
			armored_car1 = 1
			motorised_infantry = 1
			marines = 1
			tech_mountaineers = 1
			tech_support = 1
			tech_engineers = 1
			tech_logistics_company = 1
			basic_train = 1
			gwtank_chassis = 1
			gw_artillery = 1
			interwar_artillery = 1
			early_ship_hull_light = 1
			basic_torpedo = 1
			early_ship_hull_submarine = 1
			basic_battery = 1
			early_fighter = 1
			fighter1 = 1
			early_bomber = 1
			electronic_mechanical_engineering = 1
			fuel_silos = 1
			fuel_refining = 1
			trench_warfare = 1
			force_rotation = 1
			fleet_in_being = 1
			battlefleet_concentration = 1
		}

		set_technology = {
			early_ship_hull_light = 1
			early_ship_hull_submarine = 1
			basic_ship_hull_submarine = 1
			basic_battery = 1
			basic_torpedo = 1
			basic_depth_charges = 1
			mtg_transport = 1
		}
	}
	1940.5.10 = {
		set_technology = {
			support_weapons = 1
			infantry_weapons2 = 1
			improved_infantry_weapons = 1
			paratroopers = 1
			marines2 = 1
			tech_mountaineers2 = 1
			desertinfantry_at = 1
			tech_special_forces = 1
			tech_engineers2 = 1
			tech_flamethrower = 1
			tech_recon = 1
			tech_military_police = 1
			tech_maintenance_company = 1
			tech_field_hospital = 1
			tech_logistics_company2 = 1
			tech_signal_company = 1
			railway_gun = 1
			basic_light_tank_chassis = 1
			basic_medium_tank_chassis = 1
			basic_heavy_tank_chassis = 1
			armor_tech_1 = 1
			engine_tech_1 = 1
			artillery1 = 1
			artillery2 = 1
			interwar_antiair = 1
			interwar_antitank = 1
			antiair1 = 1
			antitank1 = 1
			basic_ship_hull_light = 1
			smoke_generator = 1
			basic_depth_charges = 1
			sonar = 1
			early_ship_hull_cruiser = 1
			improved_airplane_launcher = 1
			early_ship_hull_heavy = 1
			basic_secondary_battery = 1
			mtg_transport = 1
			fighter2 = 1
			CAS1 = 1
			naval_bomber1 = 1
			tactical_bomber1 = 1
			scout_plane1 = 1
			radio = 1
			radio_detection = 1
			basic_machine_tools = 1
			improved_machine_tools = 1
			basic_machine_tools = 1
			improved_machine_tools = 1
			dispersed_industry = 1
			dispersed_industry2 = 1
			construction1 = 1
			construction2 = 1
			excavation1 = 1
			fuel_refining2 = 1
			grand_battle_plan = 1
			fighter_baiting = 1
			subsidiary_carrier_role = 1
		}
	}
	1941.6.21 = {
		set_technology = {
			support_weapons2 = 1
			improved_infantry_weapons_2 = 1
			mechanised_infantry = 1
			desertinfantry_at2 = 1
			improved_special_forces = 1
			tech_recon2 = 1
			wartime_train = 1
			armored_train = 1
			improved_light_tank_chassis = 1
			armor_tech_2 = 1
			engine_tech_2 = 1
			artillery3 = 1
			antiair2 = 1
			antitank2 = 1
			improved_sonar = 1
			basic_ship_hull_cruiser = 1
			basic_cruiser_armor_scheme = 1
			basic_ship_hull_heavy = 1
			early_ship_hull_carrier = 1
			magnetic_detonator = 1
			basic_light_battery = 1
			basic_medium_battery = 1
			basic_heavy_battery = 1
			fighter3 = 1
			CAS2 = 1
			naval_bomber2 = 1
			strategic_bomber1 = 1
			mechanical_computing = 1
			decimetric_radar = 1
			basic_fire_control_system = 1
			advanced_machine_tools = 1
			advanced_machine_tools = 1
			dispersed_industry3 = 1
			construction3 = 1
			fuel_refining3 = 1
			synth_oil_experiments = 1
			prepared_defense = 1
			low_echelon_support = 1
			hunter_killer_groups = 1
		}
	}
	1942.11.19 = {
		set_technology = {
			support_weapons3 = 1
			advanced_infantry_weapons = 1
			infantry_at = 1
			paratroopers2 = 1
			marines3 = 1
			tech_mountaineers3 = 1
			survival_training = 1
			tech_engineers3_1 = 1
			tech_flamethrower2 = 1
			tech_military_police2 = 1
			tech_maintenance_company2 = 1
			tech_field_hospital2 = 1
			tech_logistics_company3 = 1
			tech_signal_company2 = 1
			improved_medium_tank_chassis = 1
			improved_heavy_tank_chassis = 1
			armor_tech_3 = 1
			engine_tech_3 = 1
			rocket_artillery = 1
			artillery4 = 1
			improved_depth_charges = 1
			basic_heavy_armor_scheme = 1
			basic_ship_hull_submarine = 1
			basic_light_shell = 1
			basic_medium_shell = 1
			basic_heavy_shell = 1
			improved_secondary_battery = 1
			damage_control_1 = 1
			fire_control_methods_1 = 1
			tactical_bomber2 = 1
			fighter4 = 1
			heavy_fighter1 = 1
			computing_machine = 1
			dispersed_industry4 = 1
			construction4 = 1
			excavation2 = 1
			fuel_refining4 = 1
			grand_assault = 1
			dispersed_fighting = 1
			floating_fortress = 1
		}
	}
	1943.7.11 = {
		set_technology = {
			support_weapons4 = 1
			armored_car2 = 1
			jaegers = 1
			shocktroops = 1
			tech_recon3 = 1
			advanced_light_tank_chassis = 1
			artillery5 = 1
			antiair3 = 1
			antitank3 = 1
			improved_ship_hull_light = 1
			improved_cruiser_armor_scheme = 1
			improved_heavy_armor_scheme = 1
			improved_ship_torpedo_launcher = 1
			improved_ship_hull_submarine = 1
			basic_submarine_snorkel = 1
			improved_light_battery = 1
			improved_medium_battery = 1
			improved_heavy_battery = 1
			dp_secondary_battery = 1
			mtg_landing_craft = 1
			infantry_offensive = 1
			CAS3 = 1
			naval_bomber3 = 1
			tactical_bomber3 = 1
			strategic_bomber2 = 1
			scout_plane2 = 1
			improved_computing_machine = 1
			improved_decimetric_radar = 1
			improved_fire_control_system = 1
			assembly_line_production = 1
			assembly_line_production = 1
			oil_processing = 1
			rubber_processing = 1
			operational_destruction = 1
			convoy_sailing = 1
		}
	}
	1944.6.6 = {
		set_technology = {
			night_vision = 1
			advanced_infantry_weapons2 = 1
			infantry_at2 = 1
			desertinfantry_at3 = 1
			tech_military_police3 = 1
			tech_maintenance_company3 = 1
			tech_field_hospital3 = 1
			tech_signal_company3 = 1
			armor_tech_4 = 1
			engine_tech_4 = 1
			artillery_a = 1
			rocket_artillery2 = 1
			antiair4 = 1
			antitank4 = 1
			improved_ship_hull_cruiser = 1
			improved_ship_hull_heavy = 1
			basic_ship_hull_carrier = 1
			electric_torpedo = 1
			advanced_ship_torpedo_launcher = 1
			improved_light_shell = 1
			improved_medium_shell = 1
			improved_heavy_shell = 1
			damage_control_2 = 1
			fire_control_methods_2 = 1
			fighter5 = 1
			CAS4 = 1
			naval_bomber4 = 1
			heavy_fighter2 = 1
			strategic_bomber3 = 1
			advanced_computing_machine = 1
			dispersed_industry5 = 1
			construction5 = 1
			excavation3 = 1
			fuel_refining5 = 1
			armored_operations = 1
			fighter_veteran_initiative = 1
			convoy_escorts = 1
		}
	}
	1944.12.6 = {
		set_technology = {
			armored_car3 = 1
			paratroopers3 = 1
			marines4 = 1
			tech_mountaineers4 = 1
			jaegers2 = 1
			shocktroops2 = 1
			elite_forces = 1
			tech_engineers4 = 1
			tech_flamethrower3 = 1
			tech_logistics_company4 = 1
			advanced_medium_tank_chassis = 1
			advanced_heavy_tank_chassis = 1
			artillery_b = 1
			homing_torpedo = 1
			heavy_fighter3 = 1
			centimetric_radar = 1
			advanced_fire_control_system = 1
			flexible_line = 1
			streamlined_line = 1
			improved_oil_processing = 1
			improved_rubber_processing = 1
			infiltration_assault = 1
			naval_strike_torpedo_tactics_oi = 1
			escort_carriers = 1
		}
	}
	1945.1.1 = {
		set_technology = {
			advanced_infantry_weapons_a = 1
			infantry_at_a = 1
			tech_recon4 = 1
			semi_modern_light_tank_chassis = 1
			artillery_c = 1
			rocket_artillery3 = 1
			antiair5 = 1
			antitank5 = 1
			damage_control_3 = 1
			fire_control_methods_3 = 1
			tactical_bomber4 = 1
			strategic_bomber4 = 1
			improved_centimetric_radar = 1
			excavation4 = 1
			fuel_refining6 = 1
			night_assault_tactics = 1
			cas_veteran_initiative = 1
			integrated_convoy_defence = 1
		}
	}
#########################################################################################

#########################################################################################
######################################## Ideas ##########################################
#########################################################################################
	1936.1.1 = {
		add_ideas = {
			neutrality_idea
			free_trade
			censored_press
			age_18
		}
		if = {
			limit = {
				has_dlc = "Death or Dishonor"
			}
			add_ideas = {
				ROM_king_carol_ii_hedonist
			}
		}
	}
	1940.5.10 = {
		add_ideas = {
			partial_economic_mobilisation
			limited_conscription
			age_17
		}
	}
	1941.6.21 = {
		add_ideas = {
			war_economy
			extensive_conscription
			age_15
		}
	}
	1942.11.19 = {
		add_ideas = {

		}
	}
	1943.7.11 = {
		add_ideas = {
			service_by_requirement
			age_13
		}
	}
	1944.6.6 = {
		add_ideas = {

		}
	}

	1944.12.16 = {
		add_ideas = {
			all_adults_serve
			age_11
		}
	}
	1945.1.1 = {
		add_ideas = {

		}
	}
#########################################################################################

#########################################################################################
######################################## Focuses ########################################
#########################################################################################
	1940.5.10 = {
		complete_national_focus = ROM_preserve_greater_romania						#35
		complete_national_focus = ROM_army_maneuvers								#35
		complete_national_focus = ROM_expand_the_air_force							#30
		unlock_national_focus = ROM_civil_works										#45
		complete_national_focus = ROM_agrarian_reform								#45
		complete_national_focus = ROM_local_development								#35
		complete_national_focus = ROM_army_war_college								#35
		complete_national_focus = ROM_danubian_transport_network					#35
		complete_national_focus = ROM_malaxa										#45
		complete_national_focus = ROM_institute_royal_dictatorship					#60
		complete_national_focus = ROM_revise_the_constitution						#70
		complete_national_focus = ROM_flexible_foreign_policy						#35
		complete_national_focus = ROM_air_superiority								#35
		complete_national_focus = ROM_trade_treaty_with_germany						#35
		complete_national_focus = ROM_iar_80										#35
		complete_national_focus = ROM_cas											#35
		complete_national_focus = ROM_royal_guards_divisions						#35
		unlock_national_focus = ROM_the_zb_53										#30
		unlock_national_focus = ROM_vanatori_de_munte								#70
		complete_national_focus = ROM_hunedoara_steel_works							#60
		complete_national_focus = ROM_appoint_german_friendly_government			#45
		complete_national_focus = ROM_invite_german_advisors						#70
		complete_national_focus = ROM_iron_guard									#35
		complete_national_focus = ROM_join_axis										#10
		unlock_national_focus = ROM_the_armored_division							#35
		complete_national_focus = ROM_expand_ploiesti_oil_production				#45
		complete_national_focus = ROM_german_romanian_oil_exploitation_company		#45
		complete_national_focus = ROM_expand_the_university_of_bucharest			#70
		complete_national_focus = ROM_acquire_modern_tanks							#35
		complete_national_focus = ROM_invest_in_the_iar								#70
		unlock_national_focus = ROM_artillery_modernization							#35
		complete_national_focus = ROM_mobile_tank_destroyers						#35
		complete_national_focus = ROM_mountain_artillery							#35
		complete_national_focus = ROM_national_defense_industry						#60
		complete_national_focus = ROM_expand_the_marine_regiment					#70
		complete_national_focus = ROM_fortify_the_borders							#30
	}

	1941.6.22 = {
		complete_national_focus = ROM_force_abdication								#70
		complete_national_focus = ROM_the_royal_foundation							#70
	}

	1943.7.11 = {
		complete_national_focus = ROM_the_maresal									#35
		complete_national_focus = ROM_exploit_the_baita_mines						#70
		complete_national_focus = ROM_a_deal_with_the_devil							#35
		complete_national_focus = ROM_form_peasant_militias							#45
	}

	1944.12.16 = {
		complete_national_focus = ROM_king_michaels_coup							#45
		complete_national_focus = ROM_appoint_allied_friendly_government			#45
		unlock_national_focus = ROM_basing_rights_for_soviet_union					#45
		unlock_national_focus = ROM_join_comintern									#45
	}
#########################################################################################

#########################################################################################
####################################### Settings ########################################
#########################################################################################
	### Settings
		set_country_flag = wants_major_news_events
		set_country_flag = wants_medium_news_events
		set_country_flag = wants_minor_news_events

		set_country_flag = wants_fallen_city_newsevents

		set_global_flag = player_wants_axis_resistance
		set_global_flag = player_wants_allies_resistance
	###

	### Bridges ###
		add_dynamic_modifier = { modifier = blowing_bridge_dynamic_modifier }
		add_dynamic_modifier = { modifier = repairing_bridge_dynamic_modifier }
	###
#########################################################################################

#########################################################################################
################################### Equipment Variants ##################################
	# Submarines #
	create_equipment_variant = {
		name = "Delfinul Class"
		type = ship_hull_submarine_2
		name_group = ROM_SS_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_torpedo_slot = ship_torpedo_sub_1
			fixed_ship_engine_slot = sub_ship_engine_1
			rear_1_custom_slot = ship_mine_layer_sub
		}
	}
	# Destroyers #
	create_equipment_variant = {
		name = "Marasti Class"
		type = ship_hull_light_1
		name_group = ROM_DD_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = light_ship_engine_1
			fixed_ship_torpedo_slot = ship_torpedo_1
			mid_1_custom_slot = ship_mine_layer_1
			rear_1_custom_slot = empty
		}
		obsolete = yes
	}
	create_equipment_variant = {
		name = "Regele Ferdinand Class"
		type = ship_hull_light_1
		name_group = ROM_DD_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = light_ship_engine_1
			fixed_ship_torpedo_slot = ship_torpedo_1
			mid_1_custom_slot = ship_mine_layer_1
			rear_1_custom_slot = ship_depth_charge_1
		}
	}
#########################################################################################

#########################################################################################
######################################## OOBs ###########################################
#########################################################################################
	oob = "_ROM_1936"
	set_naval_oob = "naval_mtg_ROM_1936"

	1940.1.1 = {
		oob = "_ROM_1940"
		set_naval_oob = "naval_mtg_ROM_1940"
	}

	1941.6.21 = {
		oob = "_ROM_1941"
		set_naval_oob = "naval_mtg_ROM_1941"
	}

	1942.11.19 = {
		oob = "_ROM_1942_11"
		set_naval_oob = "naval_mtg_ROM_1942_11"
	}

	1943.1.1 = {
		oob = "_ROM_1943"
		set_naval_oob = "naval_mtg_ROM_1943"
	}

	1944.6.1 = {
		oob = "_ROM_1944_6"
		set_naval_oob = "naval_mtg_ROM_1944_6"
	}



	1944.12.1 = {
		oob = "_ROM_1944_12"
		set_naval_oob = "naval_mtg_ROM_1944_12"
	}

	1945.1.1 = {
		oob = "_ROM_1945"
		set_naval_oob = "naval_mtg_ROM_1945"
	}
#########################################################################################
