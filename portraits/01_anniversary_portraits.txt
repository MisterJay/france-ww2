continent = {
	name = europe
	army = {
		male = {
			"gfx/leaders/Europe/Portrait_europe_generic_land_6.dds"
			"gfx/leaders/Europe/portrait_europe_generic_land_7.dds"
			"gfx/leaders/Europe/portrait_europe_generic_land_8.dds"
		}
	}

	navy = {
		male = {
			"gfx/leaders/Europe/portrait_europe_generic_navy_4.dds"
			"gfx/leaders/Europe/portrait_europe_generic_navy_5.dds"
		}
	}

	political = {
		communism = {
				male = {
					"gfx/leaders/Europe/portrait_europe_generic_4.dds"
					"gfx/leaders/Europe/portrait_europe_generic_5.dds"
					"gfx/leaders/Europe/portrait_europe_generic_6.dds"
					"gfx/leaders/Europe/Portrait_europe_generic_7.dds"
					"gfx/leaders/Europe/portrait_europe_generic_8.dds"
			}
		}
		democratic = {
				male = {
					"gfx/leaders/Europe/portrait_europe_generic_4.dds"
					"gfx/leaders/Europe/portrait_europe_generic_5.dds"
					"gfx/leaders/Europe/portrait_europe_generic_6.dds"
					"gfx/leaders/Europe/Portrait_europe_generic_7.dds"
					"gfx/leaders/Europe/portrait_europe_generic_8.dds"
			}
		}
		fascism = {
				male = {
					"gfx/leaders/Europe/portrait_europe_generic_4.dds"
					"gfx/leaders/Europe/portrait_europe_generic_5.dds"
					"gfx/leaders/Europe/portrait_europe_generic_6.dds"
					"gfx/leaders/Europe/Portrait_europe_generic_7.dds"
					"gfx/leaders/Europe/portrait_europe_generic_8.dds"
			}
		}
		neutrality = {
				male = {
					"gfx/leaders/Europe/portrait_europe_generic_4.dds"
					"gfx/leaders/Europe/portrait_europe_generic_5.dds"
					"gfx/leaders/Europe/portrait_europe_generic_6.dds"
					"gfx/leaders/Europe/Portrait_europe_generic_7.dds"
					"gfx/leaders/Europe/portrait_europe_generic_8.dds"
			}
		}
	}
}

GER = {
	army = {
		male = {
			"gfx/leaders/GER/Portrait_Germany_Generic_land_1.dds"
			"gfx/leaders/GER/Portrait_Germany_Generic_land_2.dds"
			"gfx/leaders/GER/Portrait_Germany_Generic_land_3.dds"
			"gfx/leaders/GER/Portrait_Germany_Generic_land_4.dds"
			"gfx/leaders/GER/Portrait_Germany_Generic_land_5.dds"
		}
	}

	navy = {
		male = {
			"gfx/leaders/GER/Portrait_Germany_Generic_navy_1.dds"
			"gfx/leaders/GER/Portrait_Germany_Generic_navy_2.dds"
			"gfx/leaders/GER/Portrait_Germany_Generic_navy_3.dds"
		}
	}
}

ENG = {
	army = {
		male = {
			"gfx/leaders/ENG/Portrait_Britain_Generic_land_1.dds"
			"gfx/leaders/ENG/Portrait_Britain_Generic_land_2.dds"
			"gfx/leaders/ENG/Portrait_Britain_Generic_land_3.dds"
			"gfx/leaders/ENG/Portrait_Britain_Generic_land_4.dds"
			"gfx/leaders/ENG/Portrait_Britain_Generic_land_5.dds"
		}
	}

	navy = {
		male = {
			"gfx/leaders/ENG/Portrait_Britain_Generic_navy_1.dds"
			"gfx/leaders/ENG/Portrait_Britain_Generic_navy_2.dds"
			"gfx/leaders/ENG/Portrait_Britain_Generic_navy_3.dds"
		}
	}
}

FRA = {
	army = {
		male = {
			"gfx/leaders/FRA/Portrait_France_Generic_land_1.dds"
			"gfx/leaders/FRA/Portrait_France_Generic_land_2.dds"
			"gfx/leaders/FRA/Portrait_France_Generic_land_3.dds"
			"gfx/leaders/FRA/Portrait_France_Generic_land_4.dds"
			"gfx/leaders/FRA/Portrait_France_Generic_land_5.dds"
		}
	}

	navy = {
		male = {
			"gfx/leaders/FRA/Portrait_France_Generic_navy_1.dds"
			"gfx/leaders/FRA/Portrait_France_Generic_navy_2.dds"
			"gfx/leaders/FRA/Portrait_France_Generic_navy_3.dds"
		}
	}
}

ITA = {
	army = {
		male = {
			"gfx/leaders/ITA/Portrait_Italy_Generic_land_1.dds"
			"gfx/leaders/ITA/Portrait_Italy_Generic_land_2.dds"
			"gfx/leaders/ITA/Portrait_Italy_Generic_land_3.dds"
		}
	}

	navy = {
		male = {
			"gfx/leaders/ITA/Portrait_Italy_Generic_navy_1.dds"
			"gfx/leaders/ITA/Portrait_Italy_Generic_navy_2.dds"
			"gfx/leaders/ITA/Portrait_Italy_Generic_navy_3.dds"
		}
	}
}

SOV = {
	army = {
		male = {
			"gfx/leaders/SOV/Portrait_Soviet_Generic_land_1.dds"
			"gfx/leaders/SOV/Portrait_Soviet_Generic_land_2.dds"
			"gfx/leaders/SOV/Portrait_Soviet_Generic_land_3.dds"
			"gfx/leaders/SOV/Portrait_Soviet_Generic_land_4.dds"
			"gfx/leaders/SOV/Portrait_Soviet_Generic_land_5.dds"
			"gfx/leaders/SOV/portrait_soviet_pdxcon_magic_winner.dds"
		}
	}

	navy = {
		male = {
			"gfx/leaders/SOV/Portrait_Soviet_Generic_navy_1.dds"
			"gfx/leaders/SOV/Portrait_Soviet_Generic_navy_2.dds"
			"gfx/leaders/SOV/Portrait_Soviet_Generic_navy_3.dds"
		}
	}
}

USA = {
	army = {
		male = {
			"gfx/leaders/USA/Portrait_USA_Generic_land_1.dds"
			"gfx/leaders/USA/Portrait_USA_Generic_land_2.dds"
			"gfx/leaders/USA/Portrait_USA_Generic_land_3.dds"
			"gfx/leaders/USA/Portrait_USA_Generic_land_4.dds"
			"gfx/leaders/USA/Portrait_USA_Generic_land_5.dds"
		}
	}

	navy = {
		male = {
			"gfx/leaders/USA/Portrait_USA_Generic_navy_1.dds"
			"gfx/leaders/USA/Portrait_USA_Generic_navy_2.dds"
			"gfx/leaders/USA/Portrait_USA_Generic_navy_3.dds"
		}
	}
}

SPR = {
	army = {
		male = {
			"gfx/leaders/Europe/Portrait_Europe_Generic_land_1.dds"
			"gfx/leaders/Europe/Portrait_Europe_Generic_land_2.dds"
			"gfx/leaders/Europe/Portrait_Europe_Generic_land_3.dds"
			"gfx/leaders/Europe/Portrait_Europe_Generic_land_4.dds"
			"gfx/leaders/Europe/Portrait_Europe_Generic_land_5.dds"
		}
	}

	navy = {
		male = {
			"gfx/leaders/Europe/Portrait_Europe_Generic_navy_1.dds"
			"gfx/leaders/Europe/Portrait_Europe_Generic_navy_2.dds"
			"gfx/leaders/Europe/Portrait_Europe_Generic_navy_3.dds"
		}
	}
}
