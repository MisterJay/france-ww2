##############################
##### Installation Guide #####
##############################

You just got Primo Victoria from Gitlab (either through cloning or direct download), and you want to play the mod.
There are a few steps you need to take.

First, right click "primo_victoria.mod" in Windows File Explorer, and click "Edit". It will now open in Notepad. Make sure the path is correct (path="mod/france-ww2" by default). For example, if the folder is called france-ww2-master, you will have to change path to path="mod/france-ww2-master".

Secondly, copy "primo_victoria.mod" to "Paradox Interactive\Hearts of Iron IV\mod". Now you should be able to play the mod.