#Prepares the bridge for being blown up
blow_bridge = {
    set_state_flag = bridge_being_blown
    #Amount of factories it costs
    ROOT = { add_to_variable = { amount_of_bridges_being_blown = 25 } }
    state_event = { id = dday_state_event.1 days = 10 trigger_for = ROOT }
}

#Prepares the repair of the bridge
repair_bridge = {
    set_state_flag = bridge_being_repaired
    # Actually repairs the bridge, amount of factories it costs
    ROOT = { add_to_variable = { amount_of_bridges_being_repaired = 25 } }
    state_event = { id = dday_state_event.2 days = 10 trigger_for = ROOT }
}

blow_bridge_non_gui = {
    set_state_flag = bridge_blown
    set_state_flag = { flag = bridge_recently_blown value = 1 days = 10 }
    clr_state_flag = bridge_being_blown
    BRI = {
        set_state_owner = PREV
        set_state_controller = PREV
    }
}
