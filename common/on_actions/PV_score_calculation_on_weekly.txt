on_actions = {
    on_startup = {
        effect = {
            set_variable = { global.ticker_weight_var = 2 }
            set_variable = { global.nukes_dropped_weight_var = 50 }
            set_variable = { global.industry_weight_var = 0.1 }
            set_variable = { global.score_france = 400 }
            set_variable = { global.score_germany = 350 }
        }
    }

    on_weekly_GER = {
        effect = {
            add_to_variable = { global.time_score_var = 10 }
            #Works on the fact that casualties dont go down, should be relatively performance light
            #Casualties Germany
                while_loop_effect = {
                    limit = {
                        meta_trigger = {
                            text = { casualties > [CASUALTIES_AMOUNT] }
                            CASUALTIES_AMOUNT = "[?german_casualties_var]"
                        }
                    }
                    add_to_variable = { german_casualties_var = 10000 }
                    add_to_variable = { global.german_casualties_score_var = 10 }
                }
            #

            #Casualties France and allies
                every_country = {
                    limit = { is_in_faction_with = FRA }
                    while_loop_effect = {
                        limit = {
                            meta_trigger = {
                                text = { casualties > [CASUALTIES_AMOUNT] }
                                CASUALTIES_AMOUNT = "[?casualties_var]"
                            }
                        }
                        add_to_variable = { casualties_var = 10000 }
                        add_to_variable = { global.french_casualties_score_var = 10 }
                    }
                }
            #

            # Score Normalisation #
                set_variable = { german_west_ticker_normalised_var              = global.german_ticker_2 }
                set_variable = { german_east_ticker_normalised_var              = global.german_ticker }
                set_variable = { german_industry_normalised_var                 = num_of_factories }
                set_variable = { german_nukes_dropped_normalised_var            = nukes_dropped }
                set_variable = { german_nukes_dropped_on_us_normalised_var      = nukes_dropped_on_friendly_territory }

                set_variable = { FRA.french_west_ticker_normalised_var          = global.allied_ticker }
                set_variable = { FRA.french_nukes_dropped_normalised_var        = FRA.nukes_dropped }
                set_variable = { FRA.french_nukes_dropped_on_us_normalised_var  = FRA.nukes_dropped_on_friendly_territory }
                set_variable = { FRA.french_industry_normalised_var             = FRA.num_of_factories }

                multiply_variable = { german_west_ticker_normalised_var         = global.ticker_weight_var }
                multiply_variable = { german_east_ticker_normalised_var         = global.ticker_weight_var }
                multiply_variable = { german_nukes_dropped_normalised_var       = global.nukes_dropped_weight_var }
                multiply_variable={german_nukes_dropped_on_us_normalised_var    = global.nukes_dropped_weight_var }
                multiply_variable = { german_industry_normalised_var            = global.industry_weight_var }
                round_variable = german_industry_normalised_var

                multiply_variable = { FRA.french_west_ticker_normalised_var     = global.ticker_weight_var }
                multiply_variable = { FRA.french_nukes_dropped_normalised_var   = global.nukes_dropped_weight_var }
                multiply_variable={FRA.french_nukes_dropped_on_us_normalised_var= global.nukes_dropped_weight_var }
                multiply_variable = { FRA.french_industry_normalised_var        = global.industry_weight_var }
                round_variable = FRA.french_industry_normalised_var
            #

            # Casualty total #
                set_variable = { current_score_casualties = global.french_casualties_score_var }
                subtract_from_variable = { current_score_casualties = global.german_casualties_score_var }
            #

            # German Score #
                set_variable = { germany_current_score = current_score_casualties }
                add_to_variable = { germany_current_score = german_west_ticker_normalised_var }
                add_to_variable = { germany_current_score = german_east_ticker_normalised_var }
                add_to_variable = { germany_current_score = german_nukes_dropped_normalised_var }
                add_to_variable = { germany_current_score = german_industry_normalised_var }

                add_to_variable = { germany_current_score = global.time_score_var }

                if = {
                    limit = { has_global_flag = normandy_landings_defeated }
                    add_to_variable = { germany_current_score = 300 }
                }

                if = {
                    limit = { has_global_flag = early_peace_negotiated }
                    add_to_variable = { germany_current_score = 100 }
                }

                if = {
                    limit = { has_global_flag = fall_of_berlin }
                    subtract_from_variable = { germany_current_score = 300 }
                }
            #

            #French Score #
                set_variable = { france_current_score = 300 } #Starting score of France, since their time goes down instead of up
                subtract_from_variable = { france_current_score = current_score_casualties }
                subtract_from_variable = { france_current_score = global.time_score_var }
                add_to_variable = { france_current_score = FRA.french_west_ticker_normalised_var }
                add_to_variable = { france_current_score = FRA.french_nukes_dropped_normalised_var }
                add_to_variable = { france_current_score = FRA.french_industry_normalised_var }

                if = {
                    limit = { has_global_flag = paris_liberated }
                    add_to_variable = { france_current_score = 200 }
                }

                #TODO
                if = {
                    limit = { has_global_flag = westwall_penetrated }
                    add_to_variable = { france_current_score = 100 }
                }

                if = {
                    limit = { has_global_flag = brussels_liberated }
                    add_to_variable = { france_current_score = 100 }
                }
            #

            # Weekly difference:
                set_variable = { global.germany_weekly_difference = germany_current_score }
                set_variable = { global.france_weekly_difference = france_current_score }

                subtract_from_variable = { global.germany_weekly_difference = global.germany_last_value_score }
                subtract_from_variable = { global.france_weekly_difference = global.france_last_value_score }
            #

            # Final Score Variables #
                set_variable = { global.score_germany = germany_current_score }
                set_variable = { global.score_france = france_current_score }
            #

            # For weekly difference
                set_variable = { global.germany_last_value_score = global.score_germany }
                set_variable = { global.france_last_value_score = global.score_france }
            #

            # if negative multiply by -1 #

            # Logging #
                # log = "Score overview Calculations:"
                # log = "Casualties Germany: [?global.german_casualties_normalised_var]"
    			# log = "Casualties France: [?global.french_casualties_normalised_var]"
                # log = "Ticker Germany West: [?german_west_ticker_normalised_var]"
                # log = "Ticker Germany East: [?german_east_ticker_normalised_var]"
    			# log = "Ticker France: [?french_west_ticker_normalised_var]"
                # log = "Nukes Germany: [?german_nukes_dropped_normalised_var]"
    			# log = "Nukes France: [?french_nukes_dropped_normalised_var]"
                # log = "Industry Germany: [?german_industry_normalised_var]"
    			# log = "Industry France: [?french_industry_normalised_var]"
    			# log = "Time Score: [?time_score_normalised_var]"
                #
                # log = " "
    			# log = "Score Germany: [?global.score_germany]"
    			# log = "Score France: [?global.score_france]"
                # log = " "
            #
        }
    }
}
