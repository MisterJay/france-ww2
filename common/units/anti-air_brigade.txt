# options for map_icon_category:
# For land units: infantry,armored,other
# For sea units: ship,transport,uboat

sub_units = {

	anti_air_brigade = {
		sprite = infantry
		map_icon_category = infantry
		priority = 301
		ai_priority = 10
		active = yes

		type = {
			infantry
			anti_air
		}

		group = infantry

		categories = {
			category_army
			category_line_artillery
			category_anti_air
		}

		combat_width = 1.5

		#Size Definitions
		max_organisation = 40
		default_morale = 0.1
		max_strength = 0.1
		manpower = 350
		training_time = 50
		weight = 0.3

		supply_consumption = 0.03

		need = {
			anti_air_equipment = 10
		}

		forest = {
			attack = -0.1
			movement = -0.1
		}

		river = {
			attack = -0.2
			movement = -0.2
		}

		amphibious = {
			attack = -0.3
		}
	}
	mot_anti_air_brigade = {
		sprite = infantry
		map_icon_category = infantry
		priority = 301
		ai_priority = 10
		active = yes

		type = {
			motorized
			anti_air
		}

		group = mobile

		categories = {
			category_army
			category_line_artillery
			category_anti_air
		}

		combat_width = 1.5

		#Size Definitions
		max_organisation = 31
		default_morale = 0.1
		max_strength = 1.2
		manpower = 350
		training_time = 50
		weight = 0.3

		supply_consumption = 0.3
		transport = motorized_equipment
		need = {
			anti_air_equipment = 10
			motorized_equipment = 25
		}

		forest = {
			attack = -0.1
			movement = -0.3
		}

		marsh = {
			attack = -0.2
			movement = -0.2
		}

		river = {
			attack = -0.2
			movement = -0.2
		}

		amphibious = {
			attack = -0.4
		}
	}
}
