scripted_gui = {
	scripted_gui_score = {
		context_type = player_context

		window_name = "score_scripted_gui_options_container"

		visible = {
            is_ai = no
			check_variable = { show_score_scripted_gui > 0.0 }
		}

		effects = {
			score_close_button_click = {
				set_variable = { show_score_scripted_gui = 0 }
			}
		}

		triggers = {
		}
	}
}
