can_lend_lease_infantry_and_artillery = {
    OR = {
        can_lend_lease_infantry = yes
        can_lend_lease_artillery = yes
    }
    country_exists = var:receiver
}

can_send_equipment_barbarossa = {
    custom_trigger_tooltip = {
        tooltip = can_send_equipment_barbarossa_tt
        AND = {
            is_in_faction_with = GER
            has_global_flag = operation_barbarossa_started
            check_variable = { receiver = SOV.id }
        }
    }
}

can_lend_lease_tanks = {
    OR = {
        has_country_flag = can_lend_lease_tanks
        can_send_equipment_barbarossa = yes
    }
    country_exists = var:receiver
}
can_lend_lease_aircraft = {
    OR = {
        has_country_flag = can_lend_lease_aircraft
        can_send_equipment_barbarossa = yes
    }
    country_exists = var:receiver
}

can_lend_lease_infantry = {
    OR = {
        has_country_flag = can_lend_lease_infantry
        can_send_equipment_barbarossa = yes
    }
}

can_lend_lease_artillery = {
    OR = {
        has_country_flag = can_lend_lease_artillery
        can_send_equipment_barbarossa = yes
    }
}

can_lend_lease_more = {

}

is_lend_leasing_anything_to_receiver = {
    OR = {
        is_lend_leasing_infantry_or_artillery = yes
        is_lend_leasing_armour = yes
        is_lend_leasing_air = yes
    }
}

is_lend_leasing_infantry_or_artillery = {
    OR = {
        is_lend_leasing_infantry = yes
        is_lend_leasing_artillery = yes
    }
}

is_lend_leasing_infantry = {
    OR = {
        check_variable = { infantry_level_@var:receiver > 0 }
        check_variable = { support_level_@var:receiver > 0 }
        check_variable = { motorised_level_@var:receiver > 0 }
        check_variable = { mechanised_level_@var:receiver > 0 }
        check_variable = { amphibious_mechanised_level_@var:receiver > 0 }
        check_variable = { armored_car_level_@var:receiver > 0 }
        check_variable = { fuel_level_@var:receiver > 0 }
    }
}

is_lend_leasing_artillery = {
    OR = {
        check_variable = { artillery_level_@var:receiver > 0 }
        check_variable = { rocket_artillery_level_@var:receiver > 0 }
        check_variable = { motorised_rocket_artillery_level_@var:receiver > 0 }
        check_variable = { anti_air_level_@var:receiver > 0 }
        check_variable = { anti_tank_level_@var:receiver > 0 }
    }
}

is_lend_leasing_armour = {
    OR = {
        check_variable = { light_tank_level_@var:receiver > 0 }
        check_variable = { LTD_level_@var:receiver > 0 }
        check_variable = { LSPAA_level_@var:receiver > 0 }
        check_variable = { LSPG_level_@var:receiver > 0 }
        check_variable = { medium_tank_level_@var:receiver > 0 }
        check_variable = { MTD_level_@var:receiver > 0 }
        check_variable = { MSPAA_level_@var:receiver > 0 }
        check_variable = { MSPG_level_@var:receiver > 0 }
        check_variable = { heavy_tank_level_@var:receiver > 0 }
        check_variable = { HTD_level_@var:receiver > 0 }
        check_variable = { HSPAA_level_@var:receiver > 0 }
        check_variable = { HSPG_level_@var:receiver > 0 }
        check_variable = { superheavy_tank_level_@var:receiver > 0 }
        check_variable = { SHTD_level_@var:receiver > 0 }
        check_variable = { SHSPAA_level_@var:receiver > 0 }
        check_variable = { SHSPG_level_@var:receiver > 0 }
        check_variable = { amphibious_tank_level_@var:receiver > 0 }
    }
}

is_lend_leasing_air = {
    OR = {
        check_variable = { CAS_level_@var:receiver > 0 }
        check_variable = { cv_CAS_level_@var:receiver > 0 }
        check_variable = { fighter_level_@var:receiver > 0 }
        check_variable = { cv_fighter_level_@var:receiver > 0 }
        check_variable = { naval_bomber_level_@var:receiver > 0 }
        check_variable = { cv_naval_bomber_level_@var:receiver > 0 }
        check_variable = { heavy_fighter_level_@var:receiver > 0 }
        check_variable = { tactical_bomber_level_@var:receiver > 0 }
        check_variable = { strategic_bomber_level_@var:receiver > 0 }
        check_variable = { transport_plane_level_@var:receiver > 0 }
        check_variable = { scout_plane_level_@var:receiver > 0 }
        check_variable = { jet_fighter_level_@var:receiver > 0 }
        check_variable = { jet_tactical_bomber_level_@var:receiver > 0 }
        check_variable = { jet_strategic_bomber_level_@var:receiver > 0 }
    }
}

should_not_get_war_attrition = {
    OR = {
        has_war = no
        AND = {
            original_tag = USA
            var:receiver = { original_tag = CAN }
        }
        AND = {
            is_in_faction_with = GER
            GER = { has_government = fascism }
        }
    }
}

eastern_front_scripted_ll = {
    ROOT = { is_in_faction_with = GER }
    var:receiver = { original_tag = SOV }
}

has_enough_manpower_for_click = {
    set_temp_variable = { compare_variable = temp_var:click_amount_value }
    multiply_temp_variable = { compare_variable = temp_var:click_manpower_value }
    check_variable = { manpower > temp_var:compare_variable }
}

has_enough_equipment_level = {
    subtract_from_temp_variable = { equipment_value = click_value }
    check_variable = { equipment_value > -1 }
}

has_lend_lease_equipment_in_stockpile = {
    meta_trigger = {
        text = { check_variable = { num_equipment@[EQ_TYPE]_[EQ_LEVEL] > 0 } }
        EQ_LEVEL = "[?eq_level|.0]"
        EQ_TYPE = "[THIS.get_equipment_name]"
    }
}

can_produce_next_level_equipment = {
    meta_trigger = {
        text = {
            set_temp_variable = { equipment_level_temp = equipment_level_[EQ_TYPE] }
            add_to_temp_variable = { equipment_level_temp = can_produce_equipment_level }
        }
        EQ_TYPE = "[v.get_infantry_equipment_name]"
    }
    meta_trigger = {
        text = {
            check_variable = { num_equipment@[EQ_TYPE]_[EQ_LEVEL] > 0 }
        }
        EQ_LEVEL = "[?temp_var:equipment_level_temp|.0]"
        EQ_TYPE = "[v.get_infantry_equipment_name]"
    }
}