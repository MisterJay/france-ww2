ideas = {

	country = {
		CAN_great_depression_1 = {

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = great_depression

			modifier = {
				consumer_goods_factor = 0.25
				industrial_capacity_factory = -0.30
				industrial_capacity_dockyard = -0.30
			}
		}

		CAN_great_depression_2 = {

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = great_depression

			modifier = {
				consumer_goods_factor = 0.15
				industrial_capacity_factory = -0.20
				industrial_capacity_dockyard = -0.20
			}
		}

		CAN_conscription_crisis = {

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = tfv_can_conscription_crisis

			modifier = {
				conscription_factor = -0.30
			}
		}

		CAN_conscription_crisis_mended = {

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = tfv_can_conscription_crisis

			modifier = {
				conscription_factor = -0.10
			}
		}

		CAN_defence_of_canada_regulations_democratic = {

			allowed = {
				always = no
			}

			allowed_civil_war = {
				 has_government = democratic
			}

			removal_cost = -1

			picture = can_defence_of_canada_regulations_democratic

			modifier = {
				communism_drift = -0.01
				fascism_drift = -0.01
				foreign_subversive_activites = -0.50
			}
		}

		CAN_war_bonds_1 = {

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = generic_goods_red_bonus

			modifier = {
				consumer_goods_factor = -0.03
			}
		}

		CAN_war_bonds_2 = {

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = generic_goods_red_bonus

			modifier = {
				consumer_goods_factor = -0.08
			}
		}

		CAN_national_resources_mobilization_act = {

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = generic_production_bonus

			modifier = {
				industrial_capacity_factory = 0.02
				industrial_capacity_dockyard = 0.02
				conscription_factor = 0.05
				stability_factor = -0.05
			}
		}

		CAN_wartime_prices_and_trade_board = {

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = can_wartime_prices_and_trade_board

			modifier = {
				min_export = -0.05
				global_building_slots_factor = 0.10
			}
		}

		CAN_bits_and_pieces_program = {

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = generic_production_bonus

			modifier = {
				production_factory_start_efficiency_factor = 0.05
				production_factory_efficiency_gain_factor = 0.05
				industrial_capacity_factory = 0.03
				industrial_capacity_dockyard = 0.03
			}
		}

		CAN_commit_to_the_war = {

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = generic_manpower_bonus

			modifier = {
				conscription_factor = 0.10
			}

		}

		CAN_send_in_the_zombies = {

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = can_send_in_the_zombies

			modifier = {
				conscription_factor = 0.15
				conscription = 0.01
			}
		}

		CAN_the_plan = {

			allowed = {
				always = no
			}

			allowed_civil_war = {
				is_in_faction_with = ENG
			}

			removal_cost = -1

			picture = generic_air_bonus


			equipment_bonus = {
				fighter_equipment = {
					air_range = 0.1
					air_attack = 0.05
					instant = yes
				}
				cv_fighter_equipment = {
					air_range = 0.1
					air_attack = 0.05
					instant = yes
				}
				CAS_equipment = {
					air_range = 0.1
					air_ground_attack = 0.05
					instant = yes
				}
				cv_CAS_equipment = {
					air_range = 0.1
					air_ground_attack = 0.05
					instant = yes
				}
				cv_nav_bomber_equipment = {
					air_range = 0.1
					naval_strike_attack = 0.05
					instant = yes
				}
				nav_bomber_equipment = {
					air_range = 0.1
					naval_strike_attack = 0.05
					instant = yes
				}
				tac_bomber_equipment = {
					air_range = 0.1
					air_ground_attack = 0.05
					instant = yes
				}
				heavy_fighter_equipment = {
					air_range = 0.1
					air_attack = 0.05
					instant = yes
				}
				strat_bomber_equipment = {
					air_range = 0.1
					air_bombing = 0.05
					instant = yes
				}
				transport_plane_equipment = {
					air_range = 0.1
					instant = yes
				}
			}

			modifier = {
				air_ace_generation_chance_factor = 0.10
			}
		}

		CAN_trade_fleet = {

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = escort_effort_focus

			equipment_bonus = {
				convoy = {
					build_cost_ic = -0.25 instant = yes
				}
			}
		}
	}

	# TECHNOLOGY
	tank_manufacturer = {
		designer = yes
	}

	industrial_concern = {



		montreal_lab = {

			picture = generic_industrial_concern_1

			allowed = {
				original_TAG = CAN
				has_dlc = "Together for Victory"
			}



			research_bonus = {
				electronics = 0.10
			}

			research_bonus = {
				industry = 0.10
			}

			traits = { electronics_concern }

			modifier = {
			}
		}

		polymer_corporation = {

			picture = generic_industrial_concern_2

			allowed = {
				original_tag = CAN
				has_dlc = "Together for Victory"
			}

			research_bonus = {
				synth_resources = 0.15
				industry = 0.10
			}

			modifier = {
				fuel_gain_from_states = 0.2
			}

			traits = { refinery_concern }
		}
	}

	materiel_manufacturer = {

		designer = yes

		inglis_company = {

			picture = generic_infantry_equipment_manufacturer_2

			allowed = {
				original_TAG = CAN
				has_dlc = "Together for Victory"
			}

			cost = 100
			removal_cost = 0

			research_bonus = {
				support_tech = 0.10
			}

			equipment_bonus = {
				support_equipment = {
					build_cost_ic = -0.05 instant = yes
				}
			}

			traits = { support_equipment_manufacturer }


		}

		small_arms_limited = {

			picture = generic_infantry_equipment_manufacturer_1

			allowed = {
				original_TAG = CAN
				has_dlc = "Together for Victory"
			}

			cost = 50
			removal_cost = 0

			research_bonus = {
				infantry_weapons = 0.10
			}

			equipment_bonus = {
				infantry_equipment= {
					build_cost_ic = -0.05 instant = yes
				}
			}

			traits = { infantry_equipment_manufacturer }

					}
	}

	aircraft_manufacturer = {

		designer = yes

		havilland_canada = {
			picture = generic_air_manufacturer_1


			allowed = {
				original_tag = CAN
			}

			research_bonus = {
				air_equipment = 0.15
			}

			traits = { medium_aircraft_manufacturer }

			# heavy fighters like mosquito

			ai_will_do = {
				factor = 1
			}
		}

		canadian_car_foundry = {
			picture = generic_air_manufacturer_2


			allowed = {
				original_tag = CAN
			}

			research_bonus = {
				air_equipment = 0.15
			}

			traits = { light_aircraft_manufacturer }

			# fighter/interceptors like Hurricane

			ai_will_do = {
				factor = 1
			}
		}

		CAN_fairchild_aircraft_ltd = {
			picture = generic_air_manufacturer_2

			cost = 150

			allowed = {
				original_tag = CAN
				has_dlc = "Together for Victory"
			}

			available = {
				has_completed_focus = CAN_fund_fairchilds_development
			}

			research_bonus = {
				air_equipment = 0.15
			}

			traits = { naval_aircraft_manufacturer }

			# fighter/interceptors like Hurricane

			ai_will_do = {
				factor = 1
			}
		}

		CAN_handley_page = {
			picture = generic_air_manufacturer_3

			cost = 150

			allowed = {
				original_tag = CAN
				has_dlc = "Together for Victory"
			}

			available = {
				has_completed_focus = CAN_cookie_carriers
			}

			research_bonus = {
				air_equipment = 0.15
			}

			traits = { heavy_aircraft_manufacturer }

			# fighter/interceptors like Hurricane

			ai_will_do = {
				factor = 1
			}
		}
	}
}
