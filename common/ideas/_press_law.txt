ideas = {

	press_law = {
		law = yes
		use_list_view = yes

		free_press = {
			cost = 100
			removal_cost = -1
			cancel_if_invalid = no
			default = yes
			level = 4

			ai_will_do = {
				factor = 0
			}

			modifier = {
				stability_factor = 0.05
				drift_defence_factor = -0.1
				justify_war_goal_time = 0.50
				army_morale_factor = -0.025
				enemy_justify_war_goal_time = 0.15
			}
		}

		censored_press = {
			cost = 100
			removal_cost = -1
			cancel_if_invalid = no
			level = 3

			available = {
				OR = {
					has_government = fascism
					has_government = communism
					has_government = neutrality
					AND = {
						has_government = democratic
						has_war = yes
					}
				}
			}

			ai_will_do = {
				factor = 150

				modifier = {
					factor = 5

					OR = {
						has_government = fascism
						has_government = communism
						has_government = neutrality
					}
				}
				modifier = {
					factor = 2

					surrender_progress > 0.40
					has_government = democratic
				}
			}

			modifier = {
				drift_defence_factor = 0.05
				justify_war_goal_time = -0.10
			}
		}

		state_press = {
			cost = 100
			removal_cost = -1
			cancel_if_invalid = no
			level = 2

			available = {
				OR = {
					NOT = { has_government = democratic }
					AND = {
						has_government = democratic
						has_war = yes
					}
				}
			}

			ai_will_do = {
				factor = 0
			}

			modifier = {
				stability_factor = -0.05
				drift_defence_factor = 0.10
				army_morale_factor = 0.05
				justify_war_goal_time = -0.15
			}
		}

		propaganda_press = {
			cost = 100
			removal_cost = -1
			cancel_if_invalid = no
			level = 1

			available = {
				NOT = { has_government = democratic }
			}

			ai_will_do = {
				factor = 0

				modifier = {
					factor = 50
					OR = {
						has_government = fascism
						has_government = communism
						has_government = neutrality
					}
				}
			}

			modifier = {
				stability_factor = -0.1
				drift_defence_factor = 0.15
				army_morale_factor = 0.075
				justify_war_goal_time = -0.20
			}
		}
	}
}
