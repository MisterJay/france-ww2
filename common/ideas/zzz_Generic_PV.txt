ideas = {
	hidden_ideas = {
		fra_generic_ai_training = {

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				training_time_factor = -0.4 #DO NOT GO HIGHER THAN THIS!!! IN COMBINATION WITH THE OTHER IDEA YOU MIGHT GO OVER 100% AND BE UNABLE TO TRAIN ENTIRELY!
			}
		}
	}

	country = {
		border_nation_ns = {

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = unplayable

			modifier = {
				training_time_factor = 20
				political_power_gain = -20
				consumer_goods_factor = 1
				conscription_factor = -1
				army_org_factor = -1
				production_factory_max_efficiency_factor = -1
			}
		}
		easy_mode = {
			picture = generic_infantry_bonus

			modifier = {
				army_org_factor = 0.10
				army_attack_factor = 0.05
				army_defence_factor = 0.05
				industry_air_damage_factor = -0.15
				industrial_capacity_factory = 0.10
				industrial_capacity_dockyard = 0.10
			}
		}

		hard_mode = {
			picture = generic_infantry_bonus

			modifier = {
				army_org_factor = -0.05
				army_attack_factor = -0.05
				army_defence_factor = -0.05
				industry_air_damage_factor = 0.10
				industrial_capacity_factory = -0.10
				industrial_capacity_dockyard = -0.10
			}
		}

		extreme_hard_mode = {
			picture = generic_infantry_bonus

			modifier = {
				army_org_factor = -0.20
				army_attack_factor = -0.20
				army_defence_factor = -0.20
				industry_air_damage_factor = 0.15
				industrial_capacity_factory = -0.15
				industrial_capacity_dockyard = -0.15
			}
		}
		no_resistance_idea = {

			allowed = {
				always = no
			}

			removal_cost = -1

			modifier = {
				resistance_growth = -1
			}
		}
		fra_generic_military_industrial_complex = {

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = man_five_year_plan_industry

			modifier = {
				production_speed_buildings_factor = 0.15
				consumer_goods_factor = -0.05
			}
		}

		fra_generic_ressource_extraction = {

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = generic_exploit_mines

			modifier = {
				local_resources_factor = 0.1
			}
		}

		fra_generic_propaganda_campaigns = {

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = FRA_national_mobilization_focus

			modifier = {
				war_support_weekly = 0.02
			}
		}

		fra_generic_military_training = {

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = generic_army_war_college

			modifier = {
				weekly_manpower = 5000
				training_time_factor = -0.5
			}
		}

		fra_generic_officers_academy = {

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = general_staff

			modifier = {
				army_org = 5
				army_morale_factor = 0.1
				max_dig_in = 5
			}
		}

		fra_generic_special_forces = {

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = FRA_scw_intervention_nationalists_focus

			modifier = {
				special_forces_cap = 0.1
				special_forces_min = 100
			}
		}

		fra_generic_mass_conscription = {

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = generic_reserve_divisions

			modifier = {
				non_core_manpower = 0.13
				conscription_factor = 0.1
			}
		}

		fra_generic_infiltration_tactics = {

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = generic_secret_police

			modifier = {
				special_forces_attack_factor = 0.2
				no_supply_grace = 48
				out_of_supply_factor = -0.2
			}
		}
	}
}
