ideas = {
	country = {
		greek_mountaineers = {
			allowed = { always = no }

			allowed_civil_war = { has_government = democratic }

			removal_cost = -1

			picture = generic_pp_unity_bonus

			modifier = {
				army_core_defence_factor = 0.2
			}
		}
    }
}
