PUPPET_OF_GER = {
	color = rgb { 75 75 75 }
	color_ui = rgb { 138 155 116 }
}
GER_NOR= {
	color = rgb { 75 75 75 }
	color_ui = rgb { 138 155 116 }
}
GER_CZE= {
	color = rgb { 75 75 75 }
	color_ui = rgb { 138 155 116 }
}
USA = {
	color = rgb { 20 133 237 }
	color_ui = rgb { 87 160 255 }
}
ENG = {
	color = rgb { 160 50 50 }
	color_ui = rgb { 255 73 121 }
}
SUBJECT_OF_ENG = {
	color = rgb { 160 50 50 }
	color_ui = rgb { 255 73 121 }
}
FRA = {
	color = rgb { 0 72 255 }
	color_ui = rgb { 74 147 255 }
}
CAN = {
	color = rgb { 119 48 39 }
	color_ui = rgb { 155 62 51 }
}
AST = {
	color = rgb { 57 143 97 }
	color_ui = rgb { 74 186 126 }
}
BRA = {
	color = rgb { 76 145 63 }
	color_ui = rgb { 99 189 82 }
}
CZE = {
	color = rgb { 54 167 156 }
	color_ui = rgb { 70 217 203 }
}
GRE = {
	color = rgb { 93 181 227 }
	color_ui = rgb { 121 235 255 }
}
NOR = {
	color = rgb { 111 71 71 }
	color_ui = rgb { 144 92 92 }
}
POL = {
	color = rgb { 197 92 106 }
	color_ui = rgb { 255 120 138 }
}
SAF = {
	color = rgb { 195 107 160 }
	color_ui = rgb { 198 169 248 }
}
NZL = {
	color = rgb { 152 130 191 }
	color_ui = rgb { 198 169 248 }
}

SPR_nationalist_spain = {
	color = rgb { 200 100 31 }
	color_ui = rgb { 245 123 38 }
}
SPR_carlist_spain = {
	color = rgb { 255 128 0 }
	color_ui = rgb { 255 255 122 }
}
SPR_anarchist_spain = {
	color = rgb { 204 0 0 }
	color_ui = rgb { 35 35 35 }
}
SPR_republican_spain = {
	color = rgb { 242 205 94 }
	color_ui = rgb { 255 255 122 }
}

SPA_directory = {
	color = rgb { 200 100 31 }
	color_ui = rgb { 245 123 38 }
}

SPA_spanish_empire = {
	color = rgb { 200 100 31 }
	color_ui = rgb { 245 123 38 }
}

SPB_kingdom = {
	color = rgb { 255 128 0 }
	color_ui = rgb { 255 255 122 }
}

SPC_regional_defense_council_of_iberia = {
	color = rgb { 204 0 0 }
	color_ui = rgb { 35 35 35 }
}

SPC_global_defense_council = {
	color = rgb { 204 0 0 }
	color_ui = rgb { 35 35 35 }
}

SPC_poum = {
	color = rgb { 204 0 0 }
	color_ui = rgb { 35 35 35 }
}

FRA_VICHY = {
	color = rgb { 80 168 181 }
	color_ui = rgb {115 185 196 }
}
SPR_castille = {
	color = rgb { 200 100 31 }
	color_ui = rgb { 245 123 38 }
}

SPR_nationalist_castille = {
	color = rgb { 200 100 31 }
	color_ui = rgb { 245 123 38 }
}
SPR_carlist_castille = {
	color = rgb { 255 128 0 }
	color_ui = rgb { 255 255 122 }
}
SPR_anarchist_castille = {
	color = rgb { 204 0 0 }
	color_ui = rgb { 35 35 35 }
}
SPR_republican_castille = {
	color = rgb { 242 205 94 }
	color_ui = rgb { 255 255 122 }
}
