defined_text = {
	name = score_overview
	text = {
		trigger = { is_france_or_mp_country = yes }
		localization_key = SCORE_OVERVIEW_FRA
	}
	text = {
		trigger = { original_tag = GER }
		localization_key = SCORE_OVERVIEW_GER
	}
	text = {
		localization_key = SCORE_OVERVIEW_NONE
	}
}

defined_text = {
	name = score_modifiers_positive
	text = {
		trigger = { is_france_or_mp_country = yes }
		localization_key = SCORE_MODIFIERS_POSITIVE_FRANCE
	}
	text = {
		trigger = { original_tag = GER }
		localization_key = SCORE_MODIFIERS_POSITIVE_GERMANY
	}
	text = {
		localization_key = SCORE_OVERVIEW_NONE
	}
}

defined_text = {
	name = score_modifiers_negative
	text = {
		trigger = { is_france_or_mp_country = yes }
		localization_key = SCORE_MODIFIERS_NEGATIVE_FRANCE
	}
	text = {
		trigger = { original_tag = GER }
		localization_key = SCORE_MODIFIERS_NEGATIVE_GERMANY
	}
	text = {
		localization_key = SCORE_OVERVIEW_NONE
	}
}

defined_text = {
	name = score_change
	text = {
		trigger = { is_france_or_mp_country = yes }
		localization_key = [score_change_fra]
	}
	text = {
		trigger = { original_tag = GER }
		localization_key = [score_change_ger]
	}
	text = {
		localization_key = SCORE_OVERVIEW_NONE
	}
}

defined_text = {
	name = score_change_fra
	text = {
		trigger = { check_variable = { global.france_weekly_difference > -1 } }
		localization_key = SCORE_POSITIVE_FRANCE
	}
	text = {
		trigger = { check_variable = { global.france_weekly_difference < 0 } }
		localization_key = SCORE_NEGATIVE_FRANCE
	}
}

defined_text = {
	name = score_change_ger
	text = {
		trigger = { check_variable = { global.germany_weekly_difference > -1 } }
		localization_key = SCORE_POSITIVE_GERMANY
	}
	text = {
		trigger = { check_variable = { global.germany_weekly_difference < 0 } }
		localization_key = SCORE_NEGATIVE_GERMANY
	}
}

defined_text = {
	name = score_west_ticker
	text = {
		trigger = {
			original_tag = GER
			check_variable = { german_west_ticker_normalised_var > 0 }
		}
		localization_key = SCORE_WEST_TICKER_GER_POSITIVE
	}
	text = {
		trigger = {
			is_france_or_mp_country = yes
			check_variable = { FRA.french_west_ticker_normalised_var > 0 }
		}
		localization_key = SCORE_WEST_TICKER_FRA_POSITIVE
	}
}

defined_text = {
	name = score_east_ticker_ger
	text = {
		trigger = { check_variable = { german_east_ticker_normalised_var > 0 } }
		localization_key = SCORE_EAST_TICKER_GER_POSITIVE
	}
}

defined_text = {
	name = score_casualties_positive
	text = {
		trigger = {
			original_tag = GER
			check_variable = { global.french_casualties_score_var > 0 }
		}
		localization_key = SCORE_CASUALTIES_GER_POSITIVE
	}
	text = {
		trigger = {
			is_france_or_mp_country = yes
			check_variable = { global.german_casualties_score_var > 0 }
		}
		localization_key = SCORE_CASUALTIES_FRA_POSITIVE
	}
}

defined_text = {
	name = score_casualties_negative
	text = {
		trigger = {
			original_tag = GER
			check_variable = { global.german_casualties_score_var > 0 }
		}
		localization_key = SCORE_CASUALTIES_GER_NEGATIVE
	}
	text = {
		trigger = {
			is_france_or_mp_country = yes
			check_variable = { global.french_casualties_score_var > 0 }
		}
		localization_key = SCORE_CASUALTIES_FRA_NEGATIVE
	}
}

defined_text = {
	name = time_modifier
	text = {
		trigger = { original_tag = GER }
		localization_key = SCORE_TIME_GER
	}
	text = {
		trigger = { is_france_or_mp_country = yes }
		localization_key = SCORE_TIME_FRA
	}
}

defined_text = {
	name = score_nukes_positive
	text = {
		trigger = {
			original_tag = GER
			check_variable = { german_nukes_dropped_normalised_var > 0 }
		}
		localization_key = SCORE_NUKES_GER_POSITIVE
	}
	text = {
		trigger = {
			is_france_or_mp_country = yes
			check_variable = { french_nukes_dropped_normalised_var > 0 }
		}
		localization_key = SCORE_NUKES_FRA_POSITIVE
	}
}

defined_text = {
	name = score_nukes_negative
	text = {
		trigger = {
			original_tag = GER
			check_variable = { nukes_dropped_on_friendly_territory > 0 }
		}
		localization_key = SCORE_NUKES_GER_NEGATIVE
	}
	text = {
		trigger = {
			is_france_or_mp_country = yes
			check_variable = { nukes_dropped_on_friendly_territory > 0 }
		}
		localization_key = SCORE_NUKES_FRA_NEGATIVE
	}
}

defined_text = {
	name = score_industry_positive
	text = {
		trigger = { original_tag = GER }
		localization_key = SCORE_INDUSTRY_GER_POSITIVE
	}
	text = {
		trigger = { is_france_or_mp_country = yes }
		localization_key = SCORE_INDUSTRY_FRA_POSITIVE
	}
}

defined_text = {
	name = score_normandy_landings_defeated_positive
	text = {
		trigger = { has_global_flag = normandy_landings_defeated }
		localization_key = SCORE_NORMANDY_LANDINGS_DEFEATED_GER_POSITIVE
	}
}
defined_text = {
	name = score_early_peace_negotiated_positive
	text = {
		trigger = { has_global_flag = early_peace_negotiated }
		localization_key = SCORE_EARLY_PEACE_NEGOTIATED_GER_POSITIVE
	}
}
defined_text = {
	name = score_paris_liberated_positive
	text = {
		trigger = { has_global_flag = paris_liberated }
		localization_key = SCORE_PARIS_LIBERATED_FRA_POSITIVE
	}
}
defined_text = {
	name = score_brussels_liberated_positive
	text = {
		trigger = { has_global_flag = brussels_liberated }
		localization_key = SCORE_BRUSSELS_LIBERATED_FRA_POSITIVE
	}
}
defined_text = {
	name = score_westwall_penetrated_positive
	text = {
		trigger = { has_global_flag = westwall_penetrated }
		localization_key = SCORE_WESTWALL_PENETRATED_FRA_POSITIVE
	}
}
defined_text = {
	name = score_fall_of_berlin_negative
	text = {
		trigger = { has_global_flag = fall_of_berlin }
		localization_key = SCORE_BERLIN_FALLEN_GER_NEGATIVE
	}
}

defined_text = {
	name = help_title_scripted
	text = {
		trigger = { check_variable = { help_variable = 1 } }
		localization_key = HELP_TITLE_1
	}
	text = {
		trigger = { check_variable = { help_variable = 2 } }
		localization_key = HELP_TITLE_2
	}
	text = {
		trigger = { check_variable = { help_variable = 3 } }
		localization_key = HELP_TITLE_3
	}
	text = {
		trigger = { check_variable = { help_variable = 4 } }
		localization_key = HELP_TITLE_4
	}
	text = {
		trigger = { check_variable = { help_variable = 5 } }
		localization_key = HELP_TITLE_5
	}
	text = {
		trigger = { check_variable = { help_variable = 6 } }
		localization_key = HELP_TITLE_6
	}
	text = {
		trigger = { check_variable = { help_variable = 7 } }
		localization_key = HELP_TITLE_7
	}
	text = {
		trigger = { check_variable = { help_variable = 8 } }
		localization_key = HELP_TITLE_8
	}
	text = {
		trigger = { check_variable = { help_variable = 9 } }
		localization_key = HELP_TITLE_9
	}
	text = {
		trigger = { check_variable = { help_variable = 10 } }
		localization_key = HELP_TITLE_10
	}
	text = {
		trigger = { check_variable = { help_variable = 11 } }
		localization_key = HELP_TITLE_11
	}
	text = {
		trigger = { check_variable = { help_variable = 12 } }
		localization_key = HELP_TITLE_12
	}
	text = {
		trigger = { check_variable = { help_variable = 13 } }
		localization_key = HELP_TITLE_13
	}
	text = {
		trigger = { check_variable = { help_variable = 14 } }
		localization_key = HELP_TITLE_14
	}
	text = {
		trigger = { check_variable = { help_variable = 15 } }
		localization_key = HELP_TITLE_15
	}
	text = {
		trigger = { check_variable = { help_variable = 16 } }
		localization_key = HELP_TITLE_16
	}
	text = {
		trigger = { check_variable = { help_variable = 17 } }
		localization_key = HELP_TITLE_17
	}
	text = {
		trigger = { check_variable = { help_variable = 18 } }
		localization_key = HELP_TITLE_18
	}
	text = {
		trigger = { check_variable = { help_variable = 19 } }
		localization_key = HELP_TITLE_19
	}
	text = {
		trigger = { check_variable = { help_variable = 20 } }
		localization_key = HELP_TITLE_20
	}
	text = { localization_key = NO_TEXT }
}

defined_text = {
	name = help_text_scripted
	text = {
		trigger = { check_variable = { help_variable = 1 } }
		localization_key = HELP_TEXT_1
	}
	text = {
		trigger = { check_variable = { help_variable = 2 } }
		localization_key = HELP_TEXT_2
	}
	text = {
		trigger = { check_variable = { help_variable = 3 } }
		localization_key = HELP_TEXT_3
	}
	text = {
		trigger = { check_variable = { help_variable = 4 } }
		localization_key = HELP_TEXT_4
	}
	text = {
		trigger = { check_variable = { help_variable = 5 } }
		localization_key = HELP_TEXT_5
	}
	text = {
		trigger = { check_variable = { help_variable = 6 } }
		localization_key = HELP_TEXT_6
	}
	text = {
		trigger = { check_variable = { help_variable = 7 } }
		localization_key = HELP_TEXT_7
	}
	text = {
		trigger = { check_variable = { help_variable = 8 } }
		localization_key = HELP_TEXT_8
	}
	text = {
		trigger = { check_variable = { help_variable = 9 } }
		localization_key = HELP_TEXT_9
	}
	text = {
		trigger = { check_variable = { help_variable = 10 } }
		localization_key = HELP_TEXT_10
	}
	text = {
		trigger = { check_variable = { help_variable = 11 } }
		localization_key = HELP_TEXT_11
	}
	text = {
		trigger = { check_variable = { help_variable = 12 } }
		localization_key = HELP_TEXT_12
	}
	text = {
		trigger = { check_variable = { help_variable = 13 } }
		localization_key = HELP_TEXT_13
	}
	text = {
		trigger = { check_variable = { help_variable = 14 } }
		localization_key = HELP_TEXT_14
	}
	text = {
		trigger = { check_variable = { help_variable = 15 } }
		localization_key = HELP_TEXT_15
	}
	text = {
		trigger = { check_variable = { help_variable = 16 } }
		localization_key = HELP_TEXT_16
	}
	text = {
		trigger = { check_variable = { help_variable = 17 } }
		localization_key = HELP_TEXT_17
	}
	text = {
		trigger = { check_variable = { help_variable = 18 } }
		localization_key = HELP_TEXT_18
	}
	text = {
		trigger = { check_variable = { help_variable = 19 } }
		localization_key = HELP_TEXT_19
	}
	text = {
		trigger = { check_variable = { help_variable = 20 } }
		localization_key = HELP_TEXT_20
	}
	text = { localization_key = NO_TEXT }
}
