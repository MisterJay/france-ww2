add_namespace = britain

# Tizard mission event for USA
country_event = {
	id = britain.1
	title = britain.1.t
	desc = britain.1.d
	picture = GFX_report_event_physics_lab_01

	is_triggered_only = yes

	option = {
		name = britain.1.a	#sounds good
		ai_chance = { factor = 90 }
		add_political_power = -10
		ENG = {
			country_event = { days = 1 id = britain.2 }
			add_opinion_modifier = {
				target = ROOT
				modifier = tizard_relations
			}
		}
		add_tech_bonus = {
			name = tizard_mission_focus
			uses = 1
			bonus = 0.5
			category = jet_technology
		}
		add_tech_bonus = {
			name = tizard_mission_focus
			bonus = 0.5
			uses = 3
			category = radar_tech
		}
	}

	option = {
		name = britain.1.b	#no way
		ai_chance = { factor = 10 }
		trigger = {
			is_mp = no
		}
		#add_opinion_modifier = { target = ENG modifier = tizard_refused }
		ENG = {
			country_event = { days = 1 id = britain.3 }
		}
	}

	option = {
		name = britain.1.c	#tech_sharing
		trigger = { has_dlc = "Together for Victory"}
		trigger = {
			is_mp = no
		}
		ai_chance = { factor = 90 }
		add_political_power = -10
		ENG = {
			country_event = { days = 1 id = britain.21 }
			add_opinion_modifier = {
				target = ROOT
				modifier = tizard_relations
			}
		}
		add_to_tech_sharing_group = tizard_mission
	}
}

# USA helps
country_event = {
	id = britain.2
	title = britain.2.t
	desc = britain.2.d
	picture = GFX_report_event_physics_lab_02

	is_triggered_only = yes

	option = {
		name = britain.2.a
		add_tech_bonus = {
			name = tizard_mission_focus
			bonus = 0.5
			uses = 3
			category = radar_tech
		}
	}
}

# USA says no
country_event = {
	id = britain.3
	title = britain.3.t
	desc = britain.3.d
	picture = GFX_report_event_physics_lab_01

	is_triggered_only = yes

	option = {
		name = britain.3.a
		add_opinion_modifier = { target = USA modifier = tizard_refused }
	}
}

# Burma road china
country_event = {
	id = britain.4
	title = britain.4.t
	desc = britain.4.d
	picture = GFX_report_event_burma_road

	is_triggered_only = yes

	option = {
		name = britain.4.a
	}
}

# Burma road yunnan
country_event = {
	id = britain.5
	title = britain.4.t
	desc = britain.4.d
	picture = GFX_report_event_burma_road

	is_triggered_only = yes

	option = {
		name = britain.4.a
	}
}

#End trade with Germany event for SWE/NOR
country_event = {
	id = britain.7
	title = britain.7.t
	desc = britain.7.d
	picture = GFX_report_event_merchant_ship_01

	is_triggered_only = yes


	option = {
		name = britain.7.a
		ai_chance = { factor = 60 }
		GER = {
			add_opinion_modifier = { target = ROOT modifier = embargo }
		}
	}
}


# Chamberlain Resigns
country_event = {
	id = britain.9
	title = britain.9.t
	desc = britain.9.d
	picture = GFX_report_event_chamberlain

	fire_only_once = yes

	trigger = {
		tag = ENG
		has_war = yes
		POL = { has_capitulated = yes }
		OR = {
			has_global_flag = fall_of_france
			FRA = { surrender_progress > 0.1 }
			HOL = { surrender_progress > 0.1 }
			BEL = { surrender_progress > 0.1 }
			LUX = { surrender_progress > 0.1 }
			DEN = { surrender_progress > 0.1 }
			NOR = { surrender_progress > 0.1 }
		}
		NOT = { has_country_flag = chamberlain_died }
		has_country_leader = {
			name = "Neville Chamberlain"
			ruling_only = yes
		}
	}

	mean_time_to_happen = {
		days = 1
	}

	immediate = { set_country_flag = chamberlain_resigned }

	option = {
		name = britain.9.a
		create_country_leader = {
			name = "Winston Churchill"
			desc = "POLITICS_WINSTON_CHURCHILL_DESC"
			picture = "Portrait_Britain_Winston_Churchill.dds"
			expire = "1965.1.1"
			ideology = conservatism
			traits = {
				british_bulldog
			}
		}
		add_stability = 0.1
		add_war_support = 0.1
	}
}

# Chamberlain Passes Away
country_event = {
	id = britain.10
	title = britain.10.t
	desc = britain.10.d
	picture = GFX_report_event_chamberlain

	is_triggered_only = yes

	trigger = {
		NOT = { has_country_flag = chamberlain_resigned }
		has_country_leader = {
			name = "Neville Chamberlain"
			ruling_only = yes
		}
	}

	immediate = { set_country_flag = chamberlain_died }

	option = {
		name = britain.10.a
		ai_chance = { base = 100 }
		kill_country_leader = yes
		create_country_leader = {
			name = "Winston Churchill"
			desc = "POLITICS_WINSTON_CHURCHILL_DESC"
			picture = "Portrait_Britain_Winston_Churchill.dds"
			expire = "1965.1.1"
			ideology = conservatism
			traits = {
				british_bulldog
			}
		}
	}
	option = {
		name = britain.10.b
		trigger = {
			is_mp = no
		}
		ai_chance = { base = 0 }
		kill_country_leader = yes
		create_country_leader = {
			name = "Lord Edward Halifax"
			desc = "POLITICS_EDWARD_HALIFAX_DESC"
			picture = "Portrait_Britain_Edward_Halifax.dds"
			expire = "1965.1.1"
			ideology = conservatism
			traits = {

			}
		}
	}
}

# France wants to join allies
country_event = {
	id = britain.13
	title = britain.13.t
	desc = britain.13.d
	picture = GFX_report_event_degaulle_churchill

	is_triggered_only = yes

	option = {
		name = britain.13.a
		add_to_faction = FROM
		FROM = {
			add_ai_strategy = {
				type = alliance
				id = "ENG"
				value = 200
			}
		}
		FROM = { country_event = { id = france.29 } }
		hidden_effect = {
			news_event = { id = news.175 }
		}
	}

	option = {
		name = britain.13.b
		trigger = { is_mp = no }
		ai_chance = { factor = 0 }
		FROM = {
			add_opinion_modifier = {
				target = ROOT
				modifier = FRA_go_with_britain_reject
			}
			country_event = { id = france.30 }
		}
		hidden_effect = {
			news_event = { id = news.176 }
		}
	}
}

# Germany demands Slovenia from Yugoslavia
country_event = {
	id = britain.14
	title = britain.14.t
	desc = britain.14.d
	picture = GFX_report_event_hitler_croatia_handshake

	is_triggered_only = yes

	option = { # Abandon Yugoalvia
		name = britain.14.a
		ai_chance = {
			factor = 90
			modifier = {
				has_war_with = GER
				factor = 0
			}
		}
		YUG = {
			add_opinion_modifier = { target = ENG modifier = western_betrayal }
		}
		if = {
			limit = {
				has_guaranteed = YUG
			}
			diplomatic_relation = {
   				country = YUG
   				relation = guarantee
   				active = no
  			}
		}
		if = {
			limit = {
				country_exists = FRA
			}
			FRA = { country_event = { id = france.25 hours = 2 } }
		}
		else = {
			if = {
				limit = {
					ITA = {
						OR = {
							is_in_faction_with = GER
							exists = no
						}
					}
				}
				YUG = {
					add_opinion_modifier = { target = FRA modifier = western_betrayal }
					country_event = { id = yugoslavia.5 hours = 6 }
				}
			}
		}
	}

	option = { # Support Yugoslavia
		name = britain.14.b
		trigger = {
			is_mp = no
		}
		ai_chance = {
			factor = 10
			modifier = {
				factor = 0
				is_in_faction_with = GER
			}
		}
		if = {
			limit = {
				is_in_faction_with = GER
				is_faction_leader = yes
			}
			remove_from_faction = GER
		}
		if = {
			limit = {
				is_in_faction_with = GER
				is_faction_leader = no
			}
			GER = { remove_from_faction = ENG }
		}
		add_political_power = -200
		add_stability = -0.05
		add_opinion_modifier = { target = GER modifier = condemn_aggression }
		effect_tooltip = {
			GER = {
				create_wargoal = {
					type = take_state_focus
					target = YUG
					generator = { 102 }
				}
			}
		}
		add_to_faction = YUG
		YUG = {
			add_ai_strategy = {
				type = alliance
				id = "ENG"
				value = 200
			}
		}
		if = {
			limit = {
				FRA = {
					is_puppet = no
					NOT = { has_war_with = YUG }
				}
			}
			FRA = { country_event = { id = france.26 hours = 2 } }
		}
		YUG = {
			add_opinion_modifier = { target = ENG modifier = offered_support }
		}
	}
}

# King dies
country_event = {
	id = britain.16
	title = britain.16.t
	desc = britain.16.d
	picture = GFX_report_event_europe_funeral

	fire_only_once = yes

	mean_time_to_happen = { days = 7 }

	trigger = { tag = ENG }

	immediate = {
		hidden_effect = {
			set_country_flag = ENG_king_died
			remove_ideas = george_v
			add_ideas = ENG_king_edward
			country_event = { id = mtg_britain.1 days = 264 }
		}
	}

	option = {
		name = britain.16.a
		effect_tooltip = {
			remove_ideas = george_v
			add_ideas = ENG_king_edward
		}
	}
}

# King abdicates
# Replaced by mtg_britain.1
# country_event = {
# 	id = britain.17
# 	title = britain.17.t
# 	desc = britain.17.d
# 	picture = GFX_report_event_generic_sign_treaty2

# 	trigger = {
# 		tag = ENG
# 		has_country_flag = ENG_king_died
# 		date > 1936.12.1
# 		NOT = { has_dlc = "Man the Guns" }
# 	}

# 	fire_only_once = yes

# 	mean_time_to_happen = { days = 14 }

# 	immediate = {
# 		hidden_effect = {
# 			remove_ideas = ENG_king_edward
# 			add_ideas = ENG_george_vi
# 		}
# 	}

# 	option = {
# 		name = britain.17.a
# 		effect_tooltip = {
# 			remove_ideas = ENG_king_edward
# 			add_ideas = ENG_george_vi
# 		}
# 	}
# }

country_event = {
	id = britain.21
	title = britain.21.t
	desc = britain.21.d
	picture = GFX_report_event_physics_lab_02

	is_triggered_only = yes

	option = {
		name = britain.21.a
		add_to_tech_sharing_group = tizard_mission
		USA = {
			add_opinion_modifier = { target = ENG modifier = tizard_relations }
		}
	}
}

# Stanley Baldwin Resigns
country_event = {
    id = britain.22
    title = britain.22.t
    desc = britain.22.d
    picture = GFX_report_event_royal_parade

	is_triggered_only = yes

    trigger = {
        has_idea = ENG_george_vi
        NOT = { has_country_flag = baldwin_resigned }
    }

    option = {
        name = britain.22.a
        retire_country_leader = yes
        create_country_leader = {
        	name = "Neville Chamberlain"
        	desc = "POLITICS_NEVILLE_CHAMBERLAIN_DESC"
        	picture = "Portrait_Britain_Neville_Chamberlain.dds"
        	expire = "1965.1.1"
        	ideology = conservatism
        	traits = {
        		chamberlain_appeaser rearmer
        	}
        }
        set_country_flag = baldwin_resigned
	}
}
