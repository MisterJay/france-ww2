﻿add_namespace = wtt_news

# News event for annexation of Vichy France - fleet intact
news_event = {
	id = wtt_news.23
	title = wtt_news.23.t
	desc = wtt_news.23.desc
	picture = GFX_news_event_strasbourg

	is_triggered_only = yes

	option = {
		name = wtt_news.23.a
		trigger = {
			original_tag = GER
		}
	}
	option = {
		name = wtt_news.23.b
		trigger = {
			original_tag = ENG
		}
	}
	option = {
		name = wtt_news.23.c
		trigger = {
			NOT = {
				original_tag = GER
				original_tag = ENG
			}
		}
	}
}

# News event for annexation of Vichy France - fleet scuttled
news_event = {
	id = wtt_news.24
	title = wtt_news.24.t
	desc = wtt_news.24.desc
	picture = GFX_news_french_fleet_scuttled

	is_triggered_only = yes

	option = {
		name = wtt_news.24.a
		trigger = {
			original_tag = GER
		}
	}
	option = {
		name = wtt_news.24.b
		trigger = {
			original_tag = ENG
		}
	}
	option = {
		name = wtt_news.24.c
		trigger = {
			NOT = {
				original_tag = GER
				original_tag = ENG
			}
		}
	}
}

# China - Flying Tigers sent to China
news_event = {
	id = wtt_news.34
	title = wtt_news.34.t
	desc = wtt_news.34.desc
	picture = GFX_news_event_china_flying_tigers

	is_triggered_only = yes

	option = {
		name = wtt_news.34.d
	}
}

# Canals - Suez Canal Blown
news_event = {
	id = wtt_news.40
	title = wtt_news.40.t
	desc = {
		text = wtt_news.40.desc_eng
		trigger = {
			FROM = {
				has_country_flag = blew_up_suez
				owns_state = 639
			}
		}
	}
	desc = {
		text = wtt_news.40.desc_not_eng
		trigger = {
			FROM = {
				OR = {
					NOT = {	has_country_flag = blew_up_suez }
					NOT = {
						owns_state = 639
					}
				}
			}
		}
	}
	picture = GFX_news_event_020

	is_triggered_only = yes

	option = {
		name = wtt_news.40.a
		trigger = {
			NOT = { has_country_flag = blew_up_suez }
			owns_state = 639
		}
		if = {
			limit = {
				original_tag = ENG
			}
			add_war_support = -0.05
			add_stability = -0.05
		}
	}
	option = {
		name = wtt_news.40.b
		trigger = {
			has_country_flag = blew_up_suez
			owns_state = 639
		}
		if = {
			limit = {
				original_tag = ENG
			}
			add_war_support = -0.05
			add_stability = -0.05
		}
		clr_country_flag = blew_up_suez
	}
	option = {
		name = wtt_news.40.c
		trigger = {
			any_country = {
				has_war_with = ROOT
				owns_state = 639
				has_country_flag = blew_up_suez
			}
		}
	}
	option = {
		name = wtt_news.40.d
		trigger = {
			any_country = {
				has_war_with = ROOT
				owns_state = 639
			}
			has_country_flag = blew_up_suez
		}
		clr_country_flag = blew_up_suez
	}
	option = {
		name = wtt_news.40.e
		trigger = {
			any_country = {
				has_war_with = ROOT
				owns_state = 639
				NOT = { has_country_flag = blew_up_suez }
			}
			any_other_country = {
				has_war_together_with = ROOT
				has_country_flag = blew_up_suez
			}
			NOT = { has_country_flag = blew_up_suez }
		}
	}
	option = {
		name = wtt_news.40.f
		trigger = {
			any_other_country = {
				has_war_together_with = ROOT
				owns_state = 639
				has_country_flag = blew_up_suez
			}
			NOT = { has_country_flag = blew_up_suez }
		}
	}
	option = {
		name = wtt_news.40.g
		trigger = {
			any_country = {
				has_war_with = ROOT
				has_country_flag = blew_up_suez
			}
			any_other_country = {
				has_war_together_with = ROOT
				owns_state = 639
				NOT = { has_country_flag = blew_up_suez }
			}
			NOT = { has_country_flag = blew_up_suez }
		}
	}
	option = {
		name = wtt_news.40.h
		trigger = {
			NOT = {
				any_country = {
					has_war_with = ROOT
					has_country_flag = blew_up_suez
				}
			}
			NOT = {
				any_other_country = {
					has_war_together_with = ROOT
					owns_state = 639
					has_country_flag = blew_up_suez
				}
			}
			NOT = { has_country_flag = blew_up_suez }
		}
	}
}

# Canals - Suez Canal Rebuilt
news_event = {
	id = wtt_news.42
	title = wtt_news.42.t
	desc = wtt_news.42.desc
	picture = GFX_news_event_020

	is_triggered_only = yes

	option = {
		name = wtt_news.42.a
		trigger = {
			original_tag = ENG
			has_country_flag = rebuilt_suez
		}
		clr_country_flag = rebuilt_suez
	}
	option = {
		name = wtt_news.42.b
		trigger = {
			NOT = { original_tag = ENG }
			has_country_flag = rebuilt_suez
		}
		clr_country_flag = rebuilt_suez
	}
	option = {
		name = wtt_news.42.c
		trigger = {
			any_other_country = {
				has_war_together_with = ROOT
				has_country_flag = rebuilt_suez
			}
			NOT = { has_country_flag = rebuilt_suez }
		}
	}
	option = {
		name = wtt_news.42.d
		trigger = {
			any_other_country = {
				has_war_with = ROOT
				has_country_flag = rebuilt_suez
			}
			NOT = { has_country_flag = rebuilt_suez }
		}
	}
	option = {
		name = wtt_news.42.e
		trigger = {
			NOT = { has_country_flag = rebuilt_suez }
			NOT = {
				any_other_country = {
					has_war_together_with = ROOT
					has_country_flag = rebuilt_suez
				}
			}
			NOT = {
				any_other_country = {
					has_war_with = ROOT
					has_country_flag = rebuilt_suez
				}
			}
		}
	}
}
