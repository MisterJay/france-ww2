﻿###########################
# Hungarian Events
###########################

add_namespace = hungary

# Second Vienna Award - No Territory
country_event = {
	id = hungary.1
	title = hungary.1.t
	desc = hungary.1.d
	picture = GFX_report_event_finnish_letter

	is_triggered_only = yes

	option = {
		name = hungary.1.a
	}
}

# Second Vienna Award - Northern Transylvania
country_event = {
	id = hungary.2
	title = hungary.2.t
	desc = hungary.2.d
	picture = GFX_report_event_second_vienna_award_pact

	is_triggered_only = yes

	option = {
		name = hungary.2.a
		HUN = {
			transfer_state = 436		#North Transylvania
			transfer_state = 445	#Szeklerland
			transfer_state = 423	#Maramaros
			transfer_state = 432	#Northern Partium
		}
		hidden_effect = {
			country_event = { days = 1 id = news.15 }
		}
	}
}

# Second Vienna Award - All of Transylvania
country_event = {
	id = hungary.3
	title = hungary.2.t
	desc = hungary.3.d
	picture = GFX_report_event_second_vienna_award_pact

	is_triggered_only = yes

	option = {
		name = hungary.2.a
		HUN = {
			transfer_state = 436		#North Transylvania
			transfer_state = 445	#Szeklerland
			transfer_state = 423	#Maramaros
			transfer_state = 432	#Northern Partium
			transfer_state = 456	#Crisana
			transfer_state = 455	#Arad
			transfer_state = 452	#Transylvania
		}
		hidden_effect = {
			country_event = { days = 1 id = news.16 }
		}
	}
}

# Second Vienna Award - Romania Rejects Verdict (Northern Transylvania)
country_event = {
	id = hungary.4
	title = hungary.2.t
	desc = hungary.4.d
	picture = GFX_report_event_second_vienna_award_pact

	is_triggered_only = yes

	option = {
		name = hungary.4.a
		add_state_claim = 436		#North Transylvania
		add_state_claim = 445	#Szeklerland
		add_state_claim = 423	#Maramaros
		add_state_claim = 432	#Northern Partium
		hidden_effect = {
			country_event = { days = 1 id = news.17 }
		}
	}
}

# Second Vienna Award - Romania Rejects Verdict (All of Transylvania)
country_event = {
	id = hungary.5
	title = hungary.2.t
	desc = hungary.5.d
	picture = GFX_report_event_second_vienna_award_pact

	is_triggered_only = yes

	option = {
		name = hungary.4.a
		add_state_claim = 436		#North Transylvania
		add_state_claim = 445	#Szeklerland
		add_state_claim = 423	#Maramaros
		add_state_claim = 432	#Northern Partium
		add_state_claim = 456	#Crisana
		add_state_claim = 455	#Arad
		add_state_claim = 452	#Transylvania
		hidden_effect = {
			country_event = { days = 1 id = news.18 }
		}
	}
}

# Hungary (First Ljubljana Award) from german focus
country_event = {
	id = hungary.6
	title = hungary.6.t
	desc = hungary.6.d
	picture = GFX_report_event_vienna_award_negotiations

	is_triggered_only = yes

	option = {
		name = hungary.6.a
		HUN = { transfer_state = 490 }
	}
}