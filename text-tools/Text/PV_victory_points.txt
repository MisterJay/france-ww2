136;10945;Narva;5

Ireland

264;1080;Limerick;1
216;884;Galway;1

USA

1060;8615;Austin;5
1085;8623;Oklahoma City;1
1087;8628;Topeka;1
1089;8635;Lincoln;1
1093;8644;Pierre;1
802;8653;Bismarck;1
1090;8637;Des Moines;1
1086;8626;Jefferson City;1
1084;8621;Little Rock;1
1091;8643;Madison;1
1069;8627;Frankfort;1
1067;8625;Nashville;1
1062;8618;Jackson;1
1063;8619;Montegomery;1
1064;8620;Atlanta;1
1065;8622;Columbia;1
1066;8624;Raligh;1
1068;8630;Richmond;1
1070;8632;Charleston;1
1075;8633;Columbus;1
1077;8639;Harrisburg;1
1073;8638;Trenton;1
803;8652;Augusta;1 

SOV
101;5013;Roshchino;1
101;11010;Vyborg;3
101;11006;Priozersk;1

111;4402;Sosnovy Bor;3
111;258;Kingisepp;1
111;3844;Gatchina;3
111;10975;Volosovo;1
111;415;Slantsy;1
111;10939;Luga;1

146;1189;Pskov;5


146;10814;Ostrov;3
146;917;Pytalovo;1

156;10791;Podolsk;5
156;348;Naro-Fominsk;5
156;10857;Moscow Central;50

350;9543;Lviv;20
350;5621;Stryi;5
350;1278;Chervononrad;5

351;598;Ternopil;3
351;3726;Ivano-Frankivsk;3
351;1679;Chernivtsi;3
351;5661;Kamyanets-Podil's'kyi;1
351;598;Ternopil;5


311;9509;Novovolynsk;3
311;710;Horokhiv;1
311;10266;Lutsk;10
311;626;Lyumbomi;1
311;10303;Kovel;3

311;10290;Rozhyshche;1
311;10315;Manevychi;1
311;10331;Ratne;1
311;10333;Kamin-Kashyrs'kyi;1
311;5399;Lyubeshiv;1
311;784;Kievan;1
311;5565;Dubno;1

351;9507;Zolochiv;1

146;10825;Ostrov;3
146;10784;Opochka;3
146;10772;Velikiye Luki;3
146;10770;Idritsa;1
146;10790;Pustoshka;1
146;707;Pechory;1




Format:

State;Province;Name of City;VP value

5;5012;Murmansk;5
5;3494;Rayakoski;1
5;993;Yonskii;1
5;1177;Tetrino;1
5;596;Umba;1

32;11155;Chupa;1
32;11151;Kesten'ga;1
32;5030;Kalevala;1
32;11109;Sosnovets;1
32;767;Niukhcha;1
32;7523;Tolvuyskoye;1
32;5034;Medvezhyegorsk;1
32;3584;Petrozavodsk;5


336;10215;Shepetivka;3
336;10186;Khmelnytskyi;5
336;10179;Dunaivtsi;1

202;1114;Kokhanovichi;1
202;10677;Kiyastitsy;3
202;10706;Gorbachevo;1
202;3708;Lepiel;1



112;1021;Demyansk;5
111;10942;Novogorod;5
108;1369;Rzhev;5
197;3538;Vyazma;5
197;10650;Smolensk;5
225;10588;Bryansk;5


238;2534;Kursk;20
320;10267;Luhansk;10
274;10239;Rostov-on-Don

238;5307;Rylsk;3
238;1306;Troebortnoe;1
238;10463;Tim;1
238;10500;Gorshechnoye;5
238;10508;Ponyri;1
238;10501;Zheleznogork;5

238;180;Pristen;1
238;10524;Kastornoye;1

218;10576;Oryol;10

228;2962;Oronezh;10

228;10483;Nizhnedevitsk;1
228;10467;Rossosh;1
228;846;Korotoyak;3
228;1236;Kashirskoe;1

228;868;Talovaya;3
228;10594;Borisoglebsk;5
228;10543;Anna;1
228;3418;Kalach;1

222;10523;El'ton;1
222;10622;Pallasovka;1
222;10605;Zhirnovsk;1
222;10579;Yelan;3
222;10522;Uryupinsk;1
222;10340;Kotelnikovo;1
222;559;Surovikino;3
222;10456;Volzhskiy;5

222;10417;Kalach-na-Donu;1
222;10587;Kamyshin;3
222;10474;Frolovo;1
222;10495;Mikhaylovka;5
222;10529;Novoanninsky;1
222;10586;Kotovo;1

222;10547;Antipovka;1
222;10502;Gornyi Balyklei;1
222;10464;Dubovka;1
222;10433;Nizhnii Baskunchak;1
222;10423;Znamensk;1
222;10553;Vishnyovka;1
222;10455;llovlya;1
222;10518;Ol'khovka;1

230;11175;Bryansk;10

304;10306;Brovary;10
304;10301;Yahotyn;3
304;3466;Ivankiv;1
304;1700;Pryp'yat;3
304;92;Chornobyl;3
304;10233;Tetiiv;1
304;10276;Bila Tserkva;10
304;1403;Makariv;1

274;2052;Matveev Kurgan;1
274;10270;Shakhty;3
274;1115;Millerovo;3
274;5389;Donetsk;3
274;3655;Kamensk-Shakhtinsky;1
274;1871;Shumilinskaya;1
274;10394;Oblivskaya;1
274;10415;Soviet;1
274;3655;Chekunov;1
274;;10404;Kashary;1

274;10320;Bogatyrev;1
274;10291;Volgodonsk;10

290;10354;Elista;10
290;10392;Malye Derbety;3
290;10281;Svetiograd;1
290;10232;Nevinnomyssk;1
290;10274;Izobliny;1
290;10288;Neftekumsk;1

217;444;Lipetsk;10
217;10626;Gryazi;3
217;3829;Yelets;5
217;10574;Usman;3
217;218;Volovo;1
217;10635;Dobroe;1

176;4606;Penza;10
176;10668;Zarechny;1
176;471;Belinsky;1
176;10608;Balashov;3
176;317;Kalininsk;1
176;10627;Arkadak;1
176;10653;Belinsky;1

296;10310;Luhansk;5
296;3933;Severodonetsk;3
296;10345;Petropavlivka;1
296;10362;bilokurakyne;1
296;858;Markivka;1
296;5378;Khrustaninyi;1

320;2826;Pokrovsk;3
320;10219;Volnovakha;1
320;5558;Mariupol;10
320;4175;Kramotorsk;3
320;10265;Shakhtars'k;1
320;331;Slovyansk;3

193;10678;Tambov;5
193;10663;Kotovsk;1
193;10671;Michurinsk;1
193;10748;Algasovo;1
193;10692;Rasskazovo;1
193;10625;Zherdevka;1
193;623;Uvarovo;1

203;10504;Atyrau;5
203;10559;Makat;1
203;10470;Tengiz;1
203;10651;Uralsk;3
203;10555;Kulsary;1

278;1097;Astrakhan;10
278;10263;Kochube;1
278;10413;Sasykoli;1
278;10381;Kachalovo;1

177;10780;Ryanzan;10
177;2056;Chaplygin;1
177;95;Sarai;1
177;10756;Shatsk;1
177;10782;Sasovo;1
177;10823;Spas-Klepiki;1
177;10824;Tuma;1
177;10759;Lesnoi;3

177;10719;Klekotki;1
177;10785;Malakhovo;1

194;10694;Tula;5
194;511;Novomoskovsk;5
194;10652;Chern;1
194;3026;Suvorov;1
194;1751;Ivan'kovo;1
194;10717;Zaokskiy;1

191;10695;Kaluga;5
191;1121;Obninsk;1
191;10705;Aleksin;3
191;24;Lyudinovo;1
191;10680;Sukhinichi;1
191;10687;Spas-Demensk;1
191;10739;Mikhali;1

197;10681;Safonovo;1
197;341;Yartsevo;3
197;10618;Roslavl;3
197;10633;Desnogorsk;1
197;10729;Yarilovo;1
197;10710;Velizh;1
197;2422;Ugra;1

230;10537;Vygonichi;1
230;10591;Karachev;3
230;10493;Sevsk;1
230;10497;Suzemka;1
230;10506;Trubchevsk;1
230;1994;Gomel;5
230;3707;Kholmyech;1

225;10535;Pochep;5
225;1220;Dyatkovo;1
225;3305;Kastsyukovichy;1

293;10371;Kharkivska;10
293;10397;Vovchans'k;3
293;10363;Chuhuiv;1
293;10335;Izyum;1
293;10300;Krasnohrad;1
293;10316;Kehychivka;1

156;10851;Lyubertsy;3
156;10747;Serebryanye Prudy;1
156;5178;Zaraysk;1
156;10850;Odintsovo;3
156;10809;Sakharovo;10
156;10810;Kolomna;1
156;1651;Lotoshino;1
156;4999;Drovnino;1

108;416;Rzhev;10
108;1968;Vesyegonsk;1
108;10874;Dubna;5
108;10912;Vyshny Volochyok;1
108;10934;Udomlya;1
108;10843;Toropets;1
108;10868;Andreapol;1

263;10484;Stary Oskol;10
263;208;Belgorod;10
263;4231;Grayvoron;1
263;10416;Rakitnoe;1
263;10440;Ivnya;1
263;10432;Novy Oskol;3
263;10424;Biryuch;1
263;4235;Roven'ki;1

262;10407;Sumska;10
262;10360;Okhtyrka;1
262;10366;Romny;3
262;10400;Nedryhailiv;1
262;129;Konotop;1
262;1122;Shostka;1
262;2868;Hlukhiv;3

224;10520;Zhodzina;1
224;1250;Horki;3
224;1570;Mscislau;1
224;642;Viduitsy;1
224;10538;Krasnapolle;1
224;10568;Barysaw;1
224;10450;Terebuty;1

266;10442;Semenivka;1
226;818;Bakmach;5
266;3493;Pryluky;3
226;10352;Oster;1
226;10396;Chernihiv;10
266;10410;Mena;1
266;10387;Veresoch;1

112;413;Chudovo;1
112;4248;Khvoynaya;3
112;3670;Pestovo;1
112;1676;Borovichi;1
112;247;Okulovka;1
112;10961;Kresttsy;1
112;10909;Zaluch'e;3

55;10993;Naziya;1
55;4996;Kolchanovo;1
55;11023;Alekovshchina;1
55;2136;Tikhvin;3
55;10990;Chagoda;1
55;11001;Cherepovets;1
55;11033;Kamenka;1
55;5072;Poidarsa;1
55;11066;Niuksenitsa;1
55;11049;Syamzha;1
55;504;Gonevo;1

218;1073;Bolkhov;5
218;5236;Khotynets;5
218;123;Bogoroditskoe;3
218;1906;Mtsens;3
218;5220;Golovlyovo'1
218;10593;Pokrovskoe;1
218;10551;Gubkino;1
218;10606;Pankovo;1
218;1987;Trubitsino;1
218;10573;Livini;5

306;10346;Ovruch;3
306;10337;Olevs'k;1
306;10321;Yemil'chyne;1
306;10334;Luhyny;1
306;4946;Novohrad-Volyns'kyi;3
306;10289;Bronyky;1
306;2677;Dubrivka;1
306;10227;Lyubar;3
306;153;Vinnytsia;1
306;863;Sharhorod;1

334;10214;Rozivka;1
334;10257;Mezhova;3
334;10218;Pokrovs'ke;1

312;1800;Kotel'va;1
312;4365;Tsarychanka;1
312;10279;Orzhytsya;1
312;10328;Pryatvn;1
312;10305;Lubny;3
312;2301;Zavodske;1
312;10302;Poltava;10
312;10297;Karlivka;1

356;3403;Berdyans'k;5
356;10139;Melitopol';3
356;10182;Polohy;1
356;10173;Tokmak;1
356;10196;Zaporizhzhia;10
356;1176;Novomykolaivka;1

331;10247;Dnipropetrovsk;10
331;3697;Pavlohrad;3
331;10190;Nikopol';1
331;5626;Vil'nohirs'k;1
331;10194;Pokrov;1
331;105;Pereschchepyne;1


331;4580;Kryvyi;Rih;5

344;10243;Deriivka;1
344;10262;Kremenchuk;3
344;10208;Oleksandrivka;1
344;10203;Kropyvnytskyi;10
344;1480;Haivoron;1
344;1652;Perehonikva;1

349;10155;Stavropol;3
349;3987;Pyatigorsk;1
349;10178;Neftekumsk;1
349;10198;Solenoe;1
349;10220;Arzgir;1

323;10259;Cherkaska;5
323;10299;Drabiv;1
323;10261;Kaniv;1
323;10207;Uman;3
323;555;Tal'ne;1
323;2077;Dzhulynka;1

#touch ups

146;10840;Pereezd;1
146;10900;Staraya Russa;1
146;3318;Gdov;1
146;10853;Bologovo;1

11;615;Pavlodar;5

173;10489;Saykhin;1
173;5222;Kaztalovka;1
173;10628;Zhalpaqtal;1
173;483;Uralsk;5
173;10736;Budarino;1
173;10654;Talpaq;1
173;10510;Temirzhan;1
173;1436;Qaratobe;1

6;1282;Mezan;3
6;11130;Onega;1
6;11149;Arkhangelsk;5
6;3049;Yarensk;1
6;11100;Plesetsk;1
6;11086;Nyandoma;1
6;11079;Velsk;1
6;198;Karpogory;1
6;11101;Kotlas;3
6;11165;Pinega;1

102;10987;Rybinsk;3
102;545;Yaroslavl;10
102;5146;Pereslavl-Zalessky;1
102;10959;Rostov;1
102;302;Golovino;1
102;11003;Danilov;1

105;1367;Ivanovo;10
105;10952;Pistsovo;1
105;10983;Kineshma;3
105;10950;Yuzha;1
105;10933;II'inskoe-Khovanskoe;1

85;10972;Nizhny Novgorod;10
85;10956;Kstovo;1
85;1133;Tonshaevo;1
85;10927;Sergach;1
85;5129;Pochinki;1
85;10867;Vyksa;1
85;10926;Pavlovo;1
85;10920;Arzamas;1

144;10919;Suzdal;1
144;10932;Kovrov;3
144;10884;Vladimir;5
144;112;Pokrov;1
144;10858;Melenki;1

147;10847;Saransk;5
147;480;Temnikov;1
147;10885;Dubenki;1
147;2212;Umet;1

66;1142;Nerekhta;1
66;11021;Kostroma;5
66;11030;Sudislavi;1
66;11069;Talitsa;1
66;11040;Shekshema;1

7;1308;Letka;1
7;3082;Vizinga;1
7;693;Bol'shaya Puchkoma;1
7;11172;Ukhta;3

46;11080;Piniug;1
46;11176;Kirov;5
46;11037;Yaransk;1
46;11045;Urzhum;1
46;11114;Lesnoi;1
46;11062;Uni;1

107;1883;Glazov;1
107;10924;Mozhga;1
107;10968;Votkinsk;1

176;10746;Nizhny Lomov;1
176;10742;Kuznetsk;1
176;10800;Nikolsk;1
176;10757;Mokshan;1

#new
272;1037;Ashgabat;3
272;5501;Mary;1

192;1140;Kiziltepa;1

215;262;Karakalpakiya;1
215;3103;Jizzakh;3
215;880;Zarafshan;3

229;2638;Aktau;1
229;1443;Beyneu;1
229;854;Zhanaozen;1

103;10819;Aralsk;1
103;10768;Ayteke Bi;1
103;603;Kyzylorda;3

99;10883;Aktobe;3
99;1740;Uil;1
99;10901;Martuk;1
99;10830;Kobda;1
99;3566;Bozoy;1
99;10946;Komsomol'skoye;1

27;11016;Zhezqazghan;1

182;853;Aleksandrov Gai;1
182;10697;Saratov;3
182;432;Balakovo;1
182;10803;Pereliub;1

127;400;Inza;1
127;3888;Pavlovka;1
127;10879;Surskoe;1
127;10886;Ulyanovsk;3
127;10921;Undory;1

82;226;Kazan;10
82;11027;Naberezhnye Chelny;1
82;1658;Nizhnekamsk;1
82;447;Nurlat;1
82;11017;Arsk;1
82;1852;Almetyevsk;1

60;2927;Mozhga;1
60;11061;Izhevsk;3
60;896;Glazov;1
60;11055;Uva;1

110;1206;Samara;10
110;1630;Syzran;3
110;10913;Tolyatti;1
110;10833;Bol'shaya;1
110;10899;Borskoe;1

98;3380;Orenburg;10
98;643;Orsk;5
98;10894;Buzuluk;1
98;10960;Buguruslan;1
98;225;Sol-IIetsk;1
98;54;Svetlyi;1

52;1439;Magnitogorsk;10
52;11082;Chelyabinsk;5
52;902;Satka;1
52;11054;Varna;1
52;11072;Troitsk;1

61;11046;Ufa;5
61;97;Oktyabrsky;1
61;694;Salavat;1
61;11004;Sterlitamak;1
61;11042;Beloretsk;1
61;3723;Sibay;1

21;11132;Bad'ya;1
21;11108;Seyva;1
21;209;Chusovskoi;1
21;28;Solikamsk;1
21;11123;Berezniki;1
21;11102;Perm;5
21;3080;Chernushka;1
21;11098;Kudymkar;1

49;213;Taranoskoe;1
49;11067;Kostanay;3
49;1463;Zhetikara;1
49;1360;Petropavl;1
49;51;Nur-sultan;5
49;604;Alakol;1

33;11095;Kurgan;5
33;3;Shadrinsk;1
33;11140;Mokrousovo;1

9;200;Tylmen;5
9;11164;Valutorovsk;1
9;812;Kazanskoe;1

12;11128;Yekaterinburg;3
12;11127;Kachkanar;1
12;11163;Tabory;1
12;1078;Krasnoturyinsk;1
12;1827;Burmantovo;1

24;11162;Petropavl;1
24;11119;Saumalkoi;1
24;11089;Takhtabrod;1
24;549;Talshik;1

29;1204;Nur-Sultan;3
29;3978;Astrakhanka;1
29;752;Derzhavinsk;1
29;666;Bestobe;1

10;1520;Omsk;3
10;11182;Sherbakul;1

8;909;Kuminskiy;1
